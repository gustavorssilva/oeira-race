# CDT Registrations 2019

**Hold on. Isn't this just the same code as 2018?**

Mostly, yeah. The only difference between this and the old one is that this one uses `create-react-app` while the other one is a `react-boilerplate` project.

**Okay, then...why?**

`react-boilerplate`'s build sizes are _insane_. Even with code-splitting, when you run the build command in the `react-boilerplate` project, the main.js file is 3 MB. _THREE WHOLE MEGABYTES!_ It makes no sense for an app that is essentially just a handful of forms to come to 3 MB.

Meanwhile, by copying the code into a `create-react-app` project, the main.js size is ~134 KB (not gzipped). That's almost 96% smaller. It's a no-brainer.

**Cool, so this code is good to go?**

Err, yes and no. Yes, it will work and can be deployed. No, it's not "good". See below.

---

⚠️ _If this code is going to be re-used in the future, for the love of God, refactor it first. Better yet, start from scratch. This project was **rushed**._ ⚠️

---

## Refactoring ideas

### Redux state

Restructure the redux state into something simpler and more logical.

**Some ideas:**

- Manage all runner info in `runnerState`. For teams, after posting the excel file, put the info returned by the backend into `runnerState` instead of (or as well as) `filePostState`. When a user wants to edit their existing registration, just dump the data in runnerState directly instead of `editRegistrationState`.
- The InvoiceForm and PromoCode handler components should have their own reducers instead of awkwardly including the info on the `leader` or first object in the runner-details state. On submit, merge the info with the data to be posted if it's appropriate (for example, if the user has filled in all the invoice fields, or if a promo code was successfully applied).

#### Example structure

```js
{
  raceInfo: {
    shirtSizes,
    kidsRaceOpen,
    currentPriceLevel,
  },

  runners: [runnerObject],
  currentRunnerIndex: 0,


  // Only use this for team-specific info
  // Store the team members' details in `runners`
  teamInfo: {
    manager: { name, email, phone, ...etc },
    name,
    code,
  },

  // Only use this for the request and error
  // Put the response into `runners`
  filePostState: standardStateObject,

  paymentInfo: {},
  invoiceInfo: {},

  promoCode: ``, // The string value of the code to be applied
  appliedPromoCode: {
    requesting,
    error,
    data, // The discount breakdown.
    // Could also merge the promoCode value in here when a code has successfully been applied
  },

  // Could be used for both "individual" and "team" registrations
  postRegistration: standardStateObject,

  // Used when checking for duplicate IDs
  checkIDState: standardStateObject,

  // Used the first time an individual tries to add another
  // runner to his registration
  hasAskedOthersAboutGDPR: false,
  editLinkRequest: standardStateObject,
  getRegistrationForEdit: standardStateObject,
  updatedRegistrationPost: standardStateObject,
}

const standardStateObject = {
  requesting: false,
  error: null,
  data: null,
}
```

### Components

Make page components that adapt to whether it's a regular/team/kids registration. Some already exist (e.g. `DetailsConfirmationPage`, `RunnerForm`), but they could be better.

#### Common pages

- Details confirmation page (including payment, invoice and promo-code)
- Form page (and pass a prop/check for `registration_id` to know whether it's new or edit)
- Race select page
- Success page

#### Team pages

- Join team
- Enter team code
- Team manager/leader info form
