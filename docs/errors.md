# Errors

| Error                 | Code                  | Message                                                                |
| --------------------- | --------------------- | ---------------------------------------------------------------------- |
| RaceNotFound          | racenotfound          | The specified Race does not exist                                      |
| RegistrationsClosed   | registrationsclosed   | The registration is closed for this race/enrollment type               |
| MixedRacesInTeam      | mixedracesinteam      | The team must not have enrollments with mixed race types               |
| TypeNotFound          | typenotfound          | The specified Enrollment Type does not exist                           |
| PaymentMethodNotFound | paymentmethodnotfound | The Payment method was not found. Please check it exists               |
| GenderNotFound        | gendernotfound        | The Gender not found. Please check it exists                           |
| EnrollmentNotFound    | enrollmentnotfound    | The Enrollment specified wasn't found, please check if it exists       |
| DuplicateEnrollment   | duplicateenrollment   | There is an enrollment with the same Registration or Civil ID          |
| ShirtSizeNotAvailable | shirtsizenotavailable | The Shirt Size requested is not available                              |
| NoStartingLineProof   | nostartinglineproof   | The starting line proof image is mandatory for this Enrollment         |
| PaymentNotFound       | paymentnotfound       | The Payment was not found. Please check it exists                      |
| UnreferencedPayment   | unreferencedpayment   | The Payment has no linked enrollments                                  |
| DuplicateTeamName     | duplicateteamname     | There is an team with the same name. Please choose another name        |
| TeamNotFound          | teamnotfound          | The Team specified wasn't found, please check if it exists             |
| TypeNotAllowedINTeams | typenotallowedinteams | This enrollment type is not allowed to be part of a team               |
| NoMixedTypes          | nomixedtypes          | No mixed types are allowed in enrollments                              |
| TypeMismatch          | typemismatch          | The Enrollment you are trying to update has a different type           |
| TeamAlreadyAssigned   | teamalreadyassigned   | The Enrollment already has a Team assigned                             |
| InvalidDiscountCode   | invaliddiscountcode   | The Discount code you are trying to apply does not exist or is invalid |
| NoDiscountForKids     | nodiscountforkids     | Discount codes are not apllicable to kids enrollments                  |
| InvalidStartBox       | invalidstartbox       | Invalid start box                                                      |
