import { createGlobalStyle } from 'styled-components';

/* eslint no-unused-expressions: 0 */
export default createGlobalStyle`
  @keyframes spin {
    from {
      transform: rotate(0deg);
    }
    to {
      transform: rotate(360deg);
    }
  }

  .spin { animation: spin 1s linear infinite; }

  html,
  body {
    box-sizing: border-box;
    margin: 0;
    padding: 0;
    height: 100vh;
    min-height: 100%;
  }

  *, *:before, *:after {
    box-sizing: inherit;
  }

  body, input, select, button {
    font-family: 'Roboto', sans-serif;
    color: ${({ theme }) => theme.textColor};
  }

  #app {
    position: relative;
    background-color: #f7f7f7;
    min-height: 100vh;
    min-width: 100%;
    max-width: 100vw;
    overflow-x: hidden;
  }

  #wrapper {
    position: relative;
    min-height: inherit;
    display: flex;
    flex-direction: column;
  }

  #page-content {
    flex: 1 1 auto;
    padding: 10px;
    overflow: hidden;
  }

  h1, h2, h3, h4, h5, h6 {
    color: ${({ theme }) => theme.primary};
    line-height: 1.2em;
    margin-top: 0;
    text-align: center;
  }
  h1 {
    font-size: 28px;
    text-transform: uppercase;
    margin-top: 30px;
  }

  .red-caps {
    color: ${({ theme }) => theme.warningRed};
    text-transform: uppercase;
  }

  svg {
    user-select: none;
  }

  a, button {
    cursor: pointer;
  }

  .mdi-icon {
    fill: #ffffff;
  }

  *:disabled,
  *[aria-disabled="true"],
  .disabled {
    cursor: not-allowed;
  }
`;
