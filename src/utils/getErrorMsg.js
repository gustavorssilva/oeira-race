import _ from 'lodash';
import { reportErrorToSentry } from './sentry';

const getCode = (errorObj) => _.get(errorObj, 'code');

function logError(error) {
  const values = _.map(error, (v, k) => `${k}: ${v}`).join(`\n`);
  console.error(values);
}

function parseErrorArray(errors) {
  _.each(errors, logError);
  return _.map(errors, getCode);
}

const msgObject = (message) => ({ message });

export function getFirstErrorMsg(error, reportError = true) {
  try {
    const firstError = _.get(error, 'response.data.errors[0]');

    if (_.isEmpty(firstError)) {
      return msgObject(error.toString());
    }

    if (!_.isObject(firstError)) {
      console.warn(`error type:`, typeof firstError);
      console.warn(firstError);
      return msgObject(JSON.stringify(error));
    }

    if (reportError) reportErrorToSentry(error);

    return { ...firstError, message: firstError.code };
  } catch (e) {
    return msgObject(error.toString());
  }
}

function getErrorMsg(error, reportError = true) {
  let errorMsg;
  const data = _.get(error, 'response.data');
  const errors = _.get(data, 'errors');
  const message = _.get(data, 'message');

  // Sometimes the data is just a string, so check first
  const isArray = _.isArray(errors);

  if (isArray) {
    errorMsg = parseErrorArray(errors);
  } else if (message) {
    // In case it's a 404 or something else
    errorMsg = error.toString();
  } else {
    errorMsg = data;
  }

  // Log so that the API error message is picked up in Sentry
  // (unless it was already logged in parseErrorArray)
  if (!isArray) console.error(errorMsg);
  if (reportError) reportErrorToSentry(error);

  return errorMsg;
}

export default getErrorMsg;
