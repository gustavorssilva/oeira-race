import _ from 'lodash';
import {
  AWS_ASSETS_URL,
  VALID_FILE_TYPES,
  FILE_IS_INVALID,
  MAX_FILE_SIZE,
  FILE_TOO_BIG_ERROR,
  PRICE_CHANGE_DATE,
  PRICES_BEFORE_CHANGE,
  PRICES_AFTER_CHANGE,
} from 'utils/constants';

// For checking if *anything* is loading
const isStateKey = (value, key) => /State$/.test(key) && _.isPlainObject(value);
const isActionKey = (value, key) => key === 'requesting' && _.isBoolean(value);

export function isLoadingInProgress(props) {
  const pickedProps = _.pickBy(props, isStateKey);
  const loadingValues = _.flatMap(pickedProps, (propObj) =>
    _.map(propObj, (value, key) => (isActionKey(value, key) ? value : null))
  );

  return _.some(loadingValues);
}

function scrollToY(y = 0) {
  if (!_.isFunction(window.scrollTo)) return;

  // It wouldn't scroll on every page, and setting a timeout seems to fix it
  setTimeout(() => {
    try {
      window.scrollTo({ top: y, behavior: 'smooth' });
    } catch (e) {
      window.scrollTo(0, y);
    }
  }, 200);
}

export function scrollToTop() {
  const top = document.getElementById('main-logo').clientHeight + 30;
  scrollToY(top);
}

export function scrollToLogo() {
  const top = document.getElementById('navbar').clientHeight;
  scrollToY(top);
}

export function getAssetURL(iconOrImage) {
  return `${AWS_ASSETS_URL}${iconOrImage}`;
}

export function getParam(param) {
  return decodeURIComponent(
    window.location.search
      .split(`${param}=`)
      .splice(1)
      .join('')
      .split('&')[0] || ''
  ).trim();
}

export function validateStartBoxFile(file) {
  if (!file || !file.size || !_.includes(VALID_FILE_TYPES, file.type)) {
    return FILE_IS_INVALID;
  }

  if (file.size > MAX_FILE_SIZE) {
    return FILE_TOO_BIG_ERROR;
  }

  return null;
}

export const isAfterPriceChange = () => Date.now() >= PRICE_CHANGE_DATE;
/**
 * @returns {Object} { withChip, withoutChip }
 */
export function getCurrentPrice() {
  return isAfterPriceChange() ? PRICES_AFTER_CHANGE : PRICES_BEFORE_CHANGE;
}
