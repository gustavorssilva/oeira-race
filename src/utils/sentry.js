import * as Sentry from '@sentry/browser';
import { IS_PRODUCTION } from './constants';

if (IS_PRODUCTION) {
  // Sentry config
  Sentry.init({
    dsn: 'https://e4eea3c0d2ec4068a3a1327b6d2ba01a@sentry.io/1279417',
    ignoreErrors: [
      'ResizeObserver',
      "Object doesn't support property or method 'find'",
      // Errors caused by deploying the site while people are using it
      "Unexpected token '<'", // Safari
      'Unexpected token <',
      "expected expression, got '<'",
    ],
  });
}

export function reportErrorToSentry(e) {
  if (!IS_PRODUCTION) return;
  Sentry.captureException(e);
}
