export const IS_LOCALHOST = process.env.REACT_APP_ENV === 'local';
export const IS_PRODUCTION = process.env.REACT_APP_ENV === 'production';
export const RESTART_ON_REMOUNT = '@@saga-injector/restart-on-remount';
export const DAEMON = '@@saga-injector/daemon';
export const ONCE_TILL_UNMOUNT = '@@saga-injector/once-till-unmount';

export const DATETIME_REGEX = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}\.\d{3}Z$/;
export const EMAIL_REGEX = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
export const BASIC_PHONE_REGEX = /^([0-9()+-\s]){6,}$/;

export const API_RACE_NAME = `man2019`;
const DEV_URL = 'https://dev.webapi.corridadotejo.com/';
const PROD_URL = 'https://dev.webapi.corridadotejo.com/'
// const PROD_URL = 'https://webapi.corridadotejo.com/';
export const BASE_URL = IS_PRODUCTION ? PROD_URL : DEV_URL;
window.API_URL = BASE_URL;

export const AWS_ASSETS_URL =
  'https://s3-eu-west-1.amazonaws.com/cdn.cdt2018.forkit.pt/assets/website/';
export const PORTUGAL = 'Portugal';
export const MAX_FILE_SIZE = 2000000;
export const FILE_TOO_BIG_ERROR = 'O ficheiro deve ser menor que 2MB';
export const FILE_IS_INVALID = 'Este ficheiro é inválido';
export const VALID_FILE_TYPES = [
  '.jpg',
  '.jpeg',
  '.pdf',
  'image/jpeg',
  'image/jpg',
  'application/pdf',
];

// Price changes on 21/5/2019
export const PRICE_CHANGE_DATE = Date.UTC(2019, 4, 21);
export const PRICES_BEFORE_CHANGE = { withChip: 16, withoutChip: 16 };
export const PRICES_AFTER_CHANGE = { withChip: 16, withoutChip: 16 };

export const HIDE_DORSAL_PREVIEW_BUTTON = true;
