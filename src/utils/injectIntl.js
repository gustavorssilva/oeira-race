import React, { Component } from 'react';
import { injectIntl, intlShape } from 'react-intl';
import allMessages from 'translations/allMessages';

export default (msgPrefix) => (WrappedComponent) => {
  class IntlWrapper extends Component {
    static propTypes = { intl: intlShape.isRequired };

    formatMsg = (id, values = {}) => {
      const { formatMessage } = this.props.intl;
      const msgId = `${msgPrefix}.${id}`;
      const msg = allMessages[msgId] || allMessages[id];

      if (!msg) {
        console.warn(`${msgPrefix}: no message for ${id}`);
        return id;
      }
      return formatMessage(msg, values);
    };

    render() {
      return <WrappedComponent formatMsg={this.formatMsg} {...this.props} />;
    }
  }

  return injectIntl(IntlWrapper);
};
