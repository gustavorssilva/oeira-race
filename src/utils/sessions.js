export const saveItem = (item, data) => localStorage.setItem(item, data);

export const getItem = (item) => localStorage.getItem(item);

export const saveTeam = (data) => saveItem('team', data);

export const getTeam = () => localStorage.getItem('team');

export const saveTeamCode = (data) => saveItem('team_code', data);

export const getTeamCode = () => localStorage.getItem('team_code');
