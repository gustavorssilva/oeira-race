import _ from 'lodash';
import ReactGA from 'react-ga';

// do-not-track checks
function ieTrackingProtectionEnabled() {
  const enabled = _.get(window, 'external.msTrackingProtectionEnabled');
  return _.isFunction(enabled) && enabled();
}

const browserTrackingSettings = [
  navigator.doNotTrack,
  window.doNotTrack,
  navigator.msDoNotTrack,
  ieTrackingProtectionEnabled(),
];

const doNotTrackValues = [
  '1', // modern browsers
  'yes', // old firefox (versions <32)
  true, // internet explorer
];

const includesDoNotTrack = (value) => _.includes(doNotTrackValues, value);

export const userDoesNotWantToBeTracked = _.some(
  browserTrackingSettings,
  includesDoNotTrack
);

export function fireTracking() {
  if (userDoesNotWantToBeTracked || !document.cookie) return;
  ReactGA.pageview(window.location.pathname);
}

export function deleteAllCookies() {
  document.cookie.split(';').forEach((c) => {
    document.cookie = c
      .replace(/^ +/, '')
      .replace(/=.*/, `=;expires=${new Date().toUTCString()};path=/`);
  });

  // and in case that didn't work...
  const cookies = document.cookie.split('; ');
  // eslint-disable-next-line
  for (let c = 0; c < cookies.length; c++) {
    const d = window.location.hostname.split('.');
    while (d.length > 0) {
      const cookieBase = `${encodeURIComponent(
        cookies[c].split(';')[0].split('=')[0]
      )}=; expires=Thu, 01-Jan-1970 00:00:01 GMT; domain=${d.join('.')} ;path=`;
      // eslint-disable-next-line
      const p = location.pathname.split('/');
      document.cookie = `${cookieBase}/`;
      while (p.length > 0) {
        document.cookie = cookieBase + p.join('/');
        p.pop();
      }
      d.shift();
    }
  }
}

export const hasUserAccepted = () => {
  const hasAccepted = localStorage.getItem('acceptedCookies');
  if (!hasAccepted) deleteAllCookies();
  return hasAccepted;
};
