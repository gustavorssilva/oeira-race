import WebFont from 'webfontloader';

WebFont.load({
  google: {
    families: ['Roboto:400,700,900'],
  },
});
