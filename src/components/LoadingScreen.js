import React from 'react';
import styled from 'styled-components';

const LoadingScreen = () => (
  <Container>
    <Spinner />
  </Container>
);

const Container = styled.div`
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  z-index: 100001;

  display: flex;
  justify-content: center;
  align-items: center;

  background-color: #ffffff;
  font-size: 18px;
  padding: 15px;
  text-align: center;
  white-space: pre-wrap;
`;

const Spinner = styled.div`
  @keyframes spin {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }

  border: 3px solid transparent;
  border-top-color: ${({ theme }) => theme.primary};
  border-radius: 50px;
  height: 70px;
  width: 70px;
  animation: spin 0.6s linear infinite;
`;

export default LoadingScreen;
