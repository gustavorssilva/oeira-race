import React from 'react';
import { func, node } from 'prop-types';
import styled from 'styled-components';
import { CloseIcon } from 'assets/icons';

const Modal = ({ onClick, children }) => (
  <Container role="dialog" aria-describedby="modal_content">
    <Button
      type="button"
      onClick={onClick}
      aria-describedby="close_label"
      autoFocus
    >
      <img src={CloseIcon} alt="x" />
      <span id="close_label">Fechar</span>
    </Button>
    <Content id="modal_content">{children}</Content>
  </Container>
);

Modal.propTypes = {
  children: node.isRequired,
  onClick: func.isRequired,
};

const Container = styled.div`
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  z-index: 100000;

  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;

  background-color: rgba(0, 0, 0, 0.85);
  color: white;
  font-size: 18px;
  padding: 15px;
  text-align: center;
  white-space: pre-wrap;
`;

const Button = styled.button`
  height: 50px;
  width: 50px;
  cursor: pointer;
  margin-bottom: 20px;
  padding: 0px;

  span {
    position: absolute;
    left: -10000px;
  }
`;

const Content = styled.div`
  font-size: 85%;

  div {
    margin: 0 auto;
    max-width: 400px; /* IE 11 fix */
  }
`;

export default Modal;
