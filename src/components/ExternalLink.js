import React from 'react';
import { string, node } from 'prop-types';

const ExternalLink = ({ href, children: innerContent }) => (
  <a href={href} target="_blank" rel="noopener noreferrer">
    {innerContent}
  </a>
);

ExternalLink.propTypes = {
  href: string.isRequired,
  children: node.isRequired,
};

export default ExternalLink;
