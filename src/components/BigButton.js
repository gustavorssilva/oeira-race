import React from 'react';
import { bool, func, node, string } from 'prop-types';
import styled, { css } from 'styled-components';

const BigButton = ({ children, disabled, color, onClick }) => {
  return (
    <Button
      color={color}
      onClick={disabled ? null : onClick}
      disabled={disabled}
      type="button"
    >
      {children}
    </Button>
  );
};

BigButton.propTypes = {
  children: node,
  color: string,
  disabled: bool,
  onClick: func,
};

const Button = styled.button`
  background-color: ${({ color, disabled, theme }) =>
    disabled ? theme.inactiveGrey : color || theme.primary};
  border-radius: 30px;
  color: ${({ theme }) => theme.white};
  cursor: ${({ disabled }) => (disabled ? `not-allowed` : `pointer`)};
  font-size: 17px;
  font-weight: 900;
  height: 60px;
  margin: 5px;
  text-align: center;
  text-transform: uppercase;
  transition: all 0.2s ease;
  width: 250px;

  ${({ disabled }) => {
    if (disabled) return '';
    return css`
      &:hover {
        background-color: ${({ theme }) => theme.yellow} !important;
        color: ${({ theme }) => theme.textColor};
      }
    `;
  }};
`;

export default BigButton;
