import styled, { css } from 'styled-components';

export const FullWidthDiv = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  margin: 0 auto;
  width: 100%;
  max-width: 400px;
`;

export const OneColumnGrid = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  padding: 20px 0;
  width: 100%;
`;

export const TextLeft = styled.div`
  text-align: left !important;
`;

export const BoldSpan = styled.span`
  font-weight: 700;
`;

export const BoldText = styled.div`
  font-weight: 700;

  > a {
    margin: 5px auto;
  }
`;

export const CenteredBoldText = styled(BoldText)`
  text-align: center;
  width: inherit;
`;

export const CenteredBlueItalics = styled(CenteredBoldText)`
  font-style: italic;
  color: ${({ theme }) => theme.primary};
`;

export const TextCenter = styled.div`
  text-align: center;
  white-space: pre-line;
`;

export const BoldCaps = styled(CenteredBoldText)`
  text-transform: uppercase;
`;

export const RelativeDiv = styled.div`
  position: relative;
`;

// More specific components
export const TotalPriceDiv = styled(FullWidthDiv)`
  background-color: ${({ theme }) => theme.textColor};
  color: ${({ theme }) => theme.white};
  font-size: 40px;
  font-weight: 900;
  padding: 30px;
  width: 100%;
  margin-bottom: 20px;
  margin-top: 20px;
`;

export const RunnerDetailDiv = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  width: 100%;
  border-bottom: 2px solid ${({ theme }) => theme.primary};

  svg {
    fill: ${({ theme }) => theme.textColor};
  }
`;

export const InvoiceDiv = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  width: 100%;
  margin-bottom: 20px;
  box-shadow: 0 4px 8px -2px rgba(0, 0, 0, 0.1);
`;

export const InvoiceTitleDiv = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  font-weight: bold;
  min-height: 50px;
  color: ${({ show, theme }) => (show ? 'white' : theme.textColor)};
  background-color: ${({ show, theme }) =>
    show ? theme.textColor : '#dddddd'};

  > div:last-child {
    cursor: pointer;
  }
`;

export const InvoiceFormContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  background-color: ${({ theme }) => theme.formFieldGrey};
  border-bottom: 2px solid ${({ theme }) => theme.textColor};
  padding: 0 10px 10px 10px;

  > div {
    margin-top: 20px;
    max-width: 100%; /* IE 11 fix */

    &:last-child {
      margin-top: 10px;
    }
  }
`;

export const SummaryDiv = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  font-weight: 700;
  min-height: 50px;

  > div {
    display: grid;
    align-items: center;
    grid-auto-flow: column;

    &:first-child {
      flex: 1 1 auto;
    }

    &:last-child {
      cursor: pointer;
    }

    &:not(:last-child) {
      padding: 5px 10px;
      align-self: center;
    }
  }

  ${({ warning }) =>
    warning
      ? css`
          color: ${({ theme }) => theme.warningRed};
          svg {
            fill: ${({ theme }) => theme.warningRed};
          }
        `
      : ``};

  button {
    padding: 0;
    border: 0;
  }
`;

export const AllDetailsDiv = styled.div`
  font-weight: bold;
  padding-bottom: 10px;

  div {
    padding: 0 10px;
  }

  span {
    color: ${({ theme }) => theme.primary};
    padding-right: 5px;
  }
`;

export const PaymentsContainer = styled.div`
  position: relative;
  margin: 2.2rem auto;
  width: 100%;

  > div {
    margin-bottom: 1rem;
  }
`;

export const PaymentOptionsDiv = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;

  > button {
    padding: 0;
    margin: 0 5px;
    border-radius: 5px;
    box-shadow: 0 0 0 ${({ theme }) => theme.primary};
    transition: all 0.1s ease-in-out;

    &:hover {
      box-shadow: 0 0 4px ${({ theme }) => theme.primary};
    }
  }
`;

export const SubheadingDiv = styled.div`
  color: ${({ theme }) => theme.primary};
  font-size: 16px;
  font-weight: bold;
  margin-bottom: 5px;
  text-align: center;
`;

export const NavigationButtonsGrid = styled.div`
  display: grid;
  grid-template-columns: 50px 1fr 50px;
  align-items: center;
  justify-items: center;
  width: 100%;
`;

export const DownloadButton = styled.a`
  display: flex;
  align-items: center;
  background-color: ${({ theme }) => theme.primary};
  box-shadow: 0 4px 8px -2px rgba(0, 0, 0, 0.2);
  color: white;
  font-weight: bold;
  margin: 30px 0;
  text-decoration: none;
  width: 100%;

  div {
    display: flex;
    justify-content: center;
    align-items: center;
    height: 50px;
    width: 50px;
  }

  i,
  svg {
    font-size: 30px;
  }
  span {
    line-height: 50px;
    width: 100%;
    text-align: center;
  }
`;

export const TwoButtonDiv = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 0.5rem;
  padding: 0px !important;
  width: 100%;

  > * {
    flex: 1 1 auto;
  }
`;

export const SmallButton = styled(DownloadButton)`
  display: flex;
  justify-content: center;
  align-items: center;
  margin: 10px 4px;

  &:first-child {
    margin-left: 0;
  }
  &:last-child {
    margin-right: 0;
  }

  img,
  svg,
  i {
    flex: 0 0 auto;
  }

  span {
    color: white;
    flex: 1 1 auto;
  }
`;

export const SmallRedButton = styled(SmallButton)`
  background-color: ${({ theme }) => theme.warningRed};
`;

export const SmallText = styled.div`
  font-size: 14px;

  a,
  a:visited,
  a:link {
    color: ${({ theme }) => theme.textColor};
  }
`;

export const ErrorText = styled.div`
  width: 100%;
  font-size: 18px;
  font-weight: bold;
  text-align: center;
  color: ${({ theme }) => theme.errorRed};
  margin-bottom: 30px;
`;

export const TeamTitle = styled.div`
  width: 100%;
  height: 100px;
  line-height: 100px;
  background: ${({ theme }) => theme.textColor};
  color: white;
  font-size: 30px;
  font-weight: 900;
  text-align: center;
  margin-bottom: 30px;
`;

export const TwoColTableGrid = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  grid-gap: 5px;
  background-color: ${({ theme }) => theme.formFieldGrey};
  font-size: 14px;
  padding: 5px;
  width: 100%;

  > div {
    &:nth-child(even) {
      color: ${({ theme }) => theme.primary};
      font-weight: bold;
    }

    /* IE 11 fix */
    @supports (display: grid) {
      &:nth-child(even) {
        text-align: left;
      }
      &:nth-child(odd) {
        text-align: right;
      }
    }
  }

  &.bg-white {
    background-color: #ffffff;
  }
`;

export const WarningText = styled(CenteredBoldText)`
  color: ${({ theme }) => theme.warningRed};
  margin: 10px 0;
`;

export const RelativeFormFieldDiv = styled.div`
  position: relative;
  display: inline-table; /* IE 11 fix */
  line-height: 1; /* IE 11 fix */
  width: 100%;

  input {
    padding-right: 60px !important;
  }
`;

export const TestButton = styled.button`
  position: absolute;
  bottom: 1px;
  right: 1px;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: ${({ theme }) => theme.primary};
  color: white;
  padding: 0;
  height: 50px;
  width: 50px;

  i,
  svg {
    font-size: 20px;
  }

  &:hover {
    cursor: pointer;
  }
`;

export const SidePadded = styled.div`
  padding-left: 20px;
  padding-right: 20px;
`;

export const PaddedSmallText = styled(SmallText)`
  line-height: 1.14;
  padding-left: 20px;
  padding-right: 20px;
  word-break: keep-all;
  max-width: inherit; /* IE 11 fix */
`;

export const InputContainer = styled.div`
  position: relative;
  border: 1px solid ${({ color, theme }) => color || theme.primary};
  box-shadow: 0 4px 8px -2px rgba(0, 0, 0, 0.1);
  margin-top: ${({ lessMargin }) => (lessMargin ? 0.5 : 2.2)}rem;
  transition: all 0.2s ease;
  width: 100%;
  flex: 0 0 auto; /* IE 11 fix */
`;

export const SlightlyPadded = styled.div`
  padding-top: 0.5rem;
  padding-bottom: 0.5rem;
`;

export const MarginTop = styled.div`
  margin-top: 2.2rem;
`;

export const MultibancoDiv = styled.div`
  text-align: center;
  margin-top: 10px;
  margin-bottom: 30px;

  > div {
    display: flex;
    justify-content: center;
    align-items: center;

    > div {
      padding: 0 4px;
      text-align: right;
      width: 100%;

      &:nth-child(even) {
        text-align: left;
        font-weight: bold;
      }
    }
  }
`;
