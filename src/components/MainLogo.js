import React, { PureComponent } from 'react';
import { object } from 'prop-types';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { createStructuredSelector } from 'reselect';
// import { getAssetURL } from 'utils/functions';
import injectReducer from 'utils/injectReducer';
import Logo from 'assets/oeiras-run.jpeg';
import reducer from 'containers/Registration/reducer';
import { selectRunnerDetailsState } from 'containers/Registration/selectors';
import { FullWidthDiv } from 'components/styled';

class MainLogo extends PureComponent {
  render() {
    return (
      <FullWidthDiv>
        <Img id="main-logo" src={Logo} alt="Oeiras run 2019" />
      </FullWidthDiv>
    );
  }
}

// eslint-disable-next-line
MainLogo.propTypes = { runner: object.isRequired };

const Img = styled.img`
  margin-bottom: 1.5rem;
  object-fit: contain;
  height: auto;
  width: 100%;
  max-width: 400px;
`;

const mapStateToProps = createStructuredSelector({
  runner: selectRunnerDetailsState(),
});
const withConnect = connect(mapStateToProps);
const withReducer = injectReducer({ key: 'registration', reducer });

export default compose(
  withReducer,
  withConnect
)(MainLogo);
