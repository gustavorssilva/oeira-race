import _ from 'lodash';
import React, { Component } from 'react';
import { bool, func, object, string } from 'prop-types';
import { PreviewIcon } from 'assets/icons';
import { PORTUGAL, HIDE_DORSAL_PREVIEW_BUTTON } from 'utils/constants';
import { scrollToTop, validateStartBoxFile } from 'utils/functions';
import {
  BackButton,
  BigButton,
  DatePicker,
  FormField,
  HelpIcon,
  Modal,
  RadioSelect,
  UploadField,
  SecondaryButton,
  DorsalPreview,
} from 'components';
import {
  CenteredBoldText,
  FullWidthDiv,
  OneColumnGrid,
} from 'components/styled';
import {
  ID_NUMBER,
  CHIP,
  DOB,
  FULL_NAME,
  COUNTRY_OF_RESIDENCE,
  DISTRICT,
  EMAIL,
  EMAIL_CONFIRMATION,
  PHONE,
  GENDER,
  SHIRT_SIZE,
  DORSAL_NAME,
  START_BOX,
  EVIDENCE_UPLOAD,
  OPTIONS_BY_FIELD,
  MODAL_MSGS,
  START_BOX_GENERAL,
  DROPDOWN_OPTIONS_BY_FIELD,
  DETAILS_CONFIRMATION_URL,
  NON_EDITABLE_FIELDS,
} from 'containers/Registration/constants';
import messages from 'containers/Registration/messages';
import { enableShirtOption, hasChip } from 'containers/Registration/utils';
import OptIns from '../OptIns';
import Select from '../Select';

class RunnerForm extends Component {
  state = { modalContent: '', showDorsalPreview: false };

  componentWillMount() {
    const {
      runner,
      history: { push },
    } = this.props;
    if (!runner) return push('/');
    return scrollToTop();
  }

  getShirtSizes() {
    const {
      shirtSizesState: { data },
      runner,
    } = this.props;

    const sizeData = _.find(data, { type: runner.type, gender: runner.gender });
    return _.get(sizeData, 'sizes') || [];
  }

  setErrorMsg = (error) => this.setState({ modalContent: error });

  handleContinue = () => {
    const {
      editingExistingRegistration,
      findErrors,
      handleContinue,
      history: { push },
      runner,
    } = this.props;
    const errors = findErrors(runner);
    if (errors) return this.setErrorMsg(errors);

    return editingExistingRegistration
      ? handleContinue()
      : push(DETAILS_CONFIRMATION_URL);
  };

  handleImageChange(e, field) {
    const { updateRunner } = this.props;
    const file = e.target.files[0];

    const error = validateStartBoxFile(file);
    if (error) return this.setErrorMsg(error);

    return updateRunner({ [field]: file });
  }

  renderContinueButton() {
    const { formatMsg, editingExistingRegistration } = this.props;

    const buttonText = editingExistingRegistration ? 'save' : 'continue';

    return (
      <BigButton onClick={this.handleContinue}>
        {formatMsg(buttonText)}
      </BigButton>
    );
  }

  renderHelpIcon(field) {
    const modalContent = this.props.formatMsg(MODAL_MSGS[field]);

    return <HelpIcon onClick={() => this.setState({ modalContent })} />;
  }

  renderHelpModal() {
    const { modalContent } = this.state;
    if (!modalContent) return null;

    return (
      <Modal onClick={() => this.setState({ modalContent: '' })}>
        {modalContent}
      </Modal>
    );
  }

  renderDorsalPreview() {
    const { showDorsalPreview } = this.state;
    const { runner } = this.props;
    if (!showDorsalPreview) return null;
    return (
      <DorsalPreview
        runner={runner}
        handleClose={() => this.setState({ showDorsalPreview: false })}
      />
    );
  }

  renderDatePicker(field) {
    const {
      formatMsg,
      runner,
      updateRunner,
      editingExistingRegistration,
    } = this.props;

    return (
      <DatePicker
        disabled={editingExistingRegistration}
        placeholder={formatMsg(`short_${field}`)}
        date={runner[field]}
        onChange={(value) => updateRunner({ [field]: value })}
      />
    );
  }

  renderDropdown(field) {
    const { formatMsg, runner, updateRunner } = this.props;

    if (field === DISTRICT && runner[COUNTRY_OF_RESIDENCE] !== PORTUGAL) {
      return null;
    }

    const options = DROPDOWN_OPTIONS_BY_FIELD[field];

    return (
      <Select
        value={runner[field] || ''}
        placeholder={formatMsg(field)}
        onChange={(value) => updateRunner({ [field]: value })}
        options={options}
        noOptionsMessage={formatMsg(`no_matches`)}
      />
    );
  }

  renderRadioSelect(field, hasHelper) {
    const {
      formatMsg,
      runner,
      updateRunner,
      editingExistingRegistration,
    } = this.props;

    if (editingExistingRegistration && _.includes(NON_EDITABLE_FIELDS, field)) {
      return null;
    }

    const isShirt = field === SHIRT_SIZE;
    if (isShirt && (!runner[GENDER] || !runner[CHIP])) return null;

    const options = _.map(OPTIONS_BY_FIELD[field], (option) => {
      let text = ``;
      let value = option;

      if (_.isPlainObject(option)) {
        value = option.value;
        text = formatMsg(option.text);
      } else {
        text = !isShirt && messages[value] ? formatMsg(value) : value;
      }

      const isDisabled =
        isShirt && !enableShirtOption(runner, this.getShirtSizes(), value);

      return { text, value, isDisabled };
    });

    const extraInfo =
      field === START_BOX ? formatMsg(`start_box_explanation`) : null;

    return (
      <RadioSelect
        title={formatMsg(field)}
        options={options}
        selected={runner[field]}
        handleClick={(option) => updateRunner({ [field]: option })}
        extraInfo={extraInfo}
      >
        {hasHelper && this.renderHelpIcon(field)}
      </RadioSelect>
    );
  }

  renderUploadField(field) {
    const { formatMsg, runner } = this.props;
    if (!runner[START_BOX] || runner[START_BOX] === START_BOX_GENERAL) {
      return null;
    }

    return (
      <UploadField
        field={field}
        text={formatMsg(field)}
        file={runner[field]}
        onChange={(e) => this.handleImageChange(e, field)}
        fileTypes={['.jpg', '.jpeg', '.pdf']}
      />
    );
  }

  renderField(field, type) {
    const {
      formatMsg,
      runner,
      updateRunner,
      editingExistingRegistration,
    } = this.props;
    const disabled = editingExistingRegistration && field === ID_NUMBER;

    return (
      <FormField
        disabled={disabled}
        field={field}
        placeholder={formatMsg(field)}
        value={runner[field] || ''}
        onChange={(e) => updateRunner({ [field]: e.target.value })}
        type={type || 'text'}
      />
    );
  }

  renderDorsalPreviewButton() {
    if (HIDE_DORSAL_PREVIEW_BUTTON) return null;
    const { formatMsg } = this.props;

    return (
      <SecondaryButton
        onClick={() => this.setState({ showDorsalPreview: true })}
        icon={<img src={PreviewIcon} alt="icon_preview" />}
      >
        {formatMsg('preview_dorsal')}
      </SecondaryButton>
    );
  }

  renderFields() {
    const { runner } = this.props;
    const withChip = hasChip(runner);

    return (
      <OneColumnGrid>
        {this.renderField(ID_NUMBER)}
        {this.renderDatePicker(DOB)}
        {this.renderField(FULL_NAME)}
        {this.renderDropdown(COUNTRY_OF_RESIDENCE)}
        {this.renderDropdown(DISTRICT)}
        {this.renderField(EMAIL, 'email')}
        {this.renderField(EMAIL_CONFIRMATION, 'email')}
        {this.renderField(PHONE)}
        {this.renderRadioSelect(GENDER)}
        {this.renderRadioSelect(SHIRT_SIZE)}
        {this.renderField(DORSAL_NAME)}
      </OneColumnGrid>
    );
  }

  renderOptIns() {
    const {
      runner,
      updateRunner,
      formatMsg,
      formatHTMLMsg,
      editingExistingRegistration,
    } = this.props;

    return (
      <OptIns
        showTermsAndPrivacyOptIns={!editingExistingRegistration}
        registrationObject={runner}
        updateRegistration={updateRunner}
        formatMsg={formatMsg}
        formatHTMLMsg={formatHTMLMsg}
      />
    );
  }

  render() {
    const { goBack, formatMsg, runner, title } = this.props;
    if (!runner) return null;

    return (
      <FullWidthDiv>
        <BackButton onClick={goBack} />
        <h1>{title}</h1>
        <CenteredBoldText style={{ fontSize: '18px' }}>
          {formatMsg('fill_all_fields')}
        </CenteredBoldText>

        {this.renderFields()}
        {this.renderOptIns()}

        {this.renderContinueButton()}
        {this.renderHelpModal()}
        {this.renderDorsalPreview()}
      </FullWidthDiv>
    );
  }
}

RunnerForm.propTypes = {
  editingExistingRegistration: bool,
  formatMsg: func.isRequired,
  formatHTMLMsg: func.isRequired,
  goBack: func.isRequired,
  handleContinue: func,
  history: object.isRequired,
  updateRunner: func.isRequired,
  findErrors: func.isRequired,
  runner: object.isRequired,
  shirtSizesState: object.isRequired,
  title: string.isRequired,
};

export default RunnerForm;
