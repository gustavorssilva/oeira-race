import React from 'react';
import { bool, string } from 'prop-types';

const EmptyOption = ({ text, isRequired }) => (
  <option value="" disabled={isRequired} hidden={isRequired}>
    {text}
  </option>
);

EmptyOption.propTypes = {
  isRequired: bool,
  text: string,
};

EmptyOption.defaultProps = {
  isRequired: false,
  text: '',
};

export default EmptyOption;
