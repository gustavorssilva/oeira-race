import _ from 'lodash';
import React, { Component } from 'react';
import { bool, func, number, oneOfType, string, array } from 'prop-types';
import ReactSelect from 'react-select';
import { InformationVariantIcon } from 'mdi-react';
import { textColor, primary, fadedPrimary } from 'styles/colors';
import { InputContainer } from 'components/styled';
import { Label, HelpButton, customStyles } from './styled';
import EmptyOption from './EmptyOption';

class Select extends Component {
  static propTypes = {
    label: string,
    value: oneOfType([bool, number, string]),
    onChange: func.isRequired,
    noOptionsMessage: string.isRequired,
    placeholder: string,
    options: array,
    handleHelpClick: func,
    alwaysActive: bool,
    disabled: bool,
    isRequired: bool,
    noEmptyOption: bool,
  };

  static defaultProps = {
    value: '',
    label: '',
    placeholder: '',
    options: [],
    handleHelpClick: undefined,
    alwaysActive: false,
    disabled: false,
    isRequired: false,
    noEmptyOption: false,
  };

  state = { active: false };

  shouldComponentUpdate(nextProps, nextState) {
    const { alwaysActive, value } = this.props;
    const { active } = this.state;
    const activeChanged = !alwaysActive && active !== nextState.active;
    const valueChanged = value !== nextProps.value;
    return activeChanged || valueChanged;
  }

  getActiveClass = () => {
    const { alwaysActive, value } = this.props;
    const { active } = this.state;

    return alwaysActive || active || value ? 'active' : '';
  };

  renderEmptyOption() {
    const { placeholder, isRequired, noEmptyOption } = this.props;
    if (noEmptyOption) return null;

    return (
      <EmptyOption key="empty" text={placeholder} isRequired={isRequired} />
    );
  }

  renderOptions() {
    const { options } = this.props;
    const selectables = options.map((option) => {
      const [value, text] = _.isString(option)
        ? [option, option]
        : [option.value, option.label];

      return (
        <option key={value} value={value}>
          {text}
        </option>
      );
    });

    return [this.renderEmptyOption(), ...selectables];
  }

  renderNewOptions() {
    const { options } = this.props;

    return options.map((option) => {
      const [value, label] = _.isString(option)
        ? [option, option]
        : [option.value, option.label];

      return { value, label };
    });
  }

  renderHelpButton() {
    const { handleHelpClick } = this.props;

    if (!handleHelpClick) return null;

    return (
      <HelpButton
        onClick={handleHelpClick}
        type="button"
        className={this.getActiveClass()}
      >
        <InformationVariantIcon />
      </HelpButton>
    );
  }

  render() {
    const {
      label,
      value,
      onChange,
      disabled,
      noOptionsMessage,
      placeholder,
      ...rest
    } = this.props;
    const activeClass = this.getActiveClass();
    const options = this.renderNewOptions();

    return (
      <InputContainer {...rest} className="active">
        {this.renderHelpButton()}
        <Label>{label || placeholder}</Label>

        <ReactSelect
          styles={customStyles}
          active={!!activeClass}
          disabled={disabled}
          placeholder={placeholder}
          value={_.filter(options, ['value', value])}
          options={options}
          noOptionsMessage={() => noOptionsMessage}
          onChange={({ value: v = '' }) => onChange(v)}
          onFocus={() => this.setState({ active: true })}
          onBlur={() => this.setState({ active: false })}
          theme={(theme) => ({
            ...theme,
            colors: {
              ...theme.colors,
              primary,
              primary25: fadedPrimary,
              text: textColor,
            },
          })}
        />
      </InputContainer>
    );
  }
}

export default Select;
