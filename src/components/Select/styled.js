import styled from 'styled-components';
import { textColor, primary } from 'styles/colors';

export const Label = styled.label`
  position: absolute;
  top: -0.6em;
  left: 0.7em;
  font-size: 0.8rem;
  line-height: 1;
  padding: 0 0.5em;
  background-color: #ffffff;
  color: ${primary};
  font-weight: 700;
  z-index: 2;
`;

export const HelpButton = styled.button`
  position: absolute;
  top: 1px;
  right: -7px;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: ${textColor};
  color: white;
  border: 0;
  border-radius: 20px;
  z-index: 2;
  height: 20px;
  width: 20px;
  padding: 0px;

  &.active {
    background-color: ${primary};
  }

  svg {
    height: 18px;
    width: 18px;
    fill: white;
  }
`;

export const customStyles = {
  container: (base, state) => {
    const {
      selectProps: { disabled },
    } = state;
    return {
      ...base,
      backgroundColor: 'white',
      border: 0,
      borderRadius: 0,
      color: textColor,
      opacity: disabled ? 0.5 : 1,
      padding: 0,
    };
  },
  input: (base) => ({
    ...base,
    color: textColor,
    fontSize: 16,
    border: 0,
    margin: 0,
  }),
  valueContainer: (base) => ({
    ...base,
    color: textColor,
    border: 0,
    fontSize: 16,
    height: 50,
    padding: `0 1rem`,
  }),
  control: (base) => ({
    ...base,
    border: '0px !important',
    borderWidth: '0px !important',
    borderRadius: 0,
    borderColor: 'transparent !important',
    height: '100%',
  }),
  menu: (base) => ({
    ...base,
    borderRadius: 0,
    margin: 0,
    zIndex: 9000,
    backgroundColor: `#ffffff`,
    border: `1px solid ${primary}`,
    borderTop: 0,
    left: -1,
    width: `calc(100% + 2px)`,
  }),
  singleValue: (base) => ({
    ...base,
    color: textColor,
    fontSize: 16,
  }),
  noOptionsMessage: (base) => ({
    ...base,
    fontSize: 16,
    fontWeight: 600,
    backgroundColor: primary,
    color: 'white',
  }),
  option: (base) => ({
    ...base,
    fontSize: 16,
  }),
  dropdownIndicator: (base) => ({
    ...base,
    padding: 2,
    cursor: 'pointer',
  }),
};
