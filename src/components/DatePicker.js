import _ from 'lodash';
import React, { Component } from 'react';
import { bool, func, string } from 'prop-types';
import styled from 'styled-components';
import MaskedInput from 'react-text-mask';
import { CalendarIcon } from 'assets/icons';

function convertFromISO(date) {
  if (!date) return '';
  const [year, month, day] = date.split(/\D/);
  return `${day}/${month}/${year}`;
}

function validateDate(date) {
  const [day, month, year] = date.split(/\D/);
  if (!_.every([year, month, day])) return false;

  const validDay = day.length === 2 && _.inRange(_.toNumber(day), 32);
  const validMonth = month.length === 2 && _.inRange(_.toNumber(month), 13);
  const validYear =
    year.length === 4 && _.inRange(_.toNumber(year), 1900, 2020);

  return validDay && validMonth && validYear;
}

function getValidDate(date) {
  if (!validateDate(date)) return false;
  const [day, month, year] = date.split(/\D/);
  const dateParts = [year, month, day];
  if (year.length !== 4) dateParts.reverse();

  return _.map(dateParts, (part, index) => {
    const number = _.toNumber(part);
    return index === 1 ? number - 1 : number;
  });
}

const DATE_MASK = [
  /[0-3]/,
  /\d/,
  '/',
  /[0-1]/,
  /\d/,
  '/',
  /[1-2]/,
  /[(0|1|9)]/,
  /\d/,
  /\d/,
];
class DatePicker extends Component {
  static propTypes = {
    disabled: bool,
    placeholder: string,
    date: string,
    onChange: func.isRequired,
  };

  state = { date: convertFromISO(this.props.date), touched: false };

  setTouched = () => {
    const { touched } = this.state;
    if (touched) return;
    this.setState({ touched: true });
  };

  handleChange = (e) => {
    const { onChange } = this.props;
    const { value } = e.target;
    const validDate = getValidDate(value);

    this.setState({ date: value });
    onChange(validDate ? new Date(Date.UTC(...validDate)).toISOString() : '');
  };

  render() {
    const { placeholder, disabled } = this.props;
    const { date, touched } = this.state;
    const className = disabled ? 'disabled' : '';

    return (
      <InputContainer
        isValid={!touched || !!getValidDate(date)}
        disabled={disabled}
      >
        <Label>{placeholder} (DD/MM/AAAA)</Label>
        <MaskedInput
          mask={DATE_MASK}
          id="b_date"
          placeholder="DD/MM/AAAA"
          onChange={this.handleChange}
          onFocus={this.setTouched}
          value={date}
          className={className}
          disabled={disabled}
          render={(ref, props) => <DateInput ref={ref} {...props} />}
        />
        <IconContainer>
          <img src={CalendarIcon} alt="" />
        </IconContainer>
      </InputContainer>
    );
  }
}

const InputContainer = styled.div`
  position: relative;
  display: flex;
  justify-content: space-between;
  align-items: center;
  background-color: #ffffff;
  border: 1px solid
    ${({ isValid, theme }) => (isValid ? theme.primary : theme.errorRed)};
  margin-top: 2.2rem;
  min-height: 50px;
  width: 100%;
  box-shadow: 0 4px 8px -2px rgba(0, 0, 0, 0.1);
`;

const Label = styled.div`
  position: absolute;
  top: -0.6em;
  left: 0.7em;
  font-size: 0.8rem;
  line-height: 1;
  padding: 0 0.5em;
  background-color: #ffffff;
  color: ${({ theme }) => theme.primary};
  font-weight: 700;
`;

const DateInput = styled.input`
  background-color: #ffffff;
  border: 0px;
  border-radius: 0px !important;
  color: ${({ theme }) => theme.textColor};
  font-size: 100%;
  font-family: Roboto !important;
  padding: 15px 1rem;
  width: 100%;
`;

const IconContainer = styled.div`
  position: absolute;
  right: 0;
  top: 0;
  bottom: 0;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 50px;
  font-size: 30px;

  i:hover,
  svg:hover {
    cursor: pointer;
  }
`;

export default DatePicker;
