import _ from 'lodash';
import React, { Component, Fragment } from 'react';
import { array, func, node, number, object, string } from 'prop-types';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import { bindActionCreators, compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { CheckIcon, CloseIcon, LoadingIcon } from 'mdi-react';
import injectIntl from 'utils/injectIntl';
import { scrollToTop } from 'utils/functions';
import {
  BackButton,
  BigButton,
  Modal,
  RunnersInfoList,
  InvoiceForm,
  PaymentTypeSelector,
  PromoCodeHandler,
} from 'components';
import {
  BoldSpan,
  BoldText,
  FullWidthDiv,
  TotalPriceDiv,
  TextLeft,
  TeamTitle,
  TwoColTableGrid,
  TwoButtonDiv,
  SmallButton,
  SlightlyPadded,
} from 'components/styled';
import * as actions from 'containers/Registration/actions';
import {
  CREDIT_CARD,
  PAYMENT_METHOD,
  KIT_DELIVERY,
  MANAGER_NAME,
  INVOICE_FIELDS,
} from 'containers/Registration/constants';
import {
  hasAllInvoiceInfo,
  hasIncompleteInvoiceInfo,
  getRunnerPrice,
} from 'containers/Registration/utils';
import {
  selectShirtsState,
  selectPaymentStatusState,
  selectCurrentRunnerIndex,
  selectPromoCode,
  selectRegistrationPostState,
  selectAppliedPromoCode,
} from 'containers/Registration/selectors';
import { selectTeamRegistrationState } from 'containers/Teams/selectors';

class DetailsConfirmationPage extends Component {
  static propTypes = {
    currentRunnerIndex: number.isRequired,
    formatMsg: func.isRequired,
    setRunnerIndex: func.isRequired,
    applyPromoCode: func.isRequired,
    allRunners: array.isRequired,
    promoCode: string,
    shirtSizesState: object.isRequired,
    appliedPromoCode: object.isRequired,
    history: object.isRequired,
    mainData: object.isRequired,
    regPostState: object.isRequired,
    teamPostState: object.isRequired,
    handleContinue: func.isRequired,
    updateMainData: func.isRequired,
    previousPage: string.isRequired,
    title: string.isRequired,
    dismissError: func.isRequired,
    handleAddRunner: func,
    handleEditClick: func,
    handleFileChange: func,
    handleDeleteClick: func,
    teamName: string,
    errorMsg: node,
  };

  static defaultProps = {
    errorMsg: '',
    teamName: undefined,
    handleAddRunner: undefined,
    handleEditClick: undefined,
    handleDeleteClick: undefined,
  };

  state = {
    showDetails: null,
    selectedPayment: null,
    setFinalConfirmationModal: false,
  };

  componentWillMount() {
    const {
      currentRunnerIndex,
      setRunnerIndex,
      allRunners,
      mainData,
      updateMainData,
      promoCode,
      applyPromoCode,
      appliedPromoCode,
    } = this.props;

    if (currentRunnerIndex !== 0) {
      setRunnerIndex(0);
    } else if (!mainData.discount_code && promoCode) {
      updateMainData({ discount_code: promoCode });
    }

    if (promoCode && !appliedPromoCode.data) {
      applyPromoCode({ promoCode, registrations: allRunners });
    }

    return scrollToTop();
  }

  componentWillReceiveProps(nextProps) {
    this.checkIfFree(nextProps);
  }

  getPromoCodePrices() {
    const { data } = this.props.appliedPromoCode;
    return _.get(data, 'breakdown') || {};
  }

  getNumberOfRunners = () => this.props.allRunners.length;

  setFinalConfirmationModal = (showFinalConfirmation = true) => () =>
    this.setState({ showFinalConfirmation });

  handleContinue = () => {
    const { handleContinue } = this.props;

    this.setState({ showFinalConfirmation: false });
    handleContinue();
  };

  checkIfFree(props) {
    const { appliedPromoCode, updateMainData, mainData } = props;

    if (!this.isOrderFree(appliedPromoCode)) return;

    if (!mainData[PAYMENT_METHOD]) {
      const newDetails = { ...mainData, [PAYMENT_METHOD]: CREDIT_CARD };
      this.setState({ selectedPayment: 'visa' });
      updateMainData(newDetails);
    }
  }

  isOrderFree(appliedPromoCode) {
    const dataToCheck = appliedPromoCode || this.props.appliedPromoCode;
    const totalPriceWithPromoCode = _.get(dataToCheck, 'data.total_price');
    return totalPriceWithPromoCode === 0;
  }

  calculateTotalPrice() {
    const {
      allRunners,
      appliedPromoCode: { data },
    } = this.props;
    const normalTotal = _.sum(_.map(allRunners, getRunnerPrice));

    // In case the promo-code total is 0, convert to a string
    // before checking which price to show
    return _.toString(data && data.total_price) || normalTotal;
  }

  renderFinalConfirmation() {
    if (!this.state.showFinalConfirmation) return null;
    const { formatMsg, mainData } = this.props;
    const closeModal = this.setFinalConfirmationModal(false);

    if (hasIncompleteInvoiceInfo(mainData)) {
      return (
        <Modal onClick={closeModal}>{formatMsg(`check_invoice_fields`)}</Modal>
      );
    }

    if (!mainData.discount_code) {
      return (
        <Modal onClick={closeModal}>Só é possível concluir a inscrição com o código disponibilizado no stand da CMO na feira da MMLx</Modal>
      );
    }

    const wantsInvoice = hasAllInvoiceInfo(mainData);
    const invoiceInfo = wantsInvoice ? (
      _.map(_.pick(mainData, INVOICE_FIELDS), (value, key) => (
        <TextLeft key={key}>
          <span style={{ marginRight: `0.5rem` }}>{formatMsg(key)}</span>
          <BoldSpan>{value}</BoldSpan>
        </TextLeft>
      ))
    ) : (
      <BoldText>{formatMsg(`invoice_confirmation_no_info`)}</BoldText>
    );

    return (
      <Modal onClick={closeModal}>
        <FullWidthDiv>
          <BoldText>{formatMsg(`final_confirmation`)}</BoldText>
          {wantsInvoice && (
            <div style={{ maxWidth: `100%` }}>
              {formatMsg(`please_confirm_the_following_data`)}
            </div>
          )}

          <SlightlyPadded>{invoiceInfo}</SlightlyPadded>

          <TwoButtonDiv>
            <SmallButton onClick={closeModal}>
              <CloseIcon size={50} style={{ padding: 5 }} />
              <span>{formatMsg(`cancel`)}</span>
            </SmallButton>
            <SmallButton onClick={this.handleContinue}>
              <CheckIcon size={50} style={{ padding: 5 }} />
              <span>{formatMsg(`continue`)}</span>
            </SmallButton>
          </TwoButtonDiv>
        </FullWidthDiv>
      </Modal>
    );
  }

  renderContinueButton() {
    const { formatMsg, mainData, regPostState, teamPostState } = this.props;
    const isPostingReg = regPostState.requesting || teamPostState.requesting;
    const msgId = this.isOrderFree() ? 'finalize' : 'pay';

    return (
      <BigButton
        disabled={!mainData[PAYMENT_METHOD] || isPostingReg}
        onClick={this.setFinalConfirmationModal()}
      >
        {isPostingReg ? <LoadingIcon className="spin" /> : formatMsg(msgId)}
      </BigButton>
    );
  }

  renderErrorMsg() {
    const { errorMsg, dismissError, formatMsg } = this.props;
    if (!errorMsg) return null;
    const error = errorMsg.message ? formatMsg(errorMsg.message) : errorMsg;

    return <Modal onClick={dismissError}>{error}</Modal>;
  }

  renderTeamInfo() {
    const { formatMsg, mainData, teamName } = this.props;
    if (!teamName) return null;
    const kitCollectionMethod = _.capitalize(formatMsg(mainData[KIT_DELIVERY]));

    return (
      <Fragment>
        <TeamTitle style={{ margin: 0 }}>{teamName}</TeamTitle>
        <TwoColTableGrid>
          <div>{formatMsg('team_leader')}:</div>
          <div>{mainData[MANAGER_NAME]}</div>
          <div>{formatMsg('kit_collection_method_short')}:</div>
          <div>{kitCollectionMethod}</div>
        </TwoColTableGrid>
      </Fragment>
    );
  }

  render() {
    const {
      allRunners,
      history: { push },
      mainData,
      handleAddRunner,
      handleEditClick,
      handleFileChange,
      handleDeleteClick,
      updateMainData,
      previousPage,
      shirtSizesState,
      title,
    } = this.props;
    const totalPrice = this.calculateTotalPrice();

    return (
      <FullWidthDiv>
        <BackButton onClick={() => push(previousPage)} />

        {this.renderTeamInfo()}

        <h1>{title}</h1>

        <TotalPriceDiv>16€</TotalPriceDiv>
        <InvoiceForm invoicee={mainData} handleChange={updateMainData} />
        <PromoCodeHandler
          allRunners={allRunners}
          handleUpdate={updateMainData}
          payer={mainData}
        />
        <PaymentTypeSelector
          payer={mainData}
          handleChange={updateMainData}
          isFree={this.isOrderFree()}
        />

        {this.renderContinueButton()}
        {this.renderErrorMsg()}
        {this.renderFinalConfirmation()}
      </FullWidthDiv>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  paymentStatusState: selectPaymentStatusState(),
  currentRunnerIndex: selectCurrentRunnerIndex(),
  promoCode: selectPromoCode(),
  appliedPromoCode: selectAppliedPromoCode(),
  shirtSizesState: selectShirtsState(),
  regPostState: selectRegistrationPostState(),
  teamPostState: selectTeamRegistrationState(),
});

const mapDispatchToProps = (dispatch) => bindActionCreators(actions, dispatch);

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
const withIntl = injectIntl('DetailsConfirmationPage');

export default compose(
  withConnect,
  withRouter,
  withIntl
)(DetailsConfirmationPage);
