import React from 'react';
import { func } from 'prop-types';
import styled from 'styled-components';
import { BackIcon } from 'assets/icons';

const BackButton = ({ onClick }) => (
  <Button type="button" onClick={onClick}>
    <img src={BackIcon} alt="voltar" />
    <span>Voltar</span>
  </Button>
);

BackButton.propTypes = {
  onClick: func.isRequired,
};

const Button = styled.button`
  align-self: flex-start;
  margin-bottom: 1.5rem;
  padding: 0px;

  span {
    position: absolute;
    left: -10000px;
  }
`;

export default BackButton;
