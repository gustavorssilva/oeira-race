import React from 'react';
import { string } from 'prop-types';
import { FormattedMessage } from 'react-intl';
import { FullWidthDiv, TextCenter } from 'components/styled';
import messages from 'containers/Registration/messages';

const CheckYourEmail = ({ title, toDoWhat }) => (
  <FullWidthDiv>
    <h1>{title}</h1>
    <TextCenter>
      <FormattedMessage {...messages.check_your_email} values={{ toDoWhat }} />
    </TextCenter>
  </FullWidthDiv>
);

CheckYourEmail.propTypes = {
  title: string,
  toDoWhat: string,
};

export default CheckYourEmail;
