/*
 * HomePage Messages
 *
 * This contains all the text for the HomePage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.DorsalPreview.header',
    defaultMessage: 'DorsalPreview',
  },
  dorsal_name: {
    id: 'app.components.DorsalPreview.dorsal_name',
    defaultMessage: 'Nome Utilizador',
  },
  share_dorsal: {
    id: 'app.components.DorsalPreview.share_dorsal',
    defaultMessage: 'Partilhar Dorsal',
  },
  download_dorsal: {
    id: 'app.components.DorsalPreview.download_dorsal',
    defaultMessage: 'Baixar',
  },
});
