import React, { Component } from 'react';
import { func, object } from 'prop-types';
import styled from 'styled-components';
import { CloseIcon, DorsalSVG } from 'assets/icons';
import html2canvas from 'html2canvas';
import { START_BOX } from 'containers/Registration/constants';
import { injectIntl, intlShape } from 'react-intl';
import { getAssetURL } from 'utils/functions';
import messages from './messages';

class DorsalPreview extends Component {
  componentDidMount() {
    html2canvas(this.canvasFragment).then((canvas) => {
      const canvasPng = canvas.toDataURL('image/png');
      console.log(canvasPng);
      this.dorsalAnchor.href = canvasPng;
    });
  }

  render() {
    const {
      handleClose,
      intl: { formatMessage },
      runner,
    } = this.props;
    const dorsalName =
      runner.dorsal_name || formatMessage(messages.dorsal_name);
    const [firstName = ``, lastName = ``] = dorsalName.split(' ');
    const runnerNumber = `00000`;
    const startBox = runner[START_BOX] || `>60`;
    const [symbol, ...number] = startBox;

    return (
      <Container>
        <Box role="button" onClick={() => handleClose()}>
          <img src={CloseIcon} alt="X" />
        </Box>
        <div
          ref={(c) => {
            this.canvasFragment = c;
          }}
        >
          <CanvasFragment>
            <RunnerRace>
              <img
                src={getAssetURL(`icons/cx-${startBox}.svg`)}
                alt={`Caixa de Partida ${
                  symbol === `>` ? `mais` : `menos`
                } ${number}`}
              />
              <img
                src={getAssetURL('icons/icon_race_group_2.svg')}
                alt="Vaga de partida 2"
              />
            </RunnerRace>
            <RunnerNumber
              ref={(c) => {
                this.dorsalAnchor = c;
              }}
            >
              {runnerNumber}
            </RunnerNumber>
            <DorsalName>
              <span>{firstName.slice(0, 20)}</span>
              <span>{lastName.slice(0, 20)}</span>
            </DorsalName>
          </CanvasFragment>
        </div>
      </Container>
    );
  }
}

DorsalPreview.propTypes = {
  runner: object,
  handleClose: func,
  intl: intlShape.isRequired,
};

const RunnerRace = styled.div`
  position: absolute;
  top: 21px;
  left: 15px;
  width: 44px;

  > img {
    width: 100%;
    height: auto;
    margin-bottom: 5px;
  }
`;

const RunnerNumber = styled.div`
  font-size: 27px;
  font-weight: 500;
  color: black;
  position: absolute;
  top: 89px;
  right: 13px;
`;

const DorsalName = styled.div`
  position: absolute;
  max-width: 200px;
  bottom: 17px;
  left: 15px;
  font-size: 34px;
  font-weight: 500;
  line-height: 33px;
  text-align: left;
  color: white;
  > span {
    width: 200px;
    float: left;
  }
`;

const CanvasFragment = styled.div`
  position: relative;
  width: 301px;
  height: 229px;
  background: url(${getAssetURL(DorsalSVG)});
`;

const Container = styled.div`
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  z-index: 100000;

  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;

  background-color: rgba(0, 0, 0, 0.8);
  color: white;
  font-size: 18px;
  padding: 15px;
  text-align: center;
  white-space: pre-wrap;
`;

const Box = styled.div`
  font-size: 30px;
  cursor: pointer;
  margin-bottom: 30px;
`;

export default injectIntl(DorsalPreview);
