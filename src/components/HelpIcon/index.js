import React from 'react';
import { func } from 'prop-types';
import { HelpButton, StyledIcon } from './styled';

const HelpIcon = ({ onClick }) => (
  <HelpButton type="button" onClick={onClick}>
    <StyledIcon size={18} />
  </HelpButton>
);

HelpIcon.propTypes = { onClick: func.isRequired };

export default HelpIcon;
