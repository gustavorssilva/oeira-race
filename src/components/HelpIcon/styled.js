import styled from 'styled-components';
import { HelpCircleIcon } from 'mdi-react';

export const StyledIcon = styled(HelpCircleIcon)`
  fill: ${({ theme }) => theme.textColor} !important;
`;

export const HelpButton = styled.button`
  position: absolute;
  top: -0.6rem;
  right: -0.6rem;
  z-index: 9000;

  background-color: #ffffff;
  line-height: 0;
  padding: 0;
`;
