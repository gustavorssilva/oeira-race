import React, { Component, Fragment } from 'react';
import { bool, func, object } from 'prop-types';
import injectIntl from 'utils/injectIntl';
import {
  VisaOn,
  VisaOff,
  MultibancoOn,
  MultibancoOff,
  MasterCardOn,
  MasterCardOff,
} from 'assets/icons';
import {
  FullWidthDiv,
  PaymentsContainer,
  SubheadingDiv,
  PaymentOptionsDiv,
  PaddedSmallText,
} from 'components/styled';
import {
  MULTIBANCO,
  CREDIT_CARD,
  PAYMENT_METHOD,
} from 'containers/Registration/constants';

class PaymentTypeSelector extends Component {
  static propTypes = {
    formatMsg: func.isRequired,
    payer: object.isRequired,
    handleChange: func.isRequired,
    isFree: bool,
  };

  static defaultProps = {
    isFree: false,
  };

  state = { selectedPayment: null };

  handlePaymentChange = (value, selectedPayment) => () => {
    const { payer, handleChange } = this.props;
    this.setState({ selectedPayment });
    handleChange({ ...payer, [PAYMENT_METHOD]: value });
  };

  renderPaymentWarning() {
    const { formatMsg, payer, isFree } = this.props;
    if (isFree || !payer[PAYMENT_METHOD]) return null;

    return (
      <FullWidthDiv>
        <PaddedSmallText>{formatMsg('noGoingBack')}</PaddedSmallText>
      </FullWidthDiv>
    );
  }

  renderPaymentMethods() {
    if (this.props.isFree) return null;

    const { formatMsg, payer } = this.props;
    const { selectedPayment } = this.state;
    const selected = payer[PAYMENT_METHOD];

    const visaIcon = selectedPayment === 'visa' ? VisaOn : VisaOff;
    const masterCardIcon =
      selectedPayment === 'mastercard' ? MasterCardOn : MasterCardOff;
    const mbIcon = selected === MULTIBANCO ? MultibancoOn : MultibancoOff;

    return (
      <Fragment>
        <SubheadingDiv>{formatMsg('paymentMethod')}</SubheadingDiv>
        <PaymentOptionsDiv>
          <button
            type="button"
            onClick={this.handlePaymentChange(MULTIBANCO)}
            title="Multibanco"
          >
            <img src={mbIcon} alt="multibanco" />
          </button>

          <button
            type="button"
            onClick={this.handlePaymentChange(CREDIT_CARD, 'visa')}
            title="Visa"
          >
            <img src={visaIcon} alt="visa" />
          </button>

          <button
            type="button"
            onClick={this.handlePaymentChange(CREDIT_CARD, 'mastercard')}
            title="Mastercard"
          >
            <img src={masterCardIcon} alt="mastercard" />
          </button>
        </PaymentOptionsDiv>
      </Fragment>
    );
  }

  render() {
    return (
      <PaymentsContainer>
        {this.renderPaymentMethods()}
        {this.renderPaymentWarning()}
      </PaymentsContainer>
    );
  }
}

export default injectIntl('PaymentTypeSelector')(PaymentTypeSelector);
