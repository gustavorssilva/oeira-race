import { defineMessages } from 'react-intl';

export default defineMessages({
  'PaymentTypeSelector.noGoingBack': {
    id: 'PaymentTypeSelector.noGoingBack',
    defaultMessage:
      'Confirma por favor que este é o método de pagamento que pretendes utilizar uma vez que ao avançar serás redireccionado para outro website.',
  },
  'PaymentTypeSelector.paymentMethod': {
    id: 'PaymentTypeSelector.paymentMethod',
    defaultMessage: 'Método de Pagamento',
  },
});
