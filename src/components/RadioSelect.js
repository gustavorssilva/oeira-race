import _ from 'lodash';
import React from 'react';
import { array, any, node, func, string } from 'prop-types';
import styled from 'styled-components';

const RadioSelect = ({
  title,
  options,
  selected,
  handleClick,
  children,
  extraInfo,
}) => {
  const fieldsetLabel = _.snakeCase(title);
  const buttons = _.map(options, ({ value, text, isDisabled = false }) => {
    const isSelected = value === selected;

    return (
      <Option
        key={value}
        role="radio"
        type="radio"
        aria-disabled={isDisabled}
        disabled={isDisabled}
        aria-checked={isSelected}
        selected={isSelected}
        onClick={() => (isDisabled && !isSelected ? null : handleClick(value))}
      >
        {text}
      </Option>
    );
  });

  return (
    <FieldSet aria-labelledby={fieldsetLabel}>
      {children}
      <Legend id={fieldsetLabel}>{title}</Legend>
      {extraInfo && <Explanation>{extraInfo}</Explanation>}
      <OptionsDiv role="radiogroup">{buttons}</OptionsDiv>
    </FieldSet>
  );
};

RadioSelect.propTypes = {
  handleClick: func.isRequired,
  selected: any,
  children: node,
  extraInfo: node,
  title: string.isRequired,
  options: array.isRequired,
};

const FieldSet = styled.fieldset`
  position: relative;
  border: 1px solid ${({ theme }) => theme.primary};
  box-shadow: 0 4px 8px -2px rgba(0, 0, 0, 0.1);
  margin: 2.2rem 0 0 0;
  padding: 0.5rem;
  width: 100%;
`;

const Explanation = styled.div`
  font-size: 0.85rem;
  padding: 0.5rem;
  white-space: pre-wrap;
`;

const Legend = styled.legend`
  position: absolute;
  top: -0.6em;
  left: 0.7em;
  background-color: #ffffff;
  color: ${({ theme }) => theme.primary};
  font-size: 0.8rem;
  font-weight: 700;
  line-height: 1;
  padding: 0 0.5em;
`;

const OptionsDiv = styled.div`
  display: flex;
  display: grid;
  grid-template-columns: repeat(auto-fit, minmax(30px, 1fr));
  grid-column-gap: 8px;
  width: 100%;
  justify-content: space-between;
  justify-items: stretch;
  margin-top: 0.5rem;
`;

const Option = styled.button`
  position: relative;
  background-color: ${({ theme }) => theme.inactiveGrey};
  border-radius: 5px;
  color: white;
  cursor: pointer;
  height: 50px;
  line-height: 1.13;
  padding: 5px 5px;
  text-align: center;
  text-transform: uppercase;
  transition: all 0.1s ease;
  width: 100%;
  flex: 1;

  /* Checking for IE11. */
  /* It does support "display: grid", but it doesn't support @supports */
  /* Only want margin on these buttons if it's IE 11 */
  margin: 2px;
  @supports (display: grid) {
    margin: 0;
  }

  &:hover {
    background-color: ${({ theme }) => theme.yellow};
    color: ${({ theme }) => theme.textColor};
  }

  &[aria-disabled='true']:not([aria-checked='true']) {
    color: ${({ theme }) => theme.inactiveGrey};
    background-color: ${({ theme }) => theme.white};
    border: 1px solid ${({ theme }) => theme.inactiveGrey};
    cursor: not-allowed;

    &::before {
      content: '';
      position: absolute;
      top: 5px;
      bottom: 5px;
      left: 49%;
      border: 1px solid ${({ theme }) => theme.unavailableRed};
      transform: rotate(45deg);
    }
  }

  &[aria-checked='true'] {
    background-color: ${({ theme }) => theme.primary};
    color: ${({ theme }) => theme.white};
  }
`;

export default RadioSelect;
