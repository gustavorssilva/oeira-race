import _ from 'lodash';
import React, { Component } from 'react';
import { func, object } from 'prop-types';
import injectIntl from 'utils/injectIntl';
import { INVOICE_FIELDS } from 'containers/Registration/constants';
import FormField from 'components/FormField';
import {
  CenteredBoldText,
  InvoiceFormContainer,
  InvoiceTitleDiv,
  InvoiceDiv,
} from 'components/styled';
import { HideIcon, ShowIcon, InvoiceWhite, InvoiceBlack } from 'assets/icons';
import { textColor } from 'styles/colors';

class InvoiceForm extends Component {
  static propTypes = {
    formatMsg: func.isRequired,
    handleChange: func.isRequired,
    invoicee: object.isRequired,
  };

  state = { show: true };

  handleChange = (e, field) => {
    const { invoicee, handleChange } = this.props;

    handleChange({ ...invoicee, [field]: e.target.value });
  };

  toggleForm = () => this.setState({ show: !this.state.show });

  renderFields() {
    const { formatMsg, invoicee } = this.props;

    return _.map(INVOICE_FIELDS, (field) => (
      <FormField
        key={field}
        placeholder={formatMsg(field)}
        value={invoicee[field] || ''}
        onChange={(e) => this.handleChange(e, field)}
        color={textColor}
        lessMargin
      />
    ));
  }

  renderForm() {
    const { show } = this.state;
    const { formatMsg } = this.props;

    if (!show) return null;

    return (
      <InvoiceFormContainer>
        {this.renderFields()}
        <CenteredBoldText>
          {formatMsg('if_you_want_an_invoice')}
        </CenteredBoldText>
      </InvoiceFormContainer>
    );
  }

  render() {
    const {
      props: { formatMsg },
      state: { show },
    } = this;
    const toggleIcon = show ? HideIcon : ShowIcon;
    const invoiceIcon = show ? InvoiceWhite : InvoiceBlack;

    return (
      <InvoiceDiv>
        <InvoiceTitleDiv show={show}>
          <div>
            <img src={invoiceIcon} alt="" />
          </div>
          <div>{formatMsg('invoice')}</div>
          <div tabIndex="0" role="button" onClick={() => this.toggleForm()}>
            <img src={toggleIcon} alt="toggle invoice fields" />
          </div>
        </InvoiceTitleDiv>
        {this.renderForm()}
      </InvoiceDiv>
    );
  }
}

export default injectIntl('InvoiceForm')(InvoiceForm);
