import React from 'react';
import { bool, func, node, string } from 'prop-types';
import styled from 'styled-components';
import { Ticked, Unticked } from 'assets/icons';

function isEnterOrSpace({ keyCode }) {
  return keyCode === 13 || keyCode === 32;
}

const TickBox = ({ onClick, text, value, ariaLabel }) => (
  <Container>
    <Box
      tabIndex={0}
      role="checkbox"
      aria-checked={!!value}
      aria-labelledby={ariaLabel}
      onClick={onClick}
      onKeyUp={(e) => (isEnterOrSpace(e) ? onClick() : null)}
    >
      <img src={value ? Ticked : Unticked} alt={value ? 'sim' : 'não'} />
    </Box>
    <DescriptionDiv id={ariaLabel}>{text}</DescriptionDiv>
  </Container>
);

TickBox.propTypes = {
  ariaLabel: string.isRequired,
  text: node.isRequired,
  value: bool.isRequired,
  onClick: func.isRequired,
};

const Container = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: flex-start;
  font-size: 14px;
  line-height: 1.14;
  padding-right: 15px;
`;

const Box = styled.div`
  position: relative;
  cursor: pointer;
  margin-right: 5px;
`;

const DescriptionDiv = styled.div`
  text-align: left;
  padding-top: 1rem;

  a,
  a:visited,
  a:link {
    color: ${({ theme }) => theme.textColor};
    font-weight: bold;
    text-decoration: underline;
  }
`;

export default TickBox;
