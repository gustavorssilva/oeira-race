import React from 'react';
import { func, node, object } from 'prop-types';
import styled from 'styled-components';

const SecondaryButton = ({ children, onClick, icon, style = {} }) => (
  <Button onClick={onClick} type="button" style={style}>
    {icon}
    {children}
  </Button>
);

SecondaryButton.propTypes = {
  children: node,
  style: object,
  onClick: func,
  icon: node,
};

const Button = styled.button`
  position: relative;
  background: ${({ theme }) => theme.primary};
  border-radius: 0px;
  padding: 0 0 0 50px;
  color: white;
  font-weight: bold;
  height: 50px;
  text-transform: uppercase;
  width: 100%;
  transition: all 0.2s ease;
  box-shadow: 0 4px 8px -2px rgba(0, 0, 0, 0.2);

  img {
    position: absolute;
    top: 0;
    left: 0;
  }
`;

export default SecondaryButton;
