import { defineMessages } from 'react-intl';

export default defineMessages({
  'Navbar.mainPage': {
    id: 'Navbar.mainPage',
    defaultMessage: 'Página Principal',
  },
  'Navbar.registrations': {
    id: 'Navbar.registrations',
    defaultMessage: 'Inscrições',
  },
  'Navbar.contactPage': {
    id: 'Navbar.contactPage',
    defaultMessage: 'Contactos',
  },
});
