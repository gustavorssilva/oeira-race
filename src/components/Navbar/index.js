import React, { Component } from 'react';
import { func } from 'prop-types';
import { Link } from 'react-router-dom';
import Transition from 'react-transition-group/Transition';
import styled from 'styled-components';
import injectIntl from 'utils/injectIntl';
import MenuIcon from 'mdi-react/MenuIcon';
import CloseIcon from 'mdi-react/CloseIcon';
import ExternalLInk from 'components/ExternalLink';

const menuStyle = {
  entered: { transform: 'translateX(0)' },
};

class Navbar extends Component {
  static propTypes = {
    formatMsg: func.isRequired,
  };
  state = { showMenu: false };

  toggleMenu = () => this.setState(({ showMenu }) => ({ showMenu: !showMenu }));

  renderMenu() {
    const { formatMsg } = this.props;
    const { showMenu } = this.state;

    return (
      <Transition in={showMenu} timeout={0}>
        {(state) => (
          <Menu style={menuStyle[state]}>
            <ExternalLInk href="https://www.marginalanoite.pt/">
              {formatMsg('mainPage')}
            </ExternalLInk>

            <Link to="/" onClick={this.toggleMenu}>
              {formatMsg('registrations')}
            </Link>

            <ExternalLInk href="https://www.marginalanoite.pt/contactos">
              {formatMsg('contactPage')}
            </ExternalLInk>
          </Menu>
        )}
      </Transition>
    );
  }

  render() {
    const icon = this.state.showMenu ? <CloseIcon /> : <MenuIcon />;
    return (
      <Nav id="navbar">
        <MenuButton type="button" onClick={this.toggleMenu}>
          {icon}
        </MenuButton>
        {this.renderMenu()}
      </Nav>
    );
  }
}

const navHeight = '35px';

const Nav = styled.nav`
  display: flex;
  justify-content: space-between;
  align-items: center;
  position: relative;
  height: ${navHeight};
  background-color: ${({ theme }) => theme.primaryDark};
  color: white;
  font-weight: bold;
  padding: 10px;

  i,
  svg {
    cursor: pointer;
  }
`;

const MenuButton = styled.button`
  padding: 0px;
  cursor: pointer;
  user-select: none;
`;

const Menu = styled.div`
  position: absolute;
  top: ${navHeight};
  left: 0px;
  right: 0px;

  display: flex;
  flex-direction: column;

  background-color: ${({ theme }) => theme.primaryDarkest};
  padding: 10px;
  transition: all 0.2s ease-in-out;
  transform: translateX(-100vw);
  color: ${({ theme }) => theme.yellow};

  a,
  a:visited {
    color: ${({ theme }) => theme.white};
    text-decoration: none !important;
    text-transform: uppercase;
    font-size: 90%;
    user-select: none;
    margin: 5px;

    &:hover,
    &:focus {
      color: ${({ theme }) => theme.yellow};
    }
  }
`;

export default injectIntl('Navbar')(Navbar);
