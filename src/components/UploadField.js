import React from 'react';
import { array, func, object, string } from 'prop-types';
import styled from 'styled-components';
import { UploadIcon } from 'assets/icons';

const UploadField = ({ field, text, onChange, file, fileTypes }) => {
  const fileExists = file && file.name;
  const labelText = fileExists ? file.name : text;

  return (
    <Label htmlFor={field} fileExists={fileExists}>
      <Input
        id={field}
        onChange={onChange}
        type="file"
        accept={fileTypes.join(', ')}
      />
      {labelText}
      <img src={UploadIcon} alt="" />
    </Label>
  );
};

UploadField.propTypes = {
  field: string.isRequired,
  file: object,
  fileTypes: array.isRequired,
  text: string.isRequired,
  onChange: func.isRequired,
};

const Label = styled.label`
  position: relative;
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 50px;
  width: 100%;
  box-shadow: 0 4px 8px -2px rgba(0, 0, 0, 0.2);
  background-color: ${({ fileExists, theme }) =>
    fileExists ? theme.selectedGrey : theme.primary};
  color: white;
  padding: 0 10px;

  &:hover {
    cursor: pointer;
  }
  img {
    position: absolute;
    right: 0;
    top: 0;
    background-color: transparent;
  }
`;

const Input = styled.input`
  width: 1px;
  height: 1px;
  opacity: 0;
  overflow: hidden;
  position: absolute;
  z-index: -1;
`;

export default UploadField;
