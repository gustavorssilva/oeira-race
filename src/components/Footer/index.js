import _ from 'lodash';
import React, { Component } from 'react';
import styled from 'styled-components';
import { onDesktop } from 'styles/cssQueries';
import EasyPay from 'assets/EasyPay.png';
import sponsors from './sponsors';
import SponsorLogo from './SponsorLogo';
import ExternalLink from '../ExternalLink';

class Footer extends Component {
  renderLogos() {
    return _.map(sponsors, ({ image, alt, url }) => (
      <SponsorLogo key={alt} image={image} alt={alt} url={url} />
    ));
  }

  render() {
    return (
      <FooterDiv>
        {!`SHOW SPONSORS LOGOS` && (
          <>
            <h4>PARCEIROS</h4>
            <Images>{this.renderLogos()}</Images>
          </>
        )}

        <BottomBorder>
          <div id="copyright">2019 © Oeiras run 2019</div>
          <div id="easypay">
            <img src={EasyPay} alt="EasyPay" />
          </div>
          <div id="lastlap">
            <ExternalLink href="http://www.lastlap.pt">
              Last Lap Eventos e Comunicação
            </ExternalLink>
          </div>
        </BottomBorder>
      </FooterDiv>
    );
  }
}

const FooterDiv = styled.footer`
  position: relative;
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  margin-top: 2.2rem;

  > img {
    z-index: 2;
  }

  h4 {
    width: 375px;
    max-width: 90%;
    border-bottom: 1px solid ${({ theme }) => theme.primary};
    text-align: center;
    margin: 0;
  }
`;

const Images = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  margin: 20px 0;
  max-width: 400px;

  > a {
    width: 100px;
    margin: 0 5px;
    text-align: center;
  }
`;

const BottomBorder = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-direction: column;
  background: ${({ theme }) => theme.primaryDark};
  color: white;
  font-size: 12px;
  font-weight: bold;
  padding: 15px;
  text-align: center;
  width: 100%;

  > div {
    line-height: 1;
    text-align: center;
  }

  #easypay {
    order: 1;
  }

  #copyright {
    order: 2;
    padding: 0.6rem 0;
  }

  #lastlap {
    order: 3;
  }

  ${onDesktop} {
    flex-direction: row;

    #easypay {
      order: 2;
    }

    #copyright {
      order: 1;
    }

    #lastlap {
      order: 3;
    }
  }

  a:link,
  a:visited,
  a:hover {
    color: white;
    text-decoration: none;
    cursor: pointer;
  }

  img {
    width: 200px;
  }
`;

export default Footer;
