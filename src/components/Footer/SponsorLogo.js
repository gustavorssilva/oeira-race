import React from 'react';
import { string } from 'prop-types';
import ExternalLink from '../ExternalLink';

const SponsorLogo = ({ image, alt, url }) => (
  <ExternalLink href={url}>
    <img src={image} alt={alt} title={alt} />
  </ExternalLink>
);

SponsorLogo.propTypes = {
  image: string.isRequired,
  alt: string.isRequired,
  url: string.isRequired,
};

export default SponsorLogo;
