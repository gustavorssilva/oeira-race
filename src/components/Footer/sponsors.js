import Biolectra from 'assets/Biolectra.png';
import BomPetisco from 'assets/BomPetisco.png';
import CP from 'assets/CP.png';
import Gatorade from 'assets/Gatorade.png';
import GFD from 'assets/GFD.png';
import GinsactivSport from 'assets/GinsactivSport.png';
import HospitalLuz from 'assets/HospitalLuz.png';
import NewBalance from 'assets/NewBalance.png';
import OeirasViva from 'assets/OeirasViva.png';
import Peugeot from 'assets/Peugeot.png';
import RadioRenascenca from 'assets/RadioRenascenca.jpg';
import SerraEstrela from 'assets/SerraEstrela.png';

window.NewBalance = NewBalance;

export default [
  {
    image: NewBalance,
    alt: 'New Balance',
    url: 'http://www.newbalance.com',
  },
  {
    image: Peugeot,
    alt: 'Peugeot',
    url: 'http://rede.peugeot.pt/sucursalpeugeot',
  },
  {
    image: Gatorade,
    alt: 'Gatorade',
    url: 'http://www.gatorade.com',
  },
  {
    image: SerraEstrela,
    alt: 'Agua Serra da Estrela',
    url: 'http://www.aguaserradaestrela.pt',
  },
  {
    image: HospitalLuz,
    alt: 'Hospital da Luz',
    url: 'http://hospitaldaluz.pt/pt/',
  },
  {
    image: CP,
    alt: 'CP - Comboios de Portugal',
    url: 'https://www.cp.pt',
  },
  {
    image: GFD,
    alt: 'GFD - Gabinete de Fisioterapia no Desporto',
    url: 'http://www.gfd.pt',
  },
  {
    image: OeirasViva,
    alt: 'Oeiras Viva',
    url: 'https://www.oeirasviva.pt/',
  },
  {
    image: BomPetisco,
    alt: 'Bom Petisco',
    url: 'https://bompetisco.pt/',
  },
  {
    image: RadioRenascenca,
    alt: 'Rádio Renascença',
    url: 'http://rr.sapo.pt/',
  },
  {
    image: GinsactivSport,
    alt: 'Ginsactiv Sport',
    url: 'https://ginsactivsport.pt/',
  },
  {
    image: Biolectra,
    alt: 'Biolectra',
    url: 'https://www.biolectra.com/',
  },
];
