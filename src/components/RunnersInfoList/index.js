import _ from 'lodash';
import React, { Component } from 'react';
import { array, func, object } from 'prop-types';
import injectIntl from 'utils/injectIntl';
import { CheckIcon, CloseIcon } from 'mdi-react';
import {
  EditIcon,
  DeleteIcon,
  HideIcon,
  ShowIcon,
  MoreIcon,
  WarningIcon,
} from 'assets/icons';
import {
  CONFIRM_DETAILS_FIELDS,
  FULL_NAME,
  ID_NUMBER,
  DOB,
  GENDER,
  START_BOX,
  EVIDENCE_UPLOAD,
  START_BOX_FAST_RUNNERS,
  SHIRT_SIZE,
  CHIP,
} from 'containers/Registration/constants';
import { getRunnerPrice } from 'containers/Registration/utils';
import SecondaryButton from 'components/SecondaryButton';
import UploadField from 'components/UploadField';
import {
  AllDetailsDiv,
  WarningText,
  SmallButton,
  TwoButtonDiv,
  FullWidthDiv,
  RunnerDetailDiv,
  SummaryDiv,
} from 'components/styled';

class RunnersInfoList extends Component {
  static propTypes = {
    formatMsg: func.isRequired,
    allRunners: array.isRequired,
    shirtSizes: array.isRequired,
    handleAddRunner: func,
    handleDelete: func,
    handleEditClick: func,
    handleFileChange: func,
    promoCodePrices: object,
  };

  static defaultProps = {
    promoCodePrices: {},
  };

  state = { indexToShow: null };

  getPricePerPerson(runner, index) {
    const { promoCodePrices } = this.props;
    return _.toString(promoCodePrices[index]) || getRunnerPrice(runner);
  }

  toggleDropdown(index) {
    const currentIndex = this.state.indexToShow;
    const indexToShow = index === currentIndex ? null : index;
    this.setState({ indexToShow });
  }

  runnerNeedsFile = (runner) => {
    const { [START_BOX]: startBox, [EVIDENCE_UPLOAD]: file } = runner;
    return startBox === START_BOX_FAST_RUNNERS && !file;
  };

  runnerHasWrongShirt = (runner) => {
    const { shirtSizes } = this.props;
    const availableShirts = _.find(shirtSizes, {
      type: runner[CHIP],
      gender: runner[GENDER],
    }).sizes;

    return !_.includes(availableShirts, runner[SHIRT_SIZE]);
  };

  renderAddRunnerButton() {
    const { handleAddRunner, formatMsg } = this.props;
    if (!handleAddRunner) return null;

    return (
      <SecondaryButton
        style={{ marginTop: '20px' }}
        onClick={handleAddRunner}
        icon={<img src={MoreIcon} alt="" />}
      >
        {formatMsg('add_runner')}
      </SecondaryButton>
    );
  }

  renderEditButton(runner) {
    const { formatMsg, handleEditClick } = this.props;
    if (!handleEditClick) return null;

    return (
      <SmallButton onClick={() => handleEditClick(runner)}>
        <img src={EditIcon} alt="" />
        <span>{formatMsg('edit')}</span>
      </SmallButton>
    );
  }

  renderDeleteButton(runner) {
    const { formatMsg, allRunners, handleDelete } = this.props;
    if (!handleDelete || allRunners.length === 1) return null;

    return (
      <SmallButton onClick={() => handleDelete(runner)}>
        <img src={DeleteIcon} alt="" />
        <span>{formatMsg('delete')}</span>
      </SmallButton>
    );
  }

  renderDropdownRow(runner, field) {
    const { formatMsg } = this.props;
    let msgKey = field;
    let info = runner[field];

    if (_.includes([ID_NUMBER, DOB], field)) {
      msgKey = `short_${field}`;
    } else if (_.includes([GENDER, START_BOX, CHIP], field)) {
      info = formatMsg(runner[field]);
    } else if (_.isBoolean(info)) {
      const Icon = info ? CheckIcon : CloseIcon;
      info = <Icon />;
    }

    if (field === DOB) {
      info = new Date(runner[field]).toLocaleDateString();
    }

    return (
      <div key={`${runner[DOB]}|${runner[FULL_NAME]}|${field}`}>
        <span>{formatMsg(msgKey)}:</span>
        {info}
      </div>
    );
  }

  renderDetails = (runner, index = 0) => {
    const show = this.state.indexToShow === index;
    const toggleIcon = show ? HideIcon : ShowIcon;
    const warning =
      this.runnerHasWrongShirt(runner) || this.runnerNeedsFile(runner);

    return (
      <RunnerDetailDiv key={`${index}-${runner[ID_NUMBER]}`}>
        <SummaryDiv warning={warning}>
          <div>{runner[FULL_NAME]}</div>

          <div>{warning && <img src={WarningIcon} alt="Aviso" />}</div>

          <div>{this.getPricePerPerson(runner, index)}€</div>

          <button type="button" onClick={() => this.toggleDropdown(index)}>
            <img src={toggleIcon} alt="toggle details" />
          </button>
        </SummaryDiv>
        {this.renderRunnerDropdown(runner, show)}
      </RunnerDetailDiv>
    );
  };

  renderRunnerDropdown(runner, show) {
    if (!show) return null;

    const rows = _.map(CONFIRM_DETAILS_FIELDS, (field) =>
      this.renderDropdownRow(runner, field)
    );

    return (
      <AllDetailsDiv>
        {this.renderShirtWarning(runner)}
        {rows}
        {this.renderFileUploadButton(runner)}
        <TwoButtonDiv>
          {this.renderEditButton(runner)}
          {this.renderDeleteButton(runner)}
        </TwoButtonDiv>
      </AllDetailsDiv>
    );
  }

  renderShirtWarning(runner) {
    const { formatMsg } = this.props;
    if (!this.runnerHasWrongShirt(runner)) return null;
    return <WarningText>{formatMsg('shirt_size_not_available')}</WarningText>;
  }

  renderFileUploadButton(runner) {
    const { handleFileChange } = this.props;
    if (!handleFileChange || runner[START_BOX] !== START_BOX_FAST_RUNNERS) {
      return null;
    }

    const { formatMsg } = this.props;
    const { [ID_NUMBER]: id, [EVIDENCE_UPLOAD]: file } = runner;

    const fileWarning = file ? null : (
      <WarningText key={`file-warning${id}`}>
        {formatMsg('please_upload_evidence')}
      </WarningText>
    );

    return [
      fileWarning,
      <UploadField
        key={id}
        field={EVIDENCE_UPLOAD}
        text={formatMsg(EVIDENCE_UPLOAD)}
        file={file}
        onChange={(e) => handleFileChange(e, id)}
        fileTypes={['.jpg', '.jpeg', '.pdf']}
      />,
    ];
  }

  render() {
    const { allRunners } = this.props;
    return (
      <FullWidthDiv>
        {_.map(allRunners, this.renderDetails)}
        {this.renderAddRunnerButton()}
      </FullWidthDiv>
    );
  }
}

export default injectIntl('RunnersInfoList')(RunnersInfoList);
