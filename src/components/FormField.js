import _ from 'lodash';
import React, { Component } from 'react';
import { bool, func, string, object } from 'prop-types';
import styled from 'styled-components';
import { InputContainer } from 'components/styled';
import { DORSAL_NAME } from 'containers/Registration/constants';

function trimValue(e, onChange) {
  const { value } = e.target;
  if (!_.isString(value)) return;
  e.target.value = value.trim();
  onChange(e);
}

class FormField extends Component {
  static propTypes = {
    disabled: bool,
    lessMargin: bool,
    placeholder: string,
    style: object,
    type: string,
    field: string,
    value: string,
    color: string,
    onChange: func.isRequired,
  };

  static defaultProps = {
    disabled: false,
    lessMargin: false,
    placeholder: '',
    style: undefined,
    type: 'text',
    field: '',
    value: '',
  };

  render() {
    const {
      field,
      disabled,
      placeholder,
      onChange,
      value,
      type,
      style,
      lessMargin,
      color,
    } = this.props;

    return (
      <InputContainer lessMargin={lessMargin} style={style} color={color}>
        <Label color={color}>{placeholder}</Label>
        <Input
          disabled={!!disabled}
          onChange={onChange}
          placeholder={placeholder}
          value={value}
          type={type}
          maxLength={field === DORSAL_NAME ? 40 : 255}
          onBlur={(e) => trimValue(e, onChange)}
        />
      </InputContainer>
    );
  }
}

const Input = styled.input`
  border: 0px;
  border-radius: 0px;
  background-color: #ffffff;
  font-family: Roboto !important;
  padding: 15px 1rem;
  width: 100%;

  ::placeholder,
  &:disabled {
    color: ${({ theme }) => theme.placeholderGrey};
  }
`;

const Label = styled.div`
  position: absolute;
  top: -0.6em;
  left: 0.7em;
  font-size: 0.8rem;
  line-height: 1;
  padding: 0 0.5em;
  background-color: #ffffff;
  color: ${({ color, theme }) => color || theme.primary};
  font-weight: 700;
`;

export default FormField;
