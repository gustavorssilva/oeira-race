import _ from 'lodash';
import React, { Component, Fragment } from 'react';
import { array, func, object, string } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators, compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import injectIntl from 'utils/injectIntl';
import { PlusIcon, LoadingIcon, CheckIcon } from 'mdi-react';
import * as actions from 'containers/Registration/actions';
import {
  selectPromoCode,
  selectAppliedPromoCode,
} from 'containers/Registration/selectors';
import FormField from 'components/FormField';
import {
  FullWidthDiv,
  TestButton,
  RelativeFormFieldDiv,
  WarningText,
} from 'components/styled';

class PromoCodeHandler extends Component {
  static propTypes = {
    allRunners: array.isRequired,
    applyPromoCode: func.isRequired,
    formatMsg: func.isRequired,
    handleUpdate: func.isRequired,
    payer: object.isRequired,
    appliedPromoCode: object.isRequired,
    promoCode: string.isRequired,
    setPromoCode: func.isRequired,
    resetPromoCodeState: func.isRequired,
  };

  state = { code: '' };

  updatePromoCode = (e) => {
    const {
      setPromoCode,
      handleUpdate,
      appliedPromoCode: { data: codeData, error: codeError },
      resetPromoCodeState,
      payer,
    } = this.props;
    const { value } = e.target;

    if (codeData || codeError) resetPromoCodeState();
    setPromoCode(value);
    handleUpdate({ ...payer, discount_code: value });
  };

  submitPromoCode = () => {
    const { allRunners, applyPromoCode, promoCode: code } = this.props;
    const promoCode = code.trim();
    if (!promoCode) return;
    applyPromoCode({ promoCode, registrations: allRunners });
  };

  render() {
    const { formatMsg, payer, appliedPromoCode } = this.props;
    const error = _.get(appliedPromoCode, `error.message`);

    let icon = <PlusIcon size={36} />;
    if (appliedPromoCode.requesting) {
      icon = <LoadingIcon size={36} className="spin" />;
    } else if (appliedPromoCode.data) {
      icon = <CheckIcon size={36} />;
    }

    return (
      <Fragment>
        <RelativeFormFieldDiv>
          <FormField
            placeholder={formatMsg('title')}
            value={payer.discount_code || ''}
            onChange={this.updatePromoCode}
          />
          <TestButton onClick={this.submitPromoCode}>{icon}</TestButton>
        </RelativeFormFieldDiv>
        <FullWidthDiv>
          <WarningText>{error && formatMsg(error)}</WarningText>
        </FullWidthDiv>
      </Fragment>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  promoCode: selectPromoCode(),
  appliedPromoCode: selectAppliedPromoCode(),
});

const mapDispatchToProps = (dispatch) => bindActionCreators(actions, dispatch);

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
const withIntl = injectIntl('PromoCodeHandler');

export default compose(
  withConnect,
  withIntl
)(PromoCodeHandler);
