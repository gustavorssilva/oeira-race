import { defineMessages } from 'react-intl';

export default defineMessages({
  'PromoCodeHandler.title': {
    id: 'PromoCodeHandler.title',
    defaultMessage: 'Código Promocional',
  },
});
