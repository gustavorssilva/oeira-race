import _ from 'lodash';
import React, { Component } from 'react';
import { array, func } from 'prop-types';
import injectIntl from 'utils/injectIntl';
import Modal from './Modal';

class ErrorMsgHandler extends Component {
  static propTypes = {
    formatMsg: func.isRequired,
    stateCollection: array.isRequired,
  };

  static getDerivedStateFromProps({ stateCollection, formatMsg }, state) {
    if (state.error) return null;

    // Just get one error at a time
    const errorObj = _.find(stateCollection, ({ error }) => !!error);
    if (!errorObj) return null;

    const { error: err, onClose } = errorObj;
    const error = _.isString(err)
      ? formatMsg(err)
      : formatMsg(
          err.message,
          err.field ? { field: formatMsg(err.field) } : err.values
        );
    return { error, onClose };
  }

  state = { error: '', onClose: () => null };

  dismissErrorMsg = () => {
    this.state.onClose();
    this.setState({ error: '', onClose: () => null });
  };

  render() {
    const { error } = this.state;
    if (!error) return null;

    return <Modal onClick={this.dismissErrorMsg}>{error}</Modal>;
  }
}

export default injectIntl(`ErrorMsgHandler`)(ErrorMsgHandler);
