import React, { Component } from 'react';
import { bool, func, object } from 'prop-types';
import styled from 'styled-components';
import { TickBox } from 'components';
import { OneColumnGrid, PaddedSmallText } from 'components/styled';
import {
  AGREE_TO_PRIVACY_POLICY,
  AGREE_TO_TERMS,
  ACCEPT_EMAIL_CONTACT,
  ACCEPT_PHONE_CONTACT,
  ACCEPT_EMAIL_CONTACT_OTHER_EVENTS,
} from 'containers/Registration/constants';

class OptIns extends Component {
  static propTypes = {
    showTermsAndPrivacyOptIns: bool.isRequired,
    formatMsg: func.isRequired,
    formatHTMLMsg: func.isRequired,
    registrationObject: object.isRequired,
    updateRegistration: func.isRequired,
  };

  renderTickBox(field) {
    const {
      formatHTMLMsg,
      registrationObject,
      updateRegistration,
    } = this.props;

    const value = !!registrationObject[field];

    return (
      <TickBox
        key={field}
        ariaLabel={field}
        text={formatHTMLMsg(field)}
        value={value}
        onClick={() => updateRegistration({ [field]: !value })}
      />
    );
  }

  renderTermsAndPrivacyOptIns() {
    if (!this.props.showTermsAndPrivacyOptIns) return null;
    return [
      this.renderTickBox(AGREE_TO_TERMS),
      this.renderTickBox(AGREE_TO_PRIVACY_POLICY),
    ];
  }

  render() {
    const { formatMsg, formatHTMLMsg } = this.props;

    return (
      <OneColumnGrid style={{ marginBottom: 30 }}>
        {this.renderTermsAndPrivacyOptIns()}

        <H4>{formatMsg('communication_agreements_for_this_event')}</H4>
        {this.renderTickBox(ACCEPT_EMAIL_CONTACT)}
        {this.renderTickBox(ACCEPT_PHONE_CONTACT)}

        <H4>{formatMsg('communication_agreements_for_other_events')}</H4>
        {this.renderTickBox(ACCEPT_EMAIL_CONTACT_OTHER_EVENTS)}

        <H4 className="red-caps" bottomMargin>
          {formatMsg('personal_data_collection')}
        </H4>

        <PaddedSmallText>
          {formatHTMLMsg('personal_data_collection_desc')}
        </PaddedSmallText>
      </OneColumnGrid>
    );
  }
}

const H4 = styled.h4`
  margin-top: 50px;
  margin-bottom: ${({ bottomMargin }) => (bottomMargin ? 10 : 0)}px;
  padding-left: 20px;
  padding-right: 20px;
`;

export default OptIns;
