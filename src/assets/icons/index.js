// Buttons / Decoration
import BackIcon from './icon-go-back.svg';

import CloseIcon from './icon-close.svg';
import CalendarIcon from './icon-calendar.svg';
import DeleteIcon from './icon-delete.svg';
import ShowIcon from './icon-drop-down-on.svg';
import HideIcon from './icon-drop-down-off.svg';
import EditIcon from './icon-edit.svg';
import InvoiceBlack from './icon-invoice-black.svg';
import InvoiceWhite from './icon-invoice-white.svg';
import MoreIcon from './icon-plus.svg';
import PreviewIcon from './icon-preview.svg';
import Ticked from './icon-tickbox-true.svg';
import Unticked from './icon-tickbox-false.svg';
import UploadIcon from './icon-upload.svg';
import WarningIcon from './warning.svg';

// Payments
import MultibancoOn from './multibanco-on.svg';
import MultibancoOff from './multibanco-off.svg';
import VisaOn from './visa-on.svg';
import VisaOff from './visa-off.svg';
import MasterCardOn from './master-card-on.svg';
import MasterCardOff from './master-card-off.svg';

const FacebookIcon = 'facebook-copy.svg';
const TwitterIcon = 'twitter-copy.svg';

// Dorsal preview
const DorsalSVG = 'icons/dorsal.svg';

export {
  DorsalSVG,
  TwitterIcon,
  FacebookIcon,
  MasterCardOff,
  MasterCardOn,
  VisaOff,
  VisaOn,
  MultibancoOff,
  MultibancoOn,
  WarningIcon,
  UploadIcon,
  Unticked,
  Ticked,
  PreviewIcon,
  MoreIcon,
  InvoiceWhite,
  InvoiceBlack,
  EditIcon,
  HideIcon,
  ShowIcon,
  DeleteIcon,
  CalendarIcon,
  CloseIcon,
  BackIcon,
};
