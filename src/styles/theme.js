import * as colors from './colors';
import * as queries from './cssQueries';

export default {
  ...colors,
  ...queries,
};
