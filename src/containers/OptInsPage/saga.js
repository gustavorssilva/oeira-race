import { all, call, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import { getFirstErrorMsg } from 'utils/getErrorMsg';
import { BASE_URL } from 'utils/constants';
import { GET_RUNNER_PENDING, POST_OPT_INS_PENDING } from './constants';
import {
  getRunnerFailed,
  getRunnerSuccess,
  postOptInsFailed,
  postOptInsSuccess,
} from './actions';

function getRunner(runnerId) {
  return axios.get(`${BASE_URL}enrollments/${runnerId}`);
}

function postOptIns({ optIns, runnerId }) {
  return axios.put(`${BASE_URL}enrollments/${runnerId}/optins`, optIns, {
    responseType: 'text',
  });
}

function* callGetRunner({ payload }) {
  try {
    const { data } = yield call(getRunner, payload);
    yield put(getRunnerSuccess(data));
  } catch (e) {
    yield put(getRunnerFailed(getFirstErrorMsg(e)));
  }
}

function* callPostOptIns({ payload }) {
  try {
    yield call(postOptIns, payload);
    yield put(postOptInsSuccess(true));
  } catch (e) {
    yield put(postOptInsFailed(getFirstErrorMsg(e)));
  }
}

// Individual exports for testing
function* getRunnerSaga() {
  yield takeEvery(GET_RUNNER_PENDING, callGetRunner);
}

function* postOptInsSaga() {
  yield takeEvery(POST_OPT_INS_PENDING, callPostOptIns);
}

export default function* rootSaga() {
  yield all([getRunnerSaga(), postOptInsSaga()]);
}
