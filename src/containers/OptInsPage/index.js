import _ from 'lodash';
import React, { Component } from 'react';
import { func, object } from 'prop-types';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import { injectIntl, intlShape, FormattedHTMLMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { isLoadingInProgress } from 'utils/functions';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import { BigButton, LoadingScreen, Modal } from 'components';
import { FullWidthDiv, SmallText } from 'components/styled';
import { OPT_INS } from 'containers/Registration/constants';
import OptIns from 'components/OptIns';
import regMessages from 'containers/Registration/messages';
import messages from './messages';
import * as actions from './actions';
import { selectRunner, selectPostState } from './selectors';
import reducer from './reducer';
import saga from './saga';

class OptInsPage extends Component {
  state = {
    modalContent: null,
    runnerId: this.props.location.pathname.replace('/opt-ins/', ''),
  };

  componentWillMount() {
    const { runnerId } = this.state;
    const { getRunner } = this.props;

    getRunner(runnerId);
  }

  componentWillReceiveProps(nextProps, { modalContent }) {
    const {
      resetPostOptInsState,
      runnerState: { error: getError },
      postState: { data, error: postError },
    } = nextProps;
    const error = getError || postError;

    if ((!error && !data) || modalContent) return;
    resetPostOptInsState();

    const message = this.formatMsg(
      data ? 'registration_updated' : error.message
    );
    this.setModal(message);
  }

  setModal = (modalContent) => this.setState({ modalContent });

  formatHTMLMsg = (id, values = {}) => {
    const msg = messages[id] || regMessages[id];

    if (!msg) {
      console.error('[formatHTMLMsg] Message not found:', id);
      return id;
    }

    return <FormattedHTMLMessage {...msg} values={values} />;
  };

  formatMsg = (id, values = {}) => {
    const { formatMessage } = this.props.intl;
    const msg = messages[id] || regMessages[id];

    if (!msg) {
      console.error('[formatMsg] Message not found:', id);
      return id;
    }

    return formatMessage(msg, values);
  };

  handleClick = () => {
    const {
      postOptIns,
      resetPostOptInsState,
      runnerState: { data: runner },
    } = this.props;
    const { runnerId } = this.state;

    const optIns = _.pick(runner, OPT_INS);
    resetPostOptInsState();
    return postOptIns({ runnerId, optIns });
  };

  handleOptInChange = (data) => this.props.updateRunner(data);

  renderModal() {
    const { modalContent } = this.state;
    if (!modalContent) return null;
    return <Modal onClick={() => this.setModal('')}>{modalContent}</Modal>;
  }

  renderLoadingSpinner() {
    return isLoadingInProgress(this.props) ? <LoadingScreen /> : null;
  }

  render() {
    const { data: runner } = this.props.runnerState;
    if (!runner) return this.renderModal();

    return (
      <FullWidthDiv>
        <h1>{this.formatMsg('opt_ins_title')}</h1>

        <OptIns
          showTermsAndPrivacyOptIns={false}
          registrationObject={runner}
          updateRegistration={this.handleOptInChange}
          formatMsg={this.formatMsg}
          formatHTMLMsg={this.formatHTMLMsg}
        />

        <BigButton onClick={this.handleClick}>
          {this.formatMsg('save')}
        </BigButton>

        <SmallText style={{ padding: '25px 0 20px 0' }}>
          {this.formatHTMLMsg('view_privacy_policy_here')}
        </SmallText>

        {this.renderLoadingSpinner()}
        {this.renderModal()}
      </FullWidthDiv>
    );
  }
}

OptInsPage.propTypes = {
  intl: intlShape.isRequired,
  getRunner: func.isRequired,
  postState: object.isRequired,
  postOptIns: func.isRequired,
  resetPostOptInsState: func.isRequired,
  updateRunner: func.isRequired,
  location: object.isRequired,
  runnerState: object.isRequired,
};

const mapStateToProps = createStructuredSelector({
  runnerState: selectRunner(),
  postState: selectPostState(),
});

const mapDispatchToProps = (dispatch) => bindActionCreators(actions, dispatch);

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
const withReducer = injectReducer({ key: 'optIns', reducer });
const withSaga = injectSaga({ key: 'optIns', saga });

export default injectIntl(
  compose(
    withReducer,
    withSaga,
    withConnect
  )(OptInsPage)
);
