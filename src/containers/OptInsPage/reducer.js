import { fromJS } from 'immutable';
import {
  GET_RUNNER_PENDING,
  GET_RUNNER_FAILED,
  GET_RUNNER_SUCCESS,
  UPDATE_RUNNER,
  POST_OPT_INS_PENDING,
  POST_OPT_INS_FAILED,
  POST_OPT_INS_SUCCESS,
  RESET_POST_OPT_INS_STATE,
} from './constants';

const setState = (requesting = false, error = null, data = null) => ({
  requesting,
  error,
  data,
});

function updateRunnerState(state, payload) {
  const oldData = state.get('runner').data || {};
  const newData = { ...oldData, ...payload };
  return state.set('runner', setState(false, null, newData));
}

const initialState = fromJS({
  runner: setState(),
  postState: setState(),
});

function paymentSuccessReducer(state = initialState, { type, payload }) {
  switch (type) {
    case GET_RUNNER_PENDING:
      return state.set('runner', setState(true));
    case GET_RUNNER_FAILED:
      return state.set('runner', setState(false, payload));
    case GET_RUNNER_SUCCESS:
      return state.set('runner', setState(false, null, payload));
    case UPDATE_RUNNER:
      return updateRunnerState(state, payload);

    case RESET_POST_OPT_INS_STATE:
      return state.set('postState', setState());
    case POST_OPT_INS_PENDING:
      return state.set('postState', setState(true));
    case POST_OPT_INS_FAILED:
      return state.set('postState', setState(false, payload));
    case POST_OPT_INS_SUCCESS:
      return state.set('postState', setState(false, null, payload));

    default:
      return state;
  }
}

export default paymentSuccessReducer;
