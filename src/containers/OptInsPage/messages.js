import { defineMessages } from 'react-intl';

export default defineMessages({
  opt_ins_title: {
    id: 'app.containers.PaymentSuccess.opt_ins_title',
    defaultMessage: 'Proteção de Dados',
  },
  submit: {
    id: 'app.containers.PaymentSuccess.submit',
    defaultMessage: 'Alterar',
  },
});
