import {
  GET_RUNNER_PENDING,
  GET_RUNNER_FAILED,
  GET_RUNNER_SUCCESS,
  UPDATE_RUNNER,
  POST_OPT_INS_PENDING,
  POST_OPT_INS_FAILED,
  POST_OPT_INS_SUCCESS,
  RESET_POST_OPT_INS_STATE,
} from './constants';

// =========== GET RUNNER ===========
export const getRunner = (payload) => ({ type: GET_RUNNER_PENDING, payload });
export const getRunnerFailed = (payload) => ({
  type: GET_RUNNER_FAILED,
  payload,
});
export const getRunnerSuccess = (payload) => ({
  type: GET_RUNNER_SUCCESS,
  payload,
});
export const updateRunner = (payload) => ({
  type: UPDATE_RUNNER,
  payload,
});

// =========== POST OPT-INS ===========
export const resetPostOptInsState = (payload) => ({
  type: RESET_POST_OPT_INS_STATE,
  payload,
});
export const postOptIns = (payload) => ({
  type: POST_OPT_INS_PENDING,
  payload,
});
export const postOptInsFailed = (payload) => ({
  type: POST_OPT_INS_FAILED,
  payload,
});
export const postOptInsSuccess = (payload) => ({
  type: POST_OPT_INS_SUCCESS,
  payload,
});
