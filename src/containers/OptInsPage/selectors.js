import { createSelector } from 'reselect';

// * Direct selector to the paymentSuccess state domain
const selectOptInsDomain = (state) => state.get('optIns');

// Substate selector
const getSubstate = (key) =>
  createSelector(selectOptInsDomain, (substate) => substate.get(key));

const selectRunner = () => getSubstate('runner');
const selectPostState = () => getSubstate('postState');

export { selectOptInsDomain, selectRunner, selectPostState };
