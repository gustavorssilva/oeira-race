const base = 'app/OptIns/';
export const GET_RUNNER_PENDING = `${base}GET_RUNNER_PENDING`;
export const GET_RUNNER_FAILED = `${base}GET_RUNNER_FAILED`;
export const GET_RUNNER_SUCCESS = `${base}GET_RUNNER_SUCCESS`;
export const UPDATE_RUNNER = `${base}UPDATE_RUNNER`;

export const RESET_POST_OPT_INS_STATE = `${base}RESET_POST_OPT_INS_STATE`;
export const POST_OPT_INS_PENDING = `${base}POST_OPT_INS_PENDING`;
export const POST_OPT_INS_FAILED = `${base}POST_OPT_INS_FAILED`;
export const POST_OPT_INS_SUCCESS = `${base}POST_OPT_INS_SUCCESS`;
