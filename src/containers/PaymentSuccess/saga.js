import { call, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import { getFirstErrorMsg } from 'utils/getErrorMsg';
import { BASE_URL } from 'utils/constants';
import { FETCH_PAYMENT_PENDING } from './constants';
import { fetchPaymentRejected, fetchPaymentFulfilled } from './actions';

function getPayment(paymentId) {
  return axios.get(`${BASE_URL}payments/${paymentId}`);
}

function* callGetPayment(action) {
  try {
    const paymentId = action.payload;
    const result = yield call(getPayment, paymentId);
    yield put(fetchPaymentFulfilled(result.data));
  } catch (ex) {
    yield put(fetchPaymentRejected(getFirstErrorMsg(ex)));
  }
}

// Individual exports for testing
export default function* fetchPaymentSaga() {
  yield takeEvery(FETCH_PAYMENT_PENDING, callGetPayment);
}
