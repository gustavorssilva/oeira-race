/**
 *
 * Asynchronously loads the component for PaymentSuccess
 *
 */

import Loadable from 'react-loadable';

export default Loadable({
  loader: () => import('./index'),
  loading: () => null,
});
