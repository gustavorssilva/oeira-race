/*
 * PaymentSuccess Messages
 *
 * This contains all the text for the PaymentSuccess component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.PaymentSuccess.header',
    defaultMessage: 'This is PaymentSuccess container !',
  },
});
