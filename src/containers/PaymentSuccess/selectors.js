import { createSelector } from 'reselect';

/**
 * Direct selector to the paymentSuccess state domain
 */
const selectPaymentSuccessDomain = (state) => state.get('paymentSuccess');

/**
 * Other specific selectors
 */


const selectFetchPaymentState = () => createSelector(
   selectPaymentSuccessDomain,
   (homeState) => homeState.get('payment_fetch_state')
 );


/**
 * Default selector used by PaymentSuccess
 */

const makeSelectPaymentSuccess = () => createSelector(
  selectPaymentSuccessDomain,
  (substate) => substate.toJS()
);

export default makeSelectPaymentSuccess;
export {
  selectPaymentSuccessDomain,
  selectFetchPaymentState,
};
