/*
 *
 * PaymentSuccess constants
 *
 */

export const FETCH_PAYMENT_PENDING = 'app/PaymentSuccess/FETCH_PAYMENT_PENDING';
export const FETCH_PAYMENT_REJECTED = 'app/PaymentSuccess/FETCH_PAYMENT_REJECTED';
export const FETCH_PAYMENT_FULFILLED = 'app/PaymentSuccess/FETCH_PAYMENT_FULFILLED';
