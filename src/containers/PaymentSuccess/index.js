import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators, compose } from 'redux';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import injectIntl from 'utils/injectIntl';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';
import styled from 'styled-components';

import { BigButton } from 'components';
import {
  FullWidthDiv,
  TextCenter,
  OneColumnGrid,
  MultibancoDiv,
} from 'components/styled';
import * as actions from './actions';
import { selectFetchPaymentState } from './selectors';
import reducer from './reducer';
import saga from './saga';

export class PaymentSuccess extends Component {
  static propTypes = {
    match: PropTypes.object.isRequired,
    formatMsg: PropTypes.func.isRequired,
    fetchPayment: PropTypes.func.isRequired,
    fetchPaymentState: PropTypes.object.isRequired,
    location: PropTypes.object.isRequired,
  };

  componentWillMount() {
    const { fetchPayment, match } = this.props;
    const { paymentId } = match.params;
    fetchPayment(paymentId);
  }

  isPaymentSuccessfull() {
    const params = new URLSearchParams(this.props.location.search);
    const result = params.get('result') || 'false';
    return result === 'true';
  }

  renderPaymentFailed(payment) {
    const { formatMsg } = this.props;
    const {
      creditcard_link: link,
      mb_entity: entity,
      mb_reference: ref,
      payment_amount: amount,
      payment_deadline: deadline,
    } = payment;

    return (
      <FullWidthDiv>
        <VisaDiv>
          <h1>{formatMsg(`payment`)}</h1>
          <div>{formatMsg(`payment_failed`)}</div>

          <ButtonAnchor href={link}>
            <BigButton>{formatMsg(`try_again`)}</BigButton>
          </ButtonAnchor>
        </VisaDiv>

        <TextCenter>{formatMsg(`or_pay_with_multibanco`)}</TextCenter>
        <OneColumnGrid>
          <MultibancoDiv>
            <h3>{formatMsg('multibanco_payment_details')}</h3>
            <div>
              <div>{formatMsg('entity')}:</div>
              <div>{entity}</div>
            </div>

            <div>
              <div>{formatMsg('reference')}:</div>
              <div>{ref}</div>
            </div>

            <div>
              <div>{formatMsg('amount')}:</div>
              <div>{amount}€</div>
            </div>

            <div>
              <div>{formatMsg('deadline')}:</div>
              <div>{new Date(deadline).toLocaleDateString()}</div>
            </div>
          </MultibancoDiv>
        </OneColumnGrid>
      </FullWidthDiv>
    );
  }

  render() {
    const { formatMsg } = this.props;
    const { success: payment, errors } = this.props.fetchPaymentState;

    if (errors && errors.message) {
      return <FullWidthDiv>{formatMsg(errors.message)}</FullWidthDiv>;
    }

    if (!payment) return null;
    if (!payment.is_paid || !this.isPaymentSuccessfull()) {
      return this.renderPaymentFailed(payment);
    }

    return (
      <FullWidthDiv>
        <VisaDiv>
          <h1>{formatMsg(`payment`)}</h1>
          <h3>{formatMsg(`congratulations`, { name: payment.name })}</h3>
          <TextCenter>
            {formatMsg(`paid_with_${payment.payment_method}`)}
          </TextCenter>
          <h3>{formatMsg(`good_luck`)}</h3>
        </VisaDiv>
      </FullWidthDiv>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  fetchPaymentState: selectFetchPaymentState(),
});

const mapDispatchToProps = (dispatch) => bindActionCreators(actions, dispatch);

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withIntl = injectIntl('PaymentSuccess');
const withReducer = injectReducer({ key: 'paymentSuccess', reducer });
const withSaga = injectSaga({ key: 'paymentSuccess', saga });

const ButtonAnchor = styled.a`
  text-decoration: none;
  width: 250px;
  margin: 30px auto 0 auto;
`;

const VisaDiv = styled.div`
  text-align: center;
  margin-top: 30px;
  margin-bottom: 30px;
  white-space: pre-wrap;

  h3 {
    width: 100%;
    font-size: 18px;
    color: ${({ theme }) => theme.textColor};
  }

  > div {
    display: grid;
    grid-template-columns: 1fr;
    grid-column-gap: 10px;
    margin-bottom: 30px;

    > div {
      text-align: right;

      &:nth-child(even) {
        text-align: left;
        font-weight: bold;
      }
    }
  }
`;

export default compose(
  withReducer,
  withSaga,
  withConnect,
  withIntl
)(PaymentSuccess);
