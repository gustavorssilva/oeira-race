/*
 *
 * PaymentSuccess actions
 *
 */

import {
  FETCH_PAYMENT_PENDING,
  FETCH_PAYMENT_REJECTED,
  FETCH_PAYMENT_FULFILLED,
} from './constants';


export function fetchPayment(paymentId) {
  return { type: FETCH_PAYMENT_PENDING, payload: paymentId };
}

export function fetchPaymentRejected(errors) {
  return { type: FETCH_PAYMENT_REJECTED, payload: errors };
}

export function fetchPaymentFulfilled(payment) {
  return { type: FETCH_PAYMENT_FULFILLED, payload: payment };
}
