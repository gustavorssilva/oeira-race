/*
 *
 * PaymentSuccess reducer
 *
 */

import { fromJS } from 'immutable';
import {
  FETCH_PAYMENT_PENDING,
  FETCH_PAYMENT_REJECTED,
  FETCH_PAYMENT_FULFILLED,
} from './constants';

const setPaymentFetchState = (fetching, errors, success) => ({ fetching, errors, success });

const initialState = fromJS({
  payment_fetch_state: setPaymentFetchState(false, null, null),
});

function paymentSuccessReducer(state = initialState, action) {
  switch (action.type) {

    case FETCH_PAYMENT_PENDING:
      return state.set('payment_fetch_state', setPaymentFetchState(true, null, null));
    case FETCH_PAYMENT_REJECTED:
      return state.set('payment_fetch_state', setPaymentFetchState(false, action.payload, null));
    case FETCH_PAYMENT_FULFILLED:
      return state.set('payment_fetch_state', setPaymentFetchState(false, null, action.payload));

    default:
      return state;
  }
}

export default paymentSuccessReducer;
