// ========== Common messages ==========
import { defineMessages } from 'react-intl';

export default defineMessages({
  menu_registrations: {
    id: 'app.components.App.menu_registrations',
    defaultMessage: 'Inscreve-te aqui',
  },
  registration_page: {
    id: 'app.components.App.registration_page',
    defaultMessage: 'Inscrições',
  },
  main_page: {
    id: 'app.components.App.main_page',
    defaultMessage: 'Página Principal',
  },
  contacts: {
    id: 'app.components.App.contacts',
    defaultMessage: 'Contactos',
  },
});
