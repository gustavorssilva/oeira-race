/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Helmet from 'react-helmet';
import Registration from 'containers/Registration/Loadable';
import PaymentSuccess from 'containers/PaymentSuccess/Loadable';
import Teams from 'containers/Teams/Loadable';
import JoinTeamResultScreen from 'containers/Teams/JoinTeamResultScreen/Loadable';
import OptInsPage from 'containers/OptInsPage/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import { teamURL } from 'containers/Teams/constants';
import { Footer, Navbar, MainLogo } from 'components/';

const App = () => (
  <div id="wrapper">
    <Helmet>
      <meta property="og:title" content="Oeiras run" />
      <meta property="og:site_name" content={window.location.host} />
      <meta property="og:url" content={window.location.origin} />
      <meta
        property="og:description"
        content="Inscrições para as Oeiras run"
      />
      {/* <meta
        id="facebook-share-image"
        property="og:image"
        content={`${window.location.origin}/fb-banner.jpg"`}
      />
      <meta property="og:image:width" content="301" />
      <meta property="og:image:height" content="229" /> */}
    </Helmet>
    <Navbar />

    <div id="page-content">
      <MainLogo />
      <Switch>
        <Route path="/associar-equipa/:id" component={JoinTeamResultScreen} />
        <Route path={teamURL} component={Teams} />
        <Route path="/payments/:paymentId" component={PaymentSuccess} />
        <Route path="/opt-ins/:runnerId" component={OptInsPage} />
        <Route path="/" component={Registration} />
        <Route component={NotFoundPage} />
      </Switch>
    </div>

    <Footer />
  </div>
);

export default App;
