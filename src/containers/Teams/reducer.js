/*
 *
 * Teams reducer
 *
 */

import { fromJS } from 'immutable';
import {
  // GET_PRICES_SUCCESSFUL,
  // GET_PRICES_FAILED,
  // GET_PRICES_REQUESTING,
  RESET_ALL_STATE,
} from 'containers/Registration/constants';
import {
  SET_TEAM_NAME,
  TEST_TEAM_NAME_PENDING,
  TEST_TEAM_NAME_REJECTED,
  TEST_TEAM_NAME_FULFILLED,
  SET_LEADER,
  SET_TEAM_FILE,
  SET_TEAM_CODE,
  TEST_TEAM_CODE_PENDING,
  TEST_TEAM_CODE_REJECTED,
  TEST_TEAM_CODE_FULFILLED,
  POST_TEAM_FILE_PENDING,
  POST_TEAM_FILE_REJECTED,
  POST_TEAM_FILE_FULFILLED,
  POST_TEAM_REGISTRATION_REQUESTING,
  POST_TEAM_REGISTRATION_FAILED,
  POST_TEAM_REGISTRATION_SUCCESSFUL,
  RESET_TEAM_REGISTRATION_STATE,
  RESET_EVIDENCE_FILES,
  RESET_TEAM_FILE_STATE,
  SET_EVIDENCE_FILES,
  SET_TEAM_DATA,
  SET_TEAM_MEMBER_INDEX,
  SET_TEAM_MEMBER_DETAILS,
  POST_JOIN_TEAM_REQUESTING,
  POST_JOIN_TEAM_FAILED,
  POST_JOIN_TEAM_SUCCESSFUL,
  RESET_JOIN_TEAM_STATE,
  RESET_TEST_TEAM_CODE_STATE,
  RESET_TEST_TEAM_NAME_STATE,
  RESET_JOIN_TYPE_STATE,
  SET_JOIN_TYPE,
  RESET_LINK_REQUEST_STATE,
  GET_JOIN_LINK_REQUESTING,
  GET_JOIN_LINK_FAILED,
  GET_JOIN_LINK_SUCCESSFUL,
  RESET_JOIN_REQUEST_STATE,
  CONFIRM_JOIN_REQUESTING,
  CONFIRM_JOIN_FAILED,
  CONFIRM_JOIN_SUCCESSFUL,
} from './constants';
import { defaultLeaderObject } from './utils';

const setTestTeamState = (fetching, errors, success) => ({
  fetching,
  errors,
  success,
});

const setLeader = (leader) => ({ ...defaultLeaderObject(), ...leader });
const setTeamMemberIndex = (index) => index;
const setJoinType = (type = null) => type;
const setTeamMemberDetails = (details) => ({ ...details });
const setTeamFileState = (file) => file;

const setTestTeamCodeState = (fetching, errors, success) => ({
  fetching,
  errors,
  success,
});

const setBasicState = (requesting = false, error = null, data = null) => ({
  requesting,
  error,
  data,
});

const initialState = fromJS({
  team_name: '',
  test_team_state: setTestTeamState(false, null, null),
  leader_object: setLeader({}),
  team_code: '',
  team_file: null,
  post_team_file: setBasicState(),
  test_team_code_state: setTestTeamCodeState(false, null, null),
  teamRegistrationPost: setBasicState(),
  joinTeamPost: setBasicState(),
  evidenceFiles: {},
  teamMemberDetails: null,
  teamMemberIndex: null,
  joinType: setJoinType(),
  linkRequest: setBasicState(),
  confirmJoin: setBasicState(),
});

function teamsReducer(state = initialState, { type, payload }) {
  switch (type) {
    case SET_TEAM_NAME:
      return state.set('team_name', payload);

    case TEST_TEAM_NAME_PENDING:
      return state.set('test_team_state', setTestTeamState(true, null, null));
    case TEST_TEAM_NAME_REJECTED:
      return state.set(
        'test_team_state',
        setTestTeamState(false, payload, null)
      );
    case TEST_TEAM_NAME_FULFILLED:
      return state.set(
        'test_team_state',
        setTestTeamState(false, null, payload)
      );
    case RESET_TEST_TEAM_NAME_STATE:
      return state.set('test_team_state', setTestTeamState(false, null, null));

    case SET_LEADER:
      return state.set('leader_object', setLeader(payload));

    case SET_TEAM_FILE:
      return state.set('team_file', setTeamFileState(payload));

    case SET_TEAM_CODE:
      return state.set('team_code', payload);

    case RESET_TEAM_FILE_STATE:
      return state.set('post_team_file', setBasicState());
    case POST_TEAM_FILE_PENDING:
      return state.set('post_team_file', setBasicState(true));
    case POST_TEAM_FILE_REJECTED:
      return state.set('post_team_file', setBasicState(false, payload));
    case SET_TEAM_DATA:
    case POST_TEAM_FILE_FULFILLED:
      return state.set('post_team_file', setBasicState(false, null, payload));

    case RESET_JOIN_TEAM_STATE:
      return state.set('joinTeamPost', setBasicState());
    case POST_JOIN_TEAM_REQUESTING:
      return state.set('joinTeamPost', setBasicState(true));
    case POST_JOIN_TEAM_FAILED:
      return state.set('joinTeamPost', setBasicState(false, payload));
    case POST_JOIN_TEAM_SUCCESSFUL:
      return state.set('joinTeamPost', setBasicState(false, null, payload));

    case TEST_TEAM_CODE_PENDING:
      return state.set(
        'test_team_code_state',
        setTestTeamCodeState(true, null, null)
      );
    case TEST_TEAM_CODE_REJECTED:
      return state.set(
        'test_team_code_state',
        setTestTeamCodeState(false, payload, null)
      );
    case TEST_TEAM_CODE_FULFILLED:
      return state.set(
        'test_team_code_state',
        setTestTeamCodeState(false, null, payload)
      );
    case RESET_TEST_TEAM_CODE_STATE:
      return state.set(
        'test_team_code_state',
        setTestTeamCodeState(false, null, null)
      );

    case POST_TEAM_REGISTRATION_REQUESTING:
      return state.set('teamRegistrationPost', setBasicState(true));
    case POST_TEAM_REGISTRATION_FAILED:
      return state.set('teamRegistrationPost', setBasicState(false, payload));
    case POST_TEAM_REGISTRATION_SUCCESSFUL:
      return state.set(
        'teamRegistrationPost',
        setBasicState(false, null, payload)
      );
    case RESET_TEAM_REGISTRATION_STATE:
      return state.set('teamRegistrationPost', setBasicState());

    case RESET_EVIDENCE_FILES:
      return state.set('evidenceFiles', {});
    case SET_EVIDENCE_FILES:
      return state.set('evidenceFiles', { ...payload });

    case SET_TEAM_MEMBER_INDEX:
      return state.set('teamMemberIndex', setTeamMemberIndex(payload));
    case SET_TEAM_MEMBER_DETAILS:
      return state.set('teamMemberDetails', setTeamMemberDetails(payload));

    case RESET_JOIN_TYPE_STATE:
      return state.set('joinType', setJoinType());
    case SET_JOIN_TYPE:
      return state.set('joinType', setJoinType(payload));

    case RESET_LINK_REQUEST_STATE:
      return state.set('linkRequest', setBasicState());
    case GET_JOIN_LINK_REQUESTING:
      return state.set('linkRequest', setBasicState(true));
    case GET_JOIN_LINK_FAILED:
      return state.set('linkRequest', setBasicState(false, payload));
    case GET_JOIN_LINK_SUCCESSFUL:
      return state.set('linkRequest', setBasicState(false, null, payload));

    case RESET_JOIN_REQUEST_STATE:
      return state.set('confirmJoin', setBasicState());
    case CONFIRM_JOIN_REQUESTING:
      return state.set('confirmJoin', setBasicState(true));
    case CONFIRM_JOIN_FAILED:
      return state.set('confirmJoin', setBasicState(false, payload));
    case CONFIRM_JOIN_SUCCESSFUL:
      return state.set('confirmJoin', setBasicState(false, null, payload));

    case RESET_ALL_STATE:
      return state
        .set(`team_name`, '')
        .set(`test_team_state`, setTestTeamState(false, null, null))
        .set(`leader_object`, setLeader({}))
        .set(`team_code`, '')
        .set(`team_file`, null)
        .set(`post_team_file`, setBasicState())
        .set(`test_team_code_state`, setTestTeamCodeState(false, null, null))
        .set(`teamRegistrationPost`, setBasicState())
        .set(`joinTeamPost`, setBasicState())
        .set(`evidenceFiles`, {})
        .set(`teamMemberDetails`, null)
        .set(`teamMemberIndex`, null)
        .set(`joinType`, setJoinType())
        .set(`linkRequest`, setBasicState())
        .set(`confirmJoin`, setBasicState());

    default:
      return state;
  }
}

export default teamsReducer;
