import _ from 'lodash';
import React, { Component } from 'react';
import { func, object, string } from 'prop-types';
import { scrollToTop, validateStartBoxFile } from 'utils/functions';
import { getTeam } from 'utils/sessions';
import { CloseIcon, DeleteIcon } from 'assets/icons';
import { SmallButton, TwoButtonDiv } from 'components/styled';
import DetailsConfirmationPage from 'components/DetailsConfirmationPage';
import {
  FULL_NAME,
  GENDER,
  MALE,
  ID_NUMBER,
  START_BOX,
  CREDIT_CARD,
  PAYMENT_METHOD,
  START_BOX_FAST_RUNNERS,
  EVIDENCE_UPLOAD,
  SHIRT_SIZE,
  CHIP,
} from 'containers/Registration/constants';
import {
  hasAllInvoiceInfo,
  hasNoInvoiceInfo,
} from 'containers/Registration/utils';
import {
  PAYMENT_CONFIRMATION_URL,
  EDIT_TEAM_MEMBER_URL,
  UPLOAD_EXCEL_URL,
} from '../constants';

class ConfirmDetails extends Component {
  static propTypes = {
    formatMsg: func.isRequired,
    history: object.isRequired,
    pushToStart: func.isRequired,
    postTeamRegistration: func.isRequired,
    filePostState: object.isRequired,
    appliedPromoCode: object.isRequired,
    shirtSizesState: object.isRequired,
    leader: object.isRequired,
    setLeader: func.isRequired,
    promoCode: string,
    applyPromoCode: func.isRequired,
    setTeamData: func.isRequired,
    setTeamMemberIndex: func.isRequired,
    setTeamMemberDetails: func.isRequired,
    resetTeamRegistrationState: func.isRequired,
  };

  state = {
    showDetails: null,
    errorMsg: '',
    selectedPayment: null,
    teamName: getTeam(),
  };

  componentWillMount() {
    const {
      pushToStart,
      resetTeamRegistrationState,
      leader,
      setLeader,
      promoCode,
      appliedPromoCode,
      applyPromoCode,
      filePostState: { data: runners },
    } = this.props;
    if (!this.shouldRender()) pushToStart();

    resetTeamRegistrationState();
    if (!leader.discount_code && promoCode) {
      setLeader({ ...leader, discount_code: promoCode });
    }

    if (promoCode && !appliedPromoCode.data) {
      applyPromoCode({ promoCode, registrations: runners });
    }

    scrollToTop();
  }

  componentWillReceiveProps(nextProps) {
    this.checkForPaymentResponse(nextProps);
    this.checkIfFree(nextProps);
  }

  setErrorMsg = (errorMsg) => this.setState({ errorMsg });

  setDeleteConfirmation(runner) {
    const { formatMsg } = this.props;
    const article = runner[GENDER] === MALE ? 'o' : 'a';
    const name = `${article} ${runner[FULL_NAME]}`;
    const modalContent = (
      <div>
        <div>{formatMsg('confirm_delete', { name })}</div>
        <TwoButtonDiv>
          <SmallButton onClick={() => this.setErrorMsg('')}>
            <img src={CloseIcon} alt="" />
            <span>{formatMsg('cancel')}</span>
          </SmallButton>
          <SmallButton onClick={() => this.handleDeleteTeamMember(runner)}>
            <img src={DeleteIcon} alt="" />
            <span>{formatMsg('delete')}</span>
          </SmallButton>
        </TwoButtonDiv>
      </div>
    );

    this.setState({ errorMsg: modalContent });
  }

  shouldRender() {
    const {
      filePostState: { data: runners },
      leader,
    } = this.props;
    const { teamName } = this.state;
    return runners && leader && teamName;
  }

  checkForPaymentResponse(props) {
    const {
      history: { push },
      resetTeamRegistrationState,
      teamRegistrationState: { data, error },
    } = props;

    if (!data && !error) return;

    if (error) {
      resetTeamRegistrationState();
      return;
    }

    const { payment_method: method, creditcard_link: link } = data;

    if (method === CREDIT_CARD && link) {
      window.location = link;
      return;
    }

    push(PAYMENT_CONFIRMATION_URL);
  }

  checkIfFree(props) {
    const { appliedPromoCode, setLeader, leader } = props;

    if (!this.isOrderFree(appliedPromoCode)) return;

    if (!leader[PAYMENT_METHOD]) {
      const newDetails = { ...leader, [PAYMENT_METHOD]: CREDIT_CARD };
      this.setState({ selectedPayment: 'visa' });
      setLeader(newDetails);
    }
  }

  isOrderFree(appliedPromoCode) {
    const dataToCheck = appliedPromoCode || this.props.appliedPromoCode;
    const totalPriceWithPromoCode = _.at(dataToCheck, 'data.total_price')[0];
    return totalPriceWithPromoCode === 0;
  }

  runnerNeedsFile = (runner) => {
    const { [START_BOX]: startBox, [EVIDENCE_UPLOAD]: file } = runner;
    return startBox === START_BOX_FAST_RUNNERS && !file;
  };

  runnerHasWrongShirt = (runner) => {
    const { data } = this.props.shirtSizesState;
    const availableShirts = _.find(data, {
      type: runner[CHIP],
      gender: runner[GENDER],
    }).sizes;

    return !_.includes(availableShirts, runner[SHIRT_SIZE]);
  };

  findRunnerWithoutEvidence() {
    const { data: runners } = this.props.filePostState;
    const problemRunners = _.filter(runners, this.runnerNeedsFile);

    if (_.isEmpty(problemRunners)) return null;
    return _.map(problemRunners, FULL_NAME).join(', ');
  }

  findRunnerWithWrongShirt() {
    const { data: runners } = this.props.filePostState;
    const problemRunners = _.filter(runners, this.runnerHasWrongShirt);

    if (_.isEmpty(problemRunners)) return null;
    return _.map(problemRunners, FULL_NAME).join(', ');
  }

  handleContinue = () => {
    const {
      appliedPromoCode: { data: codeData },
      filePostState: { data: runners },
      formatMsg,
      postTeamRegistration,
      leader,
    } = this.props;

    const runnerWithoutEvidence = this.findRunnerWithoutEvidence();
    if (runnerWithoutEvidence) {
      const errorMsg = formatMsg('please_upload_evidence_for', {
        name: runnerWithoutEvidence,
      });
      return this.setErrorMsg(errorMsg);
    }

    const runnerWithWrongShirt = this.findRunnerWithWrongShirt();
    if (runnerWithWrongShirt) {
      const errorMsg = formatMsg('please_change_shirt_size_for', {
        name: runnerWithWrongShirt,
      });
      return this.setErrorMsg(errorMsg);
    }

    if (!hasAllInvoiceInfo(leader) && !hasNoInvoiceInfo(leader)) {
      return this.setErrorMsg(formatMsg('check_invoice_fields'));
    }

    // remove discount code if the user didn't click the '+' button
    if (!codeData) delete leader.discount_code;

    return postTeamRegistration({ leader, runners });
  };

  handleDeleteTeamMember = (runner) => {
    const {
      filePostState: { data: runners },
      setTeamData,
    } = this.props;
    if (runners.length === 1) return;
    const index = _.indexOf(runners, runner);
    runners.splice(index, 1);
    setTeamData(runners);
    this.setState({ errorMsg: '', showDetails: null });
  };

  handleEditClick = (runner) => {
    const {
      filePostState: { data: runners },
      history: { push },
      setTeamMemberIndex,
      setTeamMemberDetails,
    } = this.props;

    setTeamMemberIndex(_.indexOf(runners, runner));
    setTeamMemberDetails(runner);
    push(EDIT_TEAM_MEMBER_URL);
  };

  handleFileChange = (e, id) => {
    const {
      setTeamData,
      filePostState: { data: runners },
    } = this.props;
    const file = e.target.files[0];

    const error = validateStartBoxFile(file);
    if (error) return this.setErrorMsg(error);

    const newTeamData = _.map(runners, (runner) => {
      const runnerData = { ...runner };
      if (runner[ID_NUMBER] !== id) return runnerData;
      runnerData[EVIDENCE_UPLOAD] = file;
      return runnerData;
    });

    return setTeamData(newTeamData);
  };

  render() {
    if (!this.shouldRender()) return null;
    const {
      filePostState: { data: runners },
      formatMsg,
      leader,
      setLeader,
    } = this.props;
    const { errorMsg, teamName } = this.state;

    if (!leader || !teamName) return null;

    return (
      <DetailsConfirmationPage
        title={formatMsg('details_confirmation')}
        previousPage={UPLOAD_EXCEL_URL}
        mainData={leader}
        allRunners={runners}
        handleDeleteClick={this.setDeleteConfirmation}
        handleEditClick={this.handleEditClick}
        handleContinue={this.handleContinue}
        updateMainData={setLeader}
        errorMsg={errorMsg}
        dismissError={() => this.setErrorMsg('')}
        teamName={teamName}
        handleFileChange={this.handleFileChange}
      />
    );
  }
}

export default ConfirmDetails;
