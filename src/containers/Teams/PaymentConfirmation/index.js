import _ from 'lodash';
import React, { Component } from 'react';
import { func, object } from 'prop-types';
import { BigButton } from 'components';
import {
  FullWidthDiv,
  BoldCaps,
  OneColumnGrid,
  TextCenter,
  MultibancoDiv,
} from 'components/styled';
import {
  BILLING_NAME,
  FULL_NAME,
  MANAGER_NAME,
  PAYMENT_METHOD,
  MULTIBANCO,
  GENDER,
  MALE,
} from 'containers/Registration/constants';
import { scrollToTop } from 'utils/functions';
import { getTeam } from 'utils/sessions';

class PaymentConfirmation extends Component {
  static propTypes = {
    formatMsg: func.isRequired,
    // getPrices: func.isRequired,
    getShirts: func.isRequired,
    teamRegistrationState: object.isRequired,
    joinTeamPostState: object.isRequired,
    pushToStart: func.isRequired,
    resetPromoCodeState: func.isRequired,
    resetAllState: func.isRequired,
  };

  componentDidMount() {
    scrollToTop();
    const { pushToStart, resetPromoCodeState } = this.props;
    if (!this.shouldRender()) pushToStart();
    resetPromoCodeState();
  }

  componentWillUnmount() {
    const {
      // getPrices,
      getShirts,
      resetAllState,
    } = this.props;

    resetAllState();
    // getPrices();
    getShirts();
  }

  getPaymentData() {
    const { teamRegistrationState, joinTeamPostState } = this.props;
    return teamRegistrationState.data || joinTeamPostState.data;
  }

  handleContinue = () => this.props.pushToStart();

  isNewTeam() {
    return !!this.props.teamRegistrationState.data;
  }

  shouldRender() {
    try {
      const { payment_amount: amount } = this.getPaymentData();
      return _.isNumber(amount);
    } catch (e) {
      console.warn('redirecting because:', e.toString());
      return false;
    }
  }

  renderConfirmationMsgByType() {
    const { formatMsg } = this.props;
    const customer = this.getPaymentData();
    const {
      mb_entity: entity,
      mb_reference: ref,
      payment_amount: amount,
    } = customer;

    const wasFree = amount === 0;
    const paymentMethod = customer[PAYMENT_METHOD];
    const msgId = `paid_with_${wasFree ? 'creditcard' : paymentMethod}`;
    const paymentConfirmationMsg = formatMsg(msgId);

    if (paymentMethod !== MULTIBANCO || wasFree) {
      return <TextCenter>{paymentConfirmationMsg}</TextCenter>;
    }

    return (
      <OneColumnGrid>
        <MultibancoDiv>
          <h3>{formatMsg('multibanco_payment_details')}</h3>
          <div>
            <div>{formatMsg('entity')}:</div>
            <div>{entity}</div>
          </div>

          <div>
            <div>{formatMsg('reference')}:</div>
            <div>{ref}</div>
          </div>

          <div>
            <div>{formatMsg('amount')}:</div>
            <div>{amount}€</div>
          </div>
        </MultibancoDiv>
        <TextCenter>{paymentConfirmationMsg}</TextCenter>
      </OneColumnGrid>
    );
  }

  renderTeamNameMsg() {
    const { formatMsg } = this.props;
    const team = getTeam();
    if (!team) return null;

    if (this.isNewTeam()) {
      return <TextCenter>{formatMsg('team_created', { team })}</TextCenter>;
    }

    const customer = this.getPaymentData();
    let name = customer[FULL_NAME];
    const billingName = customer[BILLING_NAME];
    if (name === billingName) {
      return (
        <TextCenter>{formatMsg('team_joined_by_you', { team })}</TextCenter>
      );
    }
    const article = customer[GENDER] === MALE ? 'O' : 'A';
    name = `${article} ${name}`;

    return <TextCenter>{formatMsg('team_updated', { name, team })}</TextCenter>;
  }

  render() {
    if (!this.shouldRender()) return null;

    const { formatMsg } = this.props;
    const customer = this.getPaymentData();
    const name = customer[FULL_NAME] || customer[MANAGER_NAME];

    return (
      <FullWidthDiv>
        <h1>{formatMsg('payment')}</h1>
        <BoldCaps style={{ margin: 20 }}>
          {formatMsg('congratulations', { name })}
        </BoldCaps>

        {this.renderTeamNameMsg()}

        {this.renderConfirmationMsgByType()}

        <BoldCaps style={{ marginTop: 20, marginBottom: 40 }}>
          {formatMsg('good_luck')}
        </BoldCaps>

        <BigButton onClick={this.handleContinue}>
          {formatMsg('go_back')}
        </BigButton>
      </FullWidthDiv>
    );
  }
}

export default PaymentConfirmation;
