import _ from 'lodash';
import React, { Component } from 'react';
import { func, number, object } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators, compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { PreviewIcon } from 'assets/icons';
import { scrollToTop, validateStartBoxFile } from 'utils/functions';
import injectReducer from 'utils/injectReducer';
import injectSaga from 'utils/injectSaga';
import {
  BackButton,
  BigButton,
  DatePicker,
  Dropdown,
  FormField,
  HelpIcon,
  Modal,
  RadioSelect,
  SecondaryButton,
  DorsalPreview,
  UploadField,
} from 'components';
import {
  CenteredBoldText,
  FullWidthDiv,
  OneColumnGrid,
} from 'components/styled';
import { PORTUGAL, HIDE_DORSAL_PREVIEW_BUTTON } from 'utils/constants';
import {
  CHIP,
  ID_NUMBER,
  DOB,
  FULL_NAME,
  COUNTRY_OF_RESIDENCE,
  DISTRICT,
  EMAIL,
  EMAIL_CONFIRMATION,
  PHONE,
  GENDER,
  SHIRT_SIZE,
  DORSAL_NAME,
  START_BOX,
  START_BOX_FAST_RUNNERS,
  OPTIONS_BY_FIELD,
  MODAL_MSGS,
  DROPDOWN_OPTIONS_BY_FIELD,
  EVIDENCE_UPLOAD,
} from 'containers/Registration/constants';
import {
  getIDCheck,
  getIDCheckFailed,
  resetIDCheckState,
} from 'containers/Registration/actions';
import messages from 'containers/Registration/messages';
import { applyRequiredChanges, hasChip } from 'containers/Registration/utils';
import { selectIDCheckState } from 'containers/Registration/selectors';
import reducer from 'containers/Registration/reducer';
import saga from 'containers/Registration/saga';
import { CONFIRM_DETAILS_URL } from '../constants';
import { validateTeamMemberDetails } from '../utils';

class EditTeamMember extends Component {
  state = { modalContent: '', showDorsalPreview: false };

  componentWillMount() {
    scrollToTop();
    if (!this.shouldRender()) this.goBack();
  }

  componentWillReceiveProps(nextProps) {
    const { error } = nextProps.idCheck;
    if (!error) return;
    this.setModal(error);
  }

  getErrors() {
    const { formatMsg, teamMember } = this.props;
    const shirtSizes = this.getShirtSizes();
    const errors = validateTeamMemberDetails(teamMember, shirtSizes);
    if (!errors) return null;
    return _.map(_.castArray(errors), formatMsg).join('\n');
  }

  getShirtSizes() {
    const {
      shirtSizesState: { data },
      teamMember: runner,
    } = this.props;

    return _.find(data, { type: runner.type, gender: runner.gender }).sizes;
  }

  setModal = (modalContent) => this.setState({ modalContent });

  setIDCheck(id) {
    clearTimeout(this.state.timeout);
    if (!id) return;
    this.setState({ timeout: setTimeout(this.checkID(id), 1000) });
  }

  checkID = (id) => () => {
    const {
      checkID,
      checkIDFailed,
      filePostState: { data: runners },
    } = this.props;

    if (_.filter(runners, [ID_NUMBER, id]).length > 1) {
      // set error message
      checkIDFailed(`Já alguém tem o BI/CC inserido (${id})`);
      return;
    }

    checkID(id);
  };

  shouldRender() {
    const { teamMember, teamMemberIndex } = this.props;
    return teamMember && _.isNumber(teamMemberIndex);
  }

  handleBackClick = () => {
    this.resetEditState();
    this.goBack();
  };

  handleChange(changeData) {
    const { setTeamMemberDetails, teamMember } = this.props;
    const isIdChange = _(changeData)
      .keys()
      .includes(ID_NUMBER);

    if (isIdChange) {
      this.setIDCheck(changeData[ID_NUMBER]);
    }

    applyRequiredChanges(teamMember, changeData);
    setTeamMemberDetails({ ...teamMember, ...changeData });
  }

  handleImageChange(e, field) {
    const file = e.target.files[0];
    const error = validateStartBoxFile(file);
    if (error) return this.setModal(error);

    return this.handleChange({ [field]: file });
  }

  handleSave = () => {
    const {
      idCheck: { error },
      teamMember,
      setTeamData,
      filePostState: { data: runners },
      teamMemberIndex: index,
    } = this.props;

    if (error) {
      return this.setModal(error);
    }

    runners[index] = teamMember;
    setTeamData(runners);
    this.resetEditState();
    return this.goBack();
  };

  goBack = () => this.props.history.push(CONFIRM_DETAILS_URL);

  resetEditState() {
    const { setTeamMemberIndex, setTeamMemberDetails } = this.props;
    setTeamMemberDetails(null);
    setTeamMemberIndex(null);
  }

  renderContinueButton() {
    const { formatMsg } = this.props;
    const errors = this.getErrors();
    const onClick = errors ? () => this.setModal(errors) : this.handleSave;

    return <BigButton onClick={onClick}>{formatMsg('save')}</BigButton>;
  }

  renderHelpIcon(field) {
    const modalContent = this.props.formatMsg(MODAL_MSGS[field]);

    return (
      <HelpIcon
        onClick={() => this.setState({ modalContent, showModal: true })}
      />
    );
  }

  renderHelpModal() {
    const { modalContent } = this.state;
    if (!modalContent) return null;
    return <Modal onClick={() => this.setModal(null)}>{modalContent}</Modal>;
  }

  renderDorsalPreview() {
    const { showDorsalPreview } = this.state;
    const { teamMember } = this.props;
    if (!showDorsalPreview) return null;
    return (
      <DorsalPreview
        runner={teamMember}
        handleClose={() => this.setState({ showDorsalPreview: false })}
      />
    );
  }

  renderDatePicker(field) {
    const { formatMsg, teamMember } = this.props;

    return (
      <DatePicker
        placeholder={formatMsg(`short_${field}`)}
        date={teamMember[field]}
        onChange={(value) => this.handleChange({ [field]: value })}
      />
    );
  }

  renderDropdown(field) {
    const { formatMsg, teamMember } = this.props;

    if (field === DISTRICT && teamMember[COUNTRY_OF_RESIDENCE] !== PORTUGAL) {
      return null;
    }

    const options = DROPDOWN_OPTIONS_BY_FIELD[field];

    return (
      <Dropdown
        value={teamMember[field]}
        placeholder={formatMsg(field)}
        onChange={(value) => this.handleChange({ [field]: value })}
        options={options}
      />
    );
  }

  renderRadioSelect(field) {
    const { formatMsg, teamMember } = this.props;
    const isShirt = SHIRT_SIZE === field;
    const options = _.map(OPTIONS_BY_FIELD[field], (option) => {
      let text = ``;
      let value = option;

      if (_.isPlainObject(option)) {
        value = option.value;
        text = formatMsg(option.text);
      } else {
        text = !isShirt && messages[value] ? formatMsg(value) : value;
      }

      const isDisabled = isShirt && !_.includes(this.getShirtSizes(), value);
      return { text, value, isDisabled };
    });

    const extraInfo =
      field === START_BOX ? formatMsg(`start_box_explanation`) : null;

    return (
      <RadioSelect
        title={formatMsg(field)}
        options={options}
        selected={teamMember[field]}
        handleClick={(option) => this.handleChange({ [field]: option })}
        extraInfo={extraInfo}
      />
    );
  }

  renderField(field, type) {
    const { formatMsg, teamMember } = this.props;

    return (
      <FormField
        field={field}
        placeholder={formatMsg(field)}
        value={teamMember[field]}
        onChange={(e) => this.handleChange({ [field]: e.target.value })}
        type={type || 'text'}
      />
    );
  }

  renderDorsalPreviewButton() {
    if (HIDE_DORSAL_PREVIEW_BUTTON) return null;
    const { formatMsg } = this.props;

    return (
      <SecondaryButton
        onClick={() => this.setState({ showDorsalPreview: true })}
        icon={<img src={PreviewIcon} alt="icon_preview" />}
      >
        {formatMsg('preview_dorsal')}
      </SecondaryButton>
    );
  }

  renderUploadField(field) {
    const { formatMsg, teamMember } = this.props;
    if (teamMember[START_BOX] !== START_BOX_FAST_RUNNERS) {
      return null;
    }

    return (
      <UploadField
        field={field}
        text={formatMsg(field)}
        file={teamMember[field]}
        onChange={(e) => this.handleImageChange(e, field)}
        fileTypes={['.jpg', '.jpeg', '.pdf']}
      />
    );
  }

  render() {
    if (!this.shouldRender()) return null;
    const { formatMsg, teamMember } = this.props;
    const withChip = hasChip(teamMember);

    return (
      <FullWidthDiv>
        <BackButton onClick={this.handleBackClick} />
        <h1>{formatMsg('edit_team_member_title')}</h1>
        <CenteredBoldText style={{ fontSize: '18px' }}>
          {formatMsg('fill_all_fields')}
        </CenteredBoldText>

        <OneColumnGrid style={{ width: '100%' }}>
          {this.renderRadioSelect(CHIP)}
          {this.renderField(ID_NUMBER)}
          {this.renderDatePicker(DOB)}
          {this.renderField(FULL_NAME)}
          {this.renderDropdown(COUNTRY_OF_RESIDENCE)}
          {this.renderDropdown(DISTRICT)}
          {this.renderField(EMAIL, 'email')}
          {this.renderField(EMAIL_CONFIRMATION, 'email')}
          {this.renderField(PHONE)}
          {this.renderRadioSelect(GENDER)}
          {this.renderRadioSelect(SHIRT_SIZE)}
          {this.renderField(DORSAL_NAME)}
          {this.renderDorsalPreviewButton()}
          {withChip && this.renderRadioSelect(START_BOX)}
          {withChip && this.renderUploadField(EVIDENCE_UPLOAD)}
        </OneColumnGrid>

        {this.renderContinueButton()}
        {this.renderHelpModal()}
        {this.renderDorsalPreview()}
      </FullWidthDiv>
    );
  }
}

EditTeamMember.propTypes = {
  checkID: func.isRequired,
  checkIDFailed: func.isRequired,
  formatMsg: func.isRequired,
  setTeamData: func.isRequired,
  setTeamMemberDetails: func.isRequired,
  setTeamMemberIndex: func.isRequired,
  filePostState: object.isRequired,
  history: object.isRequired,
  idCheck: object.isRequired,
  shirtSizesState: object.isRequired,
  teamMember: object.isRequired,
  teamMemberIndex: number.isRequired,
};

const mapStateToProps = createStructuredSelector({
  idCheck: selectIDCheckState(),
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      checkID: getIDCheck,
      checkIDFailed: getIDCheckFailed,
      resetCheckState: resetIDCheckState,
    },
    dispatch
  );

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
const withReducer = injectReducer({ key: 'registration', reducer });
const withSaga = injectSaga({ key: 'registration', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(EditTeamMember);
