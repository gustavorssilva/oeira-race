import { createSelector } from 'reselect';

/**
 * Direct selector to the teams state domain
 */
const selectTeamsDomain = (state) => state.get('teams');

const getSubstate = (substate) =>
  createSelector(
    selectTeamsDomain,
    (state) => state.get(substate)
  );

/**
 * Other specific selectors
 */

const selectTeamName = () => getSubstate('team_name');

const selectTestTeamState = () => getSubstate('test_team_state');

const selectLeader = () => getSubstate('leader_object');
const selectTeamFileState = () => getSubstate('team_file');
const selectTeamFilePostState = () => getSubstate('post_team_file');

const selectTeamCode = () =>
  createSelector(
    selectTeamsDomain,
    (homeState) => homeState.get('team_code')
  );

const selectTestTeamCodeState = () => getSubstate('test_team_code_state');
const selectTeamRegistrationState = () => getSubstate('teamRegistrationPost');
const selectJoinTeamPostState = () => getSubstate('joinTeamPost');
const selectEvidenceFilesState = () => getSubstate('evidenceFiles');
const selectTeamMemberIndex = () => getSubstate('teamMemberIndex');
const selectTeamMember = () => getSubstate('teamMemberDetails');
const selectJoinType = () => getSubstate('joinType');
const selectLinkRequest = () => getSubstate('linkRequest');
const selectConfirmJoinRequest = () => getSubstate('confirmJoin');

/**
 * Default selector used by Teams
 */

const makeSelectTeams = () =>
  createSelector(
    selectTeamsDomain,
    (substate) => substate.toJS()
  );

export default makeSelectTeams;
export {
  selectTeamsDomain,
  selectTeamName,
  selectTestTeamState,
  selectLeader,
  selectTeamCode,
  selectTestTeamCodeState,
  selectTeamFileState,
  selectTeamFilePostState,
  selectTeamRegistrationState,
  selectEvidenceFilesState,
  selectTeamMember,
  selectTeamMemberIndex,
  selectJoinTeamPostState,
  selectJoinType,
  selectLinkRequest,
  selectConfirmJoinRequest,
};
