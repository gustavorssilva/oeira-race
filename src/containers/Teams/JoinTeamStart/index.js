import React, { Component } from 'react';
import { func, object, string } from 'prop-types';
import { BackButton, BigButton, RadioSelect } from 'components';
import { FullWidthDiv, MarginTop } from 'components/styled';
import { scrollToTop } from 'utils/functions';
import {
  ENTER_TEAM_CODE_URL,
  JOIN_TYPE_NEW,
  JOIN_TYPE_EXISTING,
} from '../constants';

class JoinTeamStart extends Component {
  componentWillMount() {
    const { resetJoinTypeState } = this.props;
    scrollToTop();
    return resetJoinTypeState();
  }

  handleContinue = () => {
    const {
      history: { push },
      joinType,
    } = this.props;

    if (!joinType) return;
    push(ENTER_TEAM_CODE_URL);
  };

  handleSelect = (joinType) => this.props.setJoinType(joinType);

  render() {
    const { formatMsg, joinType, pushToStart } = this.props;
    const options = [
      { value: JOIN_TYPE_EXISTING, text: formatMsg('yes') },
      { value: JOIN_TYPE_NEW, text: formatMsg('no') },
    ];

    return (
      <FullWidthDiv>
        <BackButton onClick={pushToStart} />

        <h1>{formatMsg('join_team')}</h1>

        <RadioSelect
          title={formatMsg('has_the_runner_already_registered')}
          options={options}
          selected={joinType}
          handleClick={this.handleSelect}
        />
        <MarginTop>
          <BigButton disabled={!joinType} onClick={this.handleContinue}>
            {formatMsg('continue')}
          </BigButton>
        </MarginTop>
      </FullWidthDiv>
    );
  }
}

JoinTeamStart.propTypes = {
  formatMsg: func.isRequired,
  pushToStart: func.isRequired,
  resetJoinTypeState: func.isRequired,
  setJoinType: func.isRequired,
  history: object.isRequired,
  joinType: string,
};

export default JoinTeamStart;
