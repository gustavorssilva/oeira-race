import React from 'react';
import { func, object, string } from 'prop-types';
import { MagnifyIcon, LoadingIcon, CheckIcon } from 'mdi-react';
import {
  CenteredBoldText,
  FullWidthDiv,
  ErrorText,
  TestButton,
  MarginTop,
  RelativeFormFieldDiv,
} from 'components/styled';
import { BackButton, BigButton, FormField } from 'components';
import { scrollToTop } from 'utils/functions';
import { saveTeamCode } from 'utils/sessions';
import {
  CONFIRM_TEAM_URL,
  DOWNLOAD_EXCEL_URL,
  JOIN_TEAM_START_URL,
} from '../constants';

class EnterTeamCode extends React.Component {
  componentWillMount() {
    const {
      history: { push },
      resetTestTeamCodeState,
      setTeamCode,
    } = this.props;
    if (!this.shouldRender()) return push(JOIN_TEAM_START_URL);

    resetTestTeamCodeState();
    setTeamCode('');
    return scrollToTop();
  }

  shouldRender() {
    return !!this.props.joinType;
  }

  handleChangeTeamCode(e) {
    const { resetTestTeamCodeState, setTeamCode } = this.props;
    const { value } = e.target;
    resetTestTeamCodeState();
    setTeamCode(value);
  }

  handleContinue = () => {
    const {
      history: { push },
      teamCode,
    } = this.props;

    saveTeamCode(teamCode);
    push(CONFIRM_TEAM_URL);
  };

  renderTestNameButton(name) {
    const {
      testTeamCode,
      testTeamCodeState: { fetching, success },
    } = this.props;
    if (!name) return null;
    let icon = <MagnifyIcon size={36} />;

    if (fetching) {
      icon = <LoadingIcon size={36} className="spin" />;
    } else if (success) {
      icon = <CheckIcon size={36} />;
    }

    return <TestButton onClick={() => testTeamCode(name)}>{icon}</TestButton>;
  }

  renderContinueButton() {
    const { formatMsg, teamCode, testTeamCodeState } = this.props;

    // @dev if there is a name and there is not a team with that name in the DB
    const isDisabled = !teamCode || !testTeamCodeState.success;
    return (
      <BigButton disabled={isDisabled} onClick={this.handleContinue}>
        {formatMsg('continue')}
      </BigButton>
    );
  }

  renderErrorFragment() {
    const {
      formatMsg,
      history: { push },
      testTeamCodeState: { errors },
    } = this.props;

    // @dev if it is success there is a team with that name
    if (!errors) return null;

    return (
      <CenteredBoldText>
        <ErrorText>{formatMsg('team_does_not_exist')}</ErrorText>
        <BigButton onClick={() => push(DOWNLOAD_EXCEL_URL)}>
          {formatMsg('create_team')}
        </BigButton>
      </CenteredBoldText>
    );
  }

  render() {
    if (!this.shouldRender()) return null;
    const {
      formatMsg,
      teamCode,
      history: { push },
    } = this.props;

    return (
      <FullWidthDiv>
        <BackButton onClick={() => push(JOIN_TEAM_START_URL)} />
        <h1>{formatMsg('join_team')}</h1>
        <RelativeFormFieldDiv>
          <FormField
            placeholder={formatMsg('team_code')}
            value={teamCode}
            onChange={(evt) => this.handleChangeTeamCode(evt)}
            type="text"
          />
          {this.renderTestNameButton(teamCode)}
        </RelativeFormFieldDiv>

        <MarginTop>
          {this.renderErrorFragment()}
          {this.renderContinueButton()}
        </MarginTop>
      </FullWidthDiv>
    );
  }
}

EnterTeamCode.propTypes = {
  formatMsg: func.isRequired,
  history: object.isRequired,
  joinType: string.isRequired,
  teamCode: string.isRequired,
  setTeamCode: func.isRequired,
  resetTestTeamCodeState: func.isRequired,
  testTeamCode: func.isRequired,
  testTeamCodeState: object.isRequired,
};

export default EnterTeamCode;
