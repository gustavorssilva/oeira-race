import React from 'react';
import { func, object, string } from 'prop-types';
import { MagnifyIcon, LoadingIcon, CheckIcon } from 'mdi-react';
import {
  CenteredBoldText,
  FullWidthDiv,
  ErrorText,
  TestButton,
  RelativeFormFieldDiv,
  MarginTop,
} from 'components/styled';
import { BackButton, BigButton, FormField } from 'components';
import { scrollToTop } from 'utils/functions';
import { saveTeam } from 'utils/sessions';
import {
  DOWNLOAD_EXCEL_URL,
  JOIN_TEAM_START_URL,
  LEADER_INFO_URL,
} from '../constants';

class CreateTeam extends React.Component {
  componentWillMount() {
    const {
      resetEvidenceFilesState,
      setTeamName,
      resetTestTeamNameState,
    } = this.props;

    resetEvidenceFilesState();
    resetTestTeamNameState();
    setTeamName(``);

    scrollToTop();
  }

  handleChangeTeamName = (e) => {
    const { value } = e.target;
    const { setTeamName, resetTestTeamNameState } = this.props;
    resetTestTeamNameState();
    setTeamName(value);
  };

  handleContinue = () => {
    const { teamName, history } = this.props;
    saveTeam(teamName);
    history.push(LEADER_INFO_URL);
  };

  renderTestNameButton(name) {
    const {
      testTeamName,
      testTeamState: { fetching, errors },
    } = this.props;
    if (!name) return null;
    let icon = <MagnifyIcon size={36} />;

    if (fetching) {
      icon = <LoadingIcon size={36} className="spin" />;
    } else if (errors) {
      icon = <CheckIcon size={36} />;
    }

    return <TestButton onClick={() => testTeamName(name)}>{icon}</TestButton>;
  }

  renderContinueButton() {
    const { formatMsg, teamName, testTeamState } = this.props;
    // @dev if there is a name and there is not a team with that name in the DB
    const isDisabled = !teamName || !testTeamState.errors;

    return (
      <BigButton disabled={isDisabled} onClick={this.handleContinue}>
        {formatMsg('continue')}
      </BigButton>
    );
  }

  renderErrorFragment() {
    const { formatMsg, testTeamState } = this.props;

    // @dev if it is success there is a team with that name
    if (!testTeamState.success) return null;

    return (
      <CenteredBoldText>
        <ErrorText>{formatMsg('team_already_exists')}</ErrorText>
        <BigButton
          disabled={false}
          onClick={() => this.props.history.push(JOIN_TEAM_START_URL)}
        >
          {formatMsg('join_team')}
        </BigButton>
      </CenteredBoldText>
    );
  }

  render() {
    const {
      formatMsg,
      teamName,
      history: { push },
    } = this.props;
    return (
      <FullWidthDiv>
        <BackButton onClick={() => push(DOWNLOAD_EXCEL_URL)} />
        <h1>{formatMsg('team_registration_title')}</h1>

        <RelativeFormFieldDiv>
          <FormField
            placeholder={formatMsg('team_name')}
            value={teamName || ''}
            onChange={this.handleChangeTeamName}
            type="text"
          />
          {this.renderTestNameButton(teamName)}
        </RelativeFormFieldDiv>

        <MarginTop>
          {this.renderErrorFragment()}
          {this.renderContinueButton()}
        </MarginTop>
      </FullWidthDiv>
    );
  }
}

CreateTeam.propTypes = {
  formatMsg: func.isRequired,
  history: object.isRequired,
  resetEvidenceFilesState: func.isRequired,
  resetTestTeamNameState: func.isRequired,
  teamName: string.isRequired,
  setTeamName: func.isRequired,
  testTeamName: func.isRequired,
  testTeamState: object.isRequired,
};

export default CreateTeam;
