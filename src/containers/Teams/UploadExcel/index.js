import _ from 'lodash';
import React, { Component } from 'react';
import { func, object } from 'prop-types';
import {
  CenteredBlueItalics,
  CenteredBoldText,
  FullWidthDiv,
  OneColumnGrid,
  TeamTitle,
  TextCenter,
  TwoColTableGrid,
  MarginTop,
} from 'components/styled';
import { BackButton, BigButton, Modal, UploadField } from 'components';
import {
  FILE_TOO_BIG_ERROR,
  MAX_FILE_SIZE,
  FILE_IS_INVALID,
} from 'utils/constants';
import { scrollToTop } from 'utils/functions';
import { KIT_DELIVERY, MANAGER_NAME } from 'containers/Registration/constants';
import { getTeam } from 'utils/sessions';
import { CONFIRM_DETAILS_URL, LEADER_INFO_URL } from '../constants';

class UploadExcel extends Component {
  state = { showModal: false, errorMsg: '' };

  componentWillMount() {
    scrollToTop();
    if (!this.shouldRender()) this.props.pushToStart();
  }

  componentWillReceiveProps(nextProps) {
    const {
      filePostState: { data, error },
      resetTeamFileState,
      history: { push },
      formatMsg,
    } = nextProps;

    if (!data && !error) return null;

    if (error) {
      resetTeamFileState();
      if (_.isString(error)) return this.setErrorMsg(formatMsg(error));
      const fields = _(error)
        .map(formatMsg)
        .uniq()
        .value()
        .join('\n');
      return this.setErrorMsg(
        formatMsg('please_check_the_excel_file', { fields })
      );
    }
    return push(CONFIRM_DETAILS_URL);
  }

  setErrorMsg = (errorMsg) => this.setState({ errorMsg });

  handleContinue = () => {
    const { teamFile, postTeamFile } = this.props;
    return postTeamFile(teamFile);
  };

  handleFileChange = (e) => {
    const { setTeamFile, resetTeamFileState } = this.props;
    const file = e.target.files[0];

    if (!/\.xlsx$/.test(file.name)) {
      return this.setErrorMsg(FILE_IS_INVALID);
    }

    if (file.size > MAX_FILE_SIZE) {
      return this.setErrorMsg(FILE_TOO_BIG_ERROR);
    }

    resetTeamFileState();
    return setTeamFile(file);
  };

  shouldRender() {
    const { leader } = this.props;
    const teamName = getTeam();

    return leader && leader[MANAGER_NAME] && teamName;
  }

  renderContinueButton() {
    const { teamFile, formatMsg } = this.props;

    return (
      <MarginTop>
        <BigButton disabled={!teamFile} onClick={this.handleContinue}>
          {formatMsg('continue')}
        </BigButton>
      </MarginTop>
    );
  }

  renderErrorModal() {
    const { errorMsg } = this.state;
    if (!errorMsg) return null;

    return <Modal onClick={() => this.setErrorMsg('')}>{errorMsg}</Modal>;
  }

  renderUploadButton() {
    const { formatMsg, teamFile: file } = this.props;
    const field = 'team_enrollment_xlsx';

    return (
      <MarginTop>
        <UploadField
          field={field}
          text={formatMsg('upload_excel_file')}
          file={file}
          onChange={this.handleFileChange}
          fileTypes={['.xlsx']}
        />
      </MarginTop>
    );
  }

  render() {
    if (!this.shouldRender()) return null;

    const {
      leader,
      formatMsg,
      history: { push },
    } = this.props;
    const teamName = getTeam();

    if (!leader || !teamName) return null;

    const kitCollectionMethod = _.capitalize(formatMsg(leader[KIT_DELIVERY]));

    return (
      <FullWidthDiv>
        <BackButton onClick={() => push(LEADER_INFO_URL)} />
        <TeamTitle style={{ margin: 0 }}>{teamName}</TeamTitle>

        <TwoColTableGrid>
          <div>{formatMsg('team_leader')}:</div>
          <div>{leader[MANAGER_NAME]}</div>
          <div>{formatMsg('kit_collection_method_short')}:</div>
          <div>{kitCollectionMethod}</div>
        </TwoColTableGrid>

        <OneColumnGrid style={{ width: '100%' }}>
          <h1>{formatMsg('team_registration')}</h1>
          <CenteredBoldText>{formatMsg('check_the_file')}</CenteredBoldText>

          <div>
            <TextCenter>{formatMsg('example')}</TextCenter>
            <CenteredBlueItalics>
              {formatMsg('file_example')}
            </CenteredBlueItalics>
          </div>

          {this.renderUploadButton()}
        </OneColumnGrid>
        {this.renderContinueButton()}
        {this.renderErrorModal()}
      </FullWidthDiv>
    );
  }
}

UploadExcel.propTypes = {
  history: object.isRequired,
  leader: object,
  teamFile: object,
  filePostState: object,
  pushToStart: func.isRequired,
  formatMsg: func.isRequired,
  resetTeamFileState: func.isRequired,
  postTeamFile: func.isRequired,
  setTeamFile: func.isRequired,
};

export default UploadExcel;
