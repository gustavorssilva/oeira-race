import 'formdata-polyfill';
import _ from 'lodash';
import { all, call, put, takeEvery } from 'redux-saga/effects';
import axios from 'axios';
import { getFirstErrorMsg } from 'utils/getErrorMsg';
import { BASE_URL, API_RACE_NAME } from 'utils/constants';
import { getTeam } from 'utils/sessions';
import {
  GET_ID_CHECK_URL,
  TEAM_EXCEL_PARSE_URL,
  TEAM_REGISTRATION_URL,
  ID_NUMBER,
  EVIDENCE_UPLOAD,
  REQUIRED_DATA,
  INVOICE_FIELDS,
  START_BOX,
} from 'containers/Registration/constants';
// import { getPricesSaga } from 'containers/Registration/saga';
import {
  hasAllInvoiceInfo,
  needsEvidence,
  hasChip,
} from 'containers/Registration/utils';
import {
  TEST_TEAM_NAME_PENDING,
  TEST_TEAM_CODE_PENDING,
  POST_TEAM_FILE_PENDING,
  POST_TEAM_REGISTRATION_REQUESTING,
  POST_JOIN_TEAM_REQUESTING,
  GET_JOIN_LINK_REQUESTING,
  CONFIRM_JOIN_REQUESTING,
} from './constants';
import {
  testTeamNameRejected,
  testTeamNameFulfilled,
  testTeamCodeRejected,
  testTeamCodeFulfilled,
  postTeamFileRejected,
  postTeamFileFulfilled,
  postTeamRegistrationSuccessful,
  postTeamRegistrationFailed,
  postJoinTeamSuccessful,
  postJoinTeamFailed,
  requestJoinLinkSuccessful,
  requestJoinLinkFailed,
  confirmJoinRequestFailed,
  confirmJoinRequestSuccessful,
} from './actions';
import { getJoinTeamURL } from './utils';

const getCheckID = (id) => axios.get(`${GET_ID_CHECK_URL}${id}`);

function getTeamByName(teamName) {
  return axios.get(
    `${BASE_URL}${API_RACE_NAME}/findTeam?teamName=${encodeURIComponent(
      teamName
    )}`
  );
}

function getTeamByCode(teamCode) {
  return axios.get(
    `${BASE_URL}${API_RACE_NAME}/findTeam?teamID=${encodeURIComponent(
      teamCode
    )}`
  );
}

function postTeamFile(file) {
  const body = new FormData();
  body.set('team_enrollment_xlsx', file);

  return axios.post(TEAM_EXCEL_PARSE_URL, body);
}

function postTeamRegistration({ leader, runners }) {
  const body = new FormData();
  const { discount_code: discountCode, ...leaderData } = leader;

  // Remove the file from the individual runner objects
  const enrollments = _.map(
    runners,
    ({ [EVIDENCE_UPLOAD]: upload, ...runner }) => runner
  );
  const allData = { ...leaderData, team_name: getTeam(), enrollments };

  if (discountCode) allData.discount_code = discountCode;

  _.each(runners, (runner, i) => {
    const file = runner[EVIDENCE_UPLOAD];
    if (file && needsEvidence(runner)) {
      body.set(`starting_line_proof_${i}`, file);
    }
  });
  body.set('payload', JSON.stringify(allData));

  return axios.post(TEAM_REGISTRATION_URL, body);
}

function postJoinTeam(details) {
  const { runner, teamID } = details;
  const body = new FormData();
  const file = runner[EVIDENCE_UPLOAD];
  const runnerData = _.pick(runner, REQUIRED_DATA);
  const code = runner.discount_code;
  if (code) runnerData.discount_code = code;

  if (hasAllInvoiceInfo(runner)) {
    const invoiceInfo = _.pick(runner, INVOICE_FIELDS);
    _.assign(runnerData, invoiceInfo);
  }

  body.set('payload', JSON.stringify(runnerData));
  if (file) body.set('starting_line_proof', file);

  return axios.post(getJoinTeamURL(teamID), body);
}

function requestJoinLink(data) {
  const { teamId, idNumber, dateOfBirth } = data;
  const idParam = `civilId=${encodeURIComponent(idNumber.trim())}`;
  const dobParam = `birthdate=${encodeURIComponent(dateOfBirth)}`;
  const url = `${BASE_URL}${API_RACE_NAME}/teams/${teamId}/request_join?${idParam}&${dobParam}`;

  return axios.post(url, data, { responseType: 'text' });
}

function confirmJoinTeam(id) {
  return axios.put(
    `${BASE_URL}teams/confirm_join?cypheredID=${encodeURIComponent(id)}`
  );
}

function* callTestTeamName(action) {
  try {
    const teamName = action.payload;
    const result = yield call(getTeamByName, teamName);
    yield put(testTeamNameFulfilled(result.data));
  } catch (e) {
    yield put(testTeamNameRejected(getFirstErrorMsg(e, false)));
  }
}

function* callTestTeamCode(action) {
  try {
    const teamCode = action.payload;
    const { data } = yield call(getTeamByCode, teamCode);
    yield put(testTeamCodeFulfilled(data));
  } catch (e) {
    yield put(testTeamCodeRejected(getFirstErrorMsg(e, false)));
  }
}

function* callPostTeamFile({ payload: file }) {
  try {
    const { data } = yield call(postTeamFile, file);

    const ids = yield data.map(async (runner) => {
      const id = runner[ID_NUMBER];
      const { data: idData } = await getCheckID(id);
      return { id, isDuplicate: idData.is_duplicate };
    });

    let error;
    const fileContainsDuplicates = _.uniqBy(ids, 'id').length !== ids.length;
    const duplicatesInDB = _(ids)
      .filter('isDuplicate')
      .map('id')
      .value()
      .join('\n');

    if (duplicatesInDB) {
      error = `Já alguém se registou com este(s) BI/CC:\n${duplicatesInDB}`;
    } else if (fileContainsDuplicates) {
      error = 'Existem números de BI/CC duplicados na folha.';
    }

    if (error) {
      yield put(postTeamFileRejected(error));
      return;
    }

    const sanitizedData = _.map(data, ({ ...runner }) => {
      if (!hasChip(runner)) runner[START_BOX] = ``;
      return runner;
    });

    yield put(postTeamFileFulfilled(sanitizedData));
  } catch (e) {
    try {
      const fields = _.map(e.response.data.errors, 'field');
      yield put(postTeamFileRejected(fields));
    } catch (ignoreThisError) {
      yield put(postTeamFileRejected(getFirstErrorMsg(e)));
    }
  }
}

function* callPostTeamRegistration({ payload: data }) {
  try {
    const result = yield call(postTeamRegistration, data);
    yield put(postTeamRegistrationSuccessful(result.data));
  } catch (e) {
    yield put(postTeamRegistrationFailed(getFirstErrorMsg(e)));
  }
}

function* callPostJoinTeam({ payload }) {
  try {
    const { data } = yield call(postJoinTeam, payload);
    yield put(postJoinTeamSuccessful(data));
  } catch (e) {
    yield put(postJoinTeamFailed(getFirstErrorMsg(e)));
  }
}

function* callGetJoinLink({ payload }) {
  try {
    yield call(requestJoinLink, payload);
    yield put(requestJoinLinkSuccessful(true));
  } catch (e) {
    yield put(requestJoinLinkFailed(getFirstErrorMsg(e)));
  }
}

function* callConfirmJoinTeam({ payload }) {
  try {
    const { data } = yield call(confirmJoinTeam, payload);
    yield put(confirmJoinRequestSuccessful(data));
  } catch (e) {
    yield put(confirmJoinRequestFailed(getFirstErrorMsg(e)));
  }
}

// Individual exports for testing
function* testTeamNameSaga() {
  yield takeEvery(TEST_TEAM_NAME_PENDING, callTestTeamName);
}

function* testTeamCodeSaga() {
  yield takeEvery(TEST_TEAM_CODE_PENDING, callTestTeamCode);
}

function* postTeamFileSaga() {
  yield takeEvery(POST_TEAM_FILE_PENDING, callPostTeamFile);
}

function* postTeamRegistrationSaga() {
  yield takeEvery(POST_TEAM_REGISTRATION_REQUESTING, callPostTeamRegistration);
}

function* postJoinTeamSaga() {
  yield takeEvery(POST_JOIN_TEAM_REQUESTING, callPostJoinTeam);
}

function* requestJoinLinkSaga() {
  yield takeEvery(GET_JOIN_LINK_REQUESTING, callGetJoinLink);
}

function* confirmJoinTeamSaga() {
  yield takeEvery(CONFIRM_JOIN_REQUESTING, callConfirmJoinTeam);
}

// All sagas to be loaded
export default function* teamSaga() {
  yield all([
    testTeamNameSaga(),
    testTeamCodeSaga(),
    postTeamFileSaga(),
    // getPricesSaga(),
    postTeamRegistrationSaga(),
    postJoinTeamSaga(),
    requestJoinLinkSaga(),
    confirmJoinTeamSaga(),
  ]);
}
