/*
 * Teams Messages
 *
 * This contains all the text for the Teams component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  team_details_confirmation: {
    id: 'app.containers.Teams.team_details_confirmation',
    defaultMessage: 'Dados da Equipa',
  },
  confirm_delete: {
    id: 'app.containers.Teams.confirm_delete',
    defaultMessage: 'Tens a certeza de que pretendes apagar {name}?',
  },
  edit: {
    id: 'app.containers.Teams.edit',
    defaultMessage: 'Alterar',
  },
  save: {
    id: 'app.containers.Teams.save',
    defaultMessage: 'Guardar',
  },
  delete: {
    id: 'app.containers.Teams.delete',
    defaultMessage: 'Apagar',
  },
  cancel: {
    id: 'app.containers.Teams.cancel',
    defaultMessage: 'Voltar',
  },
  edit_team_member_title: {
    id: 'app.containers.Teams.edit_team_member_title',
    defaultMessage: 'Alterar dados',
  },
  has_the_runner_already_registered: {
    id: 'app.containers.Teams.has_the_runner_already_registered',
    defaultMessage: 'A inscrição do participante já foi realizada?',
  },
});
