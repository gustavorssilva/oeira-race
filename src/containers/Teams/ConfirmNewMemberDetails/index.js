import _ from 'lodash';
import React, { Component } from 'react';
import { func, number, object } from 'prop-types';
import DetailsConfirmationPage from 'components/DetailsConfirmationPage';
import {
  hasAllInvoiceInfo,
  hasNoInvoiceInfo,
} from 'containers/Registration/utils';
import { CREDIT_CARD, PAYMENT_METHOD } from 'containers/Registration/constants';
import {
  PAYMENT_CONFIRMATION_URL,
  REGISTER_NEW_TEAM_MEMBER_URL,
} from '../constants';

class ConfirmNewMemberDetails extends Component {
  state = {
    showDetails: null,
    errorMsg: '',
    selectedPayment: null,
  };

  componentWillMount() {
    const { resetJoinTeamState, pushToStart } = this.props;
    resetJoinTeamState();
    if (!this.shouldRender()) pushToStart();
  }

  componentWillReceiveProps(nextProps) {
    this.checkForPaymentResponse(nextProps);
    this.checkIfFree(nextProps);
  }

  getRunner() {
    const {
      currentRunnerIndex: i,
      runnerState: { data },
    } = this.props;
    return data[i];
  }

  setErrorMsg(errorMsg) {
    this.setState({ errorMsg });
  }

  shouldRender() {
    try {
      return !!this.getRunner();
    } catch (e) {
      return false;
    }
  }

  checkForPaymentResponse(props) {
    const {
      history: { push },
      resetJoinTeamState,
      joinTeamPostState: { data, error },
    } = props;

    if (!data && !error) return;

    if (error) {
      resetJoinTeamState();
      this.setErrorMsg(error);
      return;
    }

    const { payment_method: method, creditcard_link: link } = data;

    if (method === CREDIT_CARD && link) {
      window.location = link;
      return;
    }

    push(PAYMENT_CONFIRMATION_URL);
  }

  checkIfFree(props) {
    const { appliedPromoCode, updateRunnerDetails } = props;
    const runner = this.getRunner();

    if (!this.isOrderFree(appliedPromoCode)) return;
    if (!runner[PAYMENT_METHOD]) return;
    const newDetails = { ...runner, [PAYMENT_METHOD]: CREDIT_CARD };
    this.setState({ selectedPayment: 'visa' });
    updateRunnerDetails(newDetails);
  }

  isOrderFree(appliedPromoCode) {
    const dataToCheck = appliedPromoCode || this.props.appliedPromoCode;
    const totalPriceWithPromoCode = _.at(dataToCheck, 'data.total_price')[0];
    return totalPriceWithPromoCode === 0;
  }

  handleContinue = () => {
    const runner = this.getRunner();
    const {
      formatMsg,
      postJoinTeam,
      testTeamCodeState: {
        success: { external_id: teamID },
      },
    } = this.props;

    if (!hasAllInvoiceInfo(runner) && !hasNoInvoiceInfo(runner)) {
      return this.setErrorMsg(formatMsg('check_invoice_fields'));
    }

    return postJoinTeam({ runner, teamID });
  };

  render() {
    if (!this.shouldRender()) return null;
    const { errorMsg } = this.state;
    const { formatMsg, updateRunnerDetails } = this.props;
    const runner = this.getRunner();

    return (
      <DetailsConfirmationPage
        title={formatMsg('details_confirmation')}
        previousPage={REGISTER_NEW_TEAM_MEMBER_URL}
        mainData={runner}
        allRunners={[runner]}
        handleContinue={this.handleContinue}
        updateMainData={updateRunnerDetails}
        errorMsg={errorMsg}
        dismissError={() => this.setErrorMsg('')}
      />
    );
  }
}

ConfirmNewMemberDetails.propTypes = {
  currentRunnerIndex: number.isRequired,
  formatMsg: func.isRequired,
  postJoinTeam: func.isRequired,
  pushToStart: func.isRequired,
  appliedPromoCode: object.isRequired,
  updateRunnerDetails: func.isRequired,
  runnerState: object.isRequired,
  testTeamCodeState: object.isRequired,
  resetJoinTeamState: func.isRequired,
};

export default ConfirmNewMemberDetails;
