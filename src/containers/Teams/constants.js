/*
 *
 * Teams constants
 *
 */
const base = 'app/Teams/';
export const SET_TEAM_NAME = `${base}SET_TEAM_NAME`;

export const TEST_TEAM_NAME_PENDING = `${base}TEST_TEAM_NAME_PENDING`;
export const TEST_TEAM_NAME_REJECTED = `${base}TEST_TEAM_NAME_REJECTED`;
export const TEST_TEAM_NAME_FULFILLED = `${base}TEST_TEAM_NAME_FULFILLED`;
export const RESET_TEST_TEAM_NAME_STATE = `${base}RESET_TEST_TEAM_NAME_STATE`;

export const SET_LEADER = `${base}SET_LEADER`;
export const SET_TEAM_FILE = `${base}SET_TEAM_FILE`;

export const RESET_TEAM_FILE_STATE = `${base}RESET_TEAM_FILE_STATE`;
export const POST_TEAM_FILE_PENDING = `${base}POST_TEAM_FILE_PENDING`;
export const POST_TEAM_FILE_REJECTED = `${base}POST_TEAM_FILE_REJECTED`;
export const POST_TEAM_FILE_FULFILLED = `${base}POST_TEAM_FILE_FULFILLED`;
export const SET_TEAM_CODE = `${base}SET_TEAM_CODE`;

export const TEST_TEAM_CODE_PENDING = `${base}TEST_TEAM_CODE_PENDING`;
export const TEST_TEAM_CODE_REJECTED = `${base}TEST_TEAM_CODE_REJECTED`;
export const TEST_TEAM_CODE_FULFILLED = `${base}TEST_TEAM_CODE_FULFILLED`;
export const RESET_TEST_TEAM_CODE_STATE = `${base}RESET_TEST_TEAM_CODE_STATE`;

export const POST_TEAM_REGISTRATION_REQUESTING = `${base}TEAM_REG_REQUESTING`;
export const POST_TEAM_REGISTRATION_FAILED = `${base}TEAM_REG_FAILED`;
export const POST_TEAM_REGISTRATION_SUCCESSFUL = `${base}TEAM_REG_SUCCESSFUL`;
export const RESET_TEAM_REGISTRATION_STATE = `${base}RESET_TEAM_REG_STATE`;

export const POST_JOIN_TEAM_REQUESTING = `${base}JOIN_TEAM_REQUESTING`;
export const POST_JOIN_TEAM_FAILED = `${base}JOIN_TEAM_FAILED`;
export const POST_JOIN_TEAM_SUCCESSFUL = `${base}JOIN_TEAM_SUCCESSFUL`;
export const RESET_JOIN_TEAM_STATE = `${base}RESET_JOIN_TEAM_STATE`;

export const RESET_JOIN_TYPE_STATE = `${base}RESET_JOIN_TYPE_STATE`;
export const SET_JOIN_TYPE = `${base}SET_JOIN_TYPE`;

export const GET_JOIN_LINK_REQUESTING = `${base}GET_JOIN_LINK_REQUESTING`;
export const GET_JOIN_LINK_FAILED = `${base}GET_JOIN_LINK_FAILED`;
export const GET_JOIN_LINK_SUCCESSFUL = `${base}GET_JOIN_LINK_SUCCESSFUL`;
export const RESET_LINK_REQUEST_STATE = `${base}RESET_LINK_REQUEST_STATE`;

export const RESET_JOIN_REQUEST_STATE = `${base}RESET_JOIN_REQUEST_STATE`;
export const CONFIRM_JOIN_REQUESTING = `${base}CONFIRM_JOIN_REQUESTING`;
export const CONFIRM_JOIN_FAILED = `${base}CONFIRM_JOIN_FAILED`;
export const CONFIRM_JOIN_SUCCESSFUL = `${base}CONFIRM_JOIN_SUCCESSFUL`;

export const SET_EVIDENCE_FILES = `${base}SET_EVIDENCE_FILES`;
export const RESET_EVIDENCE_FILES = `${base}RESET_EVIDENCE_FILES`;

// Used for editing the data received from the parse request
export const SET_TEAM_DATA = `${base}SET_TEAM_DATA`;
export const SET_TEAM_MEMBER_INDEX = `${base}SET_TEAM_MEMBER_INDEX`;
export const SET_TEAM_MEMBER_DETAILS = `${base}SET_TEAM_MEMBER_DETAILS`;

// ========================= Page URLs =========================
export const teamURL = '/equipa/';
export const DOWNLOAD_EXCEL_URL = `${teamURL}descarregar-excel`;
export const JOIN_TEAM_START_URL = `${teamURL}associar-inicio`;
export const ENTER_TEAM_CODE_URL = `${teamURL}codigo-da-equipa`;
export const REQUEST_JOIN_LINK_URL = `${teamURL}procurar-inscricao`;
export const CONFIRM_TEAM_URL = `${teamURL}confirmar-equipa`;
export const REGISTER_NEW_TEAM_MEMBER_URL = `${teamURL}associar`;
export const CONFIRM_NEW_MEMBER_DETAILS_URL = `${teamURL}confirmar-dados-novos`;
export const LEADER_INFO_URL = `${teamURL}responsavel`;
export const CREATE_TEAM_URL = `${teamURL}criar`;
export const UPLOAD_EXCEL_URL = `${teamURL}enviar-excel`;
export const CONFIRM_DETAILS_URL = `${teamURL}confirmar-dados`;
export const EDIT_TEAM_MEMBER_URL = `${teamURL}alterar-dados`;
export const PAYMENT_CONFIRMATION_URL = `${teamURL}confirmacao`;

window.x = 'MhlJ-KDHp';

// ========================= Data =========================
export const JOIN_TYPE_NEW = 'JOIN_TYPE_NEW';
export const JOIN_TYPE_EXISTING = 'JOIN_TYPE_EXISTING';
