import _ from 'lodash';
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import OptIns from 'components/OptIns';
import regMessages from 'containers/Registration/messages';
import {
  BoldCaps,
  FullWidthDiv,
  OneColumnGrid,
  TeamTitle,
  SidePadded,
  MarginTop,
} from 'components/styled';
import {
  BackButton,
  BigButton,
  FormField,
  HelpIcon,
  InvoiceForm,
  Modal,
  RadioSelect,
} from 'components';
import { scrollToTop } from 'utils/functions';
import {
  EMAIL_CONFIRMATION,
  MANAGER_NAME,
  MANAGER_EMAIL,
  MANAGER_PHONE,
  KIT_DELIVERY,
  OPTIONS_BY_FIELD,
} from 'containers/Registration/constants';
import { getTeam } from 'utils/sessions';
import { CREATE_TEAM_URL, UPLOAD_EXCEL_URL } from '../constants';
import { validateLeaderDetails } from '../utils';

class Leader extends Component {
  state = { showModal: false, errorMsg: '' };

  componentWillMount() {
    scrollToTop();
    const teamName = getTeam();
    if (!teamName) this.props.pushToStart();
  }

  setErrorMsg = (errorMsg) => this.setState({ errorMsg });

  handleContinue() {
    const { history, leader, formatMsg } = this.props;
    const errors = validateLeaderDetails(leader);

    if (errors) {
      const errorMsg = _.map(_.castArray(errors), formatMsg).join('\n');
      return this.setErrorMsg(errorMsg);
    }

    return history.push(UPLOAD_EXCEL_URL);
  }

  handleChangeLeader(e, field) {
    const { leader, setLeader } = this.props;
    leader[field] = e.target.value;
    setLeader(leader);
  }

  handleOptInChange = (data) => {
    const { leader, setLeader } = this.props;
    setLeader({ ...leader, ...data });
  };

  handleRadioUpdate(value, field) {
    const data = { target: { value } };
    this.handleChangeLeader(data, field);
  }

  renderContinueButton() {
    const { formatMsg } = this.props;

    return (
      <BigButton onClick={() => this.handleContinue()}>
        {formatMsg('continue')}
      </BigButton>
    );
  }

  renderErrorModal() {
    const { errorMsg } = this.state;
    if (!errorMsg) return null;

    return <Modal onClick={() => this.setErrorMsg('')}>{errorMsg}</Modal>;
  }

  renderKitCollectionModal() {
    const { formatMsg } = this.props;
    const { showModal } = this.state;
    if (!showModal) return null;

    return (
      <Modal onClick={() => this.setState({ showModal: false })}>
        <OneColumnGrid>
          <div>{formatMsg('two_ways_to_collect_kits')}</div>
          <div>
            <BoldCaps>{formatMsg('as_a_group')}</BoldCaps>
            <div>{formatMsg('as_a_group_explanation')}</div>
          </div>
          <div>
            <BoldCaps>{formatMsg('individual_collection')}</BoldCaps>
            <div>{formatMsg('individual_collection_explanation')}</div>
          </div>
        </OneColumnGrid>
      </Modal>
    );
  }

  renderRadioSelect(field) {
    const { formatMsg } = this.props;
    const { leader } = this.props;
    const options = OPTIONS_BY_FIELD[field].map((value) => {
      const text = regMessages[value] ? formatMsg(value) : value;
      return { text, value };
    });

    return (
      <RadioSelect
        title={formatMsg(field)}
        options={options}
        selected={leader[field]}
        handleClick={(option) => this.handleRadioUpdate(option, field)}
      >
        <HelpIcon onClick={() => this.setState({ showModal: true })} />
      </RadioSelect>
    );
  }

  render() {
    const {
      leader,
      formatHTMLMsg,
      formatMsg,
      history: { push },
      pushToStart,
      setLeader,
    } = this.props;
    const teamName = getTeam();

    if (!leader || !teamName) {
      pushToStart();
      return null;
    }

    return (
      <FullWidthDiv>
        <BackButton onClick={() => push(CREATE_TEAM_URL)} />

        <TeamTitle>{teamName}</TeamTitle>
        <h1 style={{ margin: 0 }}>{formatMsg('team_leader')}</h1>
        <OneColumnGrid style={{ width: '100%' }}>
          <FormField
            placeholder={formatMsg('name')}
            value={leader[MANAGER_NAME] || ''}
            onChange={(e) => this.handleChangeLeader(e, MANAGER_NAME)}
            type="text"
          />

          <FormField
            placeholder={formatMsg('email')}
            value={leader[MANAGER_EMAIL] || ''}
            onChange={(e) => this.handleChangeLeader(e, MANAGER_EMAIL)}
            type="email"
          />

          <FormField
            placeholder={formatMsg(EMAIL_CONFIRMATION)}
            value={leader[EMAIL_CONFIRMATION] || ''}
            onChange={(e) => this.handleChangeLeader(e, EMAIL_CONFIRMATION)}
            type="email"
          />

          <FormField
            placeholder={formatMsg('phone_number')}
            value={leader[MANAGER_PHONE] || ''}
            onChange={(e) => this.handleChangeLeader(e, MANAGER_PHONE)}
            type="text"
          />

          {this.renderRadioSelect(KIT_DELIVERY)}

          <MarginTop>
            <InvoiceForm invoicee={leader} handleChange={setLeader} />
          </MarginTop>
        </OneColumnGrid>

        <OptIns
          showTermsAndPrivacyOptIns
          registrationObject={leader}
          updateRegistration={this.handleOptInChange}
          formatMsg={formatMsg}
          formatHTMLMsg={formatHTMLMsg}
        />

        <OneColumnGrid>
          <SidePadded>
            {formatMsg('confirm_others_accept_terms_and_privacy_policy')}
          </SidePadded>
        </OneColumnGrid>

        {this.renderContinueButton()}

        {this.renderKitCollectionModal()}
        {this.renderErrorModal()}
      </FullWidthDiv>
    );
  }
}

Leader.propTypes = {
  history: PropTypes.object.isRequired,
  leader: PropTypes.object.isRequired,
  formatHTMLMsg: PropTypes.func.isRequired,
  formatMsg: PropTypes.func.isRequired,
  pushToStart: PropTypes.func.isRequired,
  setLeader: PropTypes.func.isRequired,
};

export default Leader;
