import {
  SET_TEAM_NAME,
  TEST_TEAM_NAME_PENDING,
  TEST_TEAM_NAME_REJECTED,
  TEST_TEAM_NAME_FULFILLED,
  SET_LEADER,
  SET_TEAM_FILE,
  POST_TEAM_FILE_PENDING,
  POST_TEAM_FILE_REJECTED,
  POST_TEAM_FILE_FULFILLED,
  SET_TEAM_CODE,
  TEST_TEAM_CODE_PENDING,
  TEST_TEAM_CODE_REJECTED,
  TEST_TEAM_CODE_FULFILLED,
  POST_TEAM_REGISTRATION_REQUESTING,
  POST_TEAM_REGISTRATION_FAILED,
  POST_TEAM_REGISTRATION_SUCCESSFUL,
  RESET_TEAM_REGISTRATION_STATE,
  RESET_EVIDENCE_FILES,
  SET_EVIDENCE_FILES,
  RESET_TEAM_FILE_STATE,
  SET_TEAM_DATA,
  SET_TEAM_MEMBER_INDEX,
  SET_TEAM_MEMBER_DETAILS,
  POST_JOIN_TEAM_REQUESTING,
  POST_JOIN_TEAM_FAILED,
  POST_JOIN_TEAM_SUCCESSFUL,
  RESET_JOIN_TEAM_STATE,
  RESET_TEST_TEAM_CODE_STATE,
  RESET_TEST_TEAM_NAME_STATE,
  RESET_JOIN_TYPE_STATE,
  SET_JOIN_TYPE,
  RESET_LINK_REQUEST_STATE,
  GET_JOIN_LINK_REQUESTING,
  GET_JOIN_LINK_FAILED,
  GET_JOIN_LINK_SUCCESSFUL,
  RESET_JOIN_REQUEST_STATE,
  CONFIRM_JOIN_REQUESTING,
  CONFIRM_JOIN_FAILED,
  CONFIRM_JOIN_SUCCESSFUL,
} from './constants';

// ~~~~~~~~~~ TEAM NAME ~~~~~~~~~~
export const setTeamName = (teamName) => ({
  type: SET_TEAM_NAME,
  payload: teamName,
});
export const resetTestTeamNameState = () => ({
  type: RESET_TEST_TEAM_NAME_STATE,
});
export const testTeamName = (teamName) => ({
  type: TEST_TEAM_NAME_PENDING,
  payload: teamName,
});
export const testTeamNameRejected = (error) => ({
  type: TEST_TEAM_NAME_REJECTED,
  payload: error,
});
export const testTeamNameFulfilled = (success) => ({
  type: TEST_TEAM_NAME_FULFILLED,
  payload: success,
});

// ~~~~~~~~~~ LEADER ~~~~~~~~~~
export const setLeader = (leader) => ({ type: SET_LEADER, payload: leader });

// ~~~~~~~~~~ TEAM FILE ~~~~~~~~~~
export const setTeamFile = (file) => ({ type: SET_TEAM_FILE, payload: file });
export const resetTeamFileState = () => ({ type: RESET_TEAM_FILE_STATE });
export const postTeamFile = (file) => ({
  type: POST_TEAM_FILE_PENDING,
  payload: file,
});
export const postTeamFileRejected = (file) => ({
  type: POST_TEAM_FILE_REJECTED,
  payload: file,
});

export const postTeamFileFulfilled = (response) => ({
  type: POST_TEAM_FILE_FULFILLED,
  payload: response,
});

// ~~~~~~~~~~ SET TEAM CODE ~~~~~~~~~~
export const setTeamCode = (teamCode) => ({
  type: SET_TEAM_CODE,
  payload: teamCode,
});

// ~~~~~~~~~~ TEST TEAM CODE ~~~~~~~~~~
export const resetTestTeamCodeState = () => ({
  type: RESET_TEST_TEAM_CODE_STATE,
});
export const testTeamCode = (teamCode) => ({
  type: TEST_TEAM_CODE_PENDING,
  payload: teamCode,
});
export const testTeamCodeRejected = (error) => ({
  type: TEST_TEAM_CODE_REJECTED,
  payload: error,
});
export const testTeamCodeFulfilled = (success) => ({
  type: TEST_TEAM_CODE_FULFILLED,
  payload: success,
});

// ~~~~~~~~~~ POST TEAM REGISTRATION ~~~~~~~~~~
export const resetTeamRegistrationState = () => ({
  type: RESET_TEAM_REGISTRATION_STATE,
});
export const postTeamRegistration = (payload) => ({
  type: POST_TEAM_REGISTRATION_REQUESTING,
  payload,
});
export const postTeamRegistrationFailed = (payload) => ({
  type: POST_TEAM_REGISTRATION_FAILED,
  payload,
});
export const postTeamRegistrationSuccessful = (payload) => ({
  type: POST_TEAM_REGISTRATION_SUCCESSFUL,
  payload,
});

// ~~~~~~~~~~ POST JOIN TEAM REGISTRATION ~~~~~~~~~~
export const resetJoinTeamState = () => ({
  type: RESET_JOIN_TEAM_STATE,
});
export const postJoinTeam = (payload) => ({
  type: POST_JOIN_TEAM_REQUESTING,
  payload,
});
export const postJoinTeamFailed = (payload) => ({
  type: POST_JOIN_TEAM_FAILED,
  payload,
});
export const postJoinTeamSuccessful = (payload) => ({
  type: POST_JOIN_TEAM_SUCCESSFUL,
  payload,
});

// ~~~~~~~~~~ EVIDENCE FILES ~~~~~~~~~~
export const resetEvidenceFilesState = () => ({
  type: RESET_EVIDENCE_FILES,
});
export const setEvidenceFiles = (payload) => ({
  type: SET_EVIDENCE_FILES,
  payload,
});

// ~~~~~~~~~~ EDITING/DELETING TEAM MEMBERS ~~~~~~~~~~
export const setTeamData = (payload) => ({ type: SET_TEAM_DATA, payload });
// for tracking where to insert the team member later
export const setTeamMemberIndex = (payload) => ({
  type: SET_TEAM_MEMBER_INDEX,
  payload,
});
export const setTeamMemberDetails = (payload) => ({
  type: SET_TEAM_MEMBER_DETAILS,
  payload,
});

// ~~~~~~~~~~ JOIN TYPE (existing or new registration) ~~~~~~~~~~
export const resetJoinTypeState = () => ({ type: RESET_JOIN_TYPE_STATE });
export const setJoinType = (payload) => ({ type: SET_JOIN_TYPE, payload });

// ~~~~~~~~~~ REQUEST JOIN TEAM LINK ~~~~~~~~~~
export const resetLinkRequestState = () => ({ type: RESET_LINK_REQUEST_STATE });
export const requestJoinLink = (payload) => ({
  type: GET_JOIN_LINK_REQUESTING,
  payload,
});
export const requestJoinLinkFailed = (payload) => ({
  type: GET_JOIN_LINK_FAILED,
  payload,
});
export const requestJoinLinkSuccessful = (payload) => ({
  type: GET_JOIN_LINK_SUCCESSFUL,
  payload,
});

// ~~~~~~~~~~ CONFIRM JOIN TEAM REQUEST ~~~~~~~~~~
export const resetConfirmJoinRequestState = () => ({
  type: RESET_JOIN_REQUEST_STATE,
});
export const confirmJoinRequest = (payload) => ({
  type: CONFIRM_JOIN_REQUESTING,
  payload,
});
export const confirmJoinRequestFailed = (payload) => ({
  type: CONFIRM_JOIN_FAILED,
  payload,
});
export const confirmJoinRequestSuccessful = (payload) => ({
  type: CONFIRM_JOIN_SUCCESSFUL,
  payload,
});
