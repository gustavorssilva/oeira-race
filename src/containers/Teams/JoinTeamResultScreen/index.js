import React, { Component } from 'react';
import { func, object } from 'prop-types';
import { connect } from 'react-redux';
import { injectIntl, intlShape, FormattedHTMLMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { bindActionCreators, compose } from 'redux';

import { scrollToTop } from 'utils/functions';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';

import { LoadingScreen, BigButton } from 'components';
import {
  BoldCaps,
  FullWidthDiv,
  TeamTitle,
  TwoColTableGrid,
  TextCenter,
} from 'components/styled';

import registrationMessages from 'containers/Registration/messages';
import * as actions from '../actions';
import messages from '../messages';
import reducer from '../reducer';
import saga from '../saga';
import { selectConfirmJoinRequest } from '../selectors';

class JoinTeamResultScreen extends Component {
  componentWillMount() {
    const { confirmJoinRequest, match } = this.props;
    const { id } = match.params;
    confirmJoinRequest(id);
    scrollToTop();
  }

  formatHTMLMsg = (id, values = {}) => {
    const msg = messages[id] || registrationMessages[id];

    if (!msg) {
      console.error('[formatHTMLMsg] Message not found:', id);
      return id;
    }

    return <FormattedHTMLMessage {...msg} values={values} />;
  };

  formatMsg = (id, values = {}) => {
    const { formatMessage } = this.props.intl;
    const msg = messages[id] || registrationMessages[id];

    if (!msg) {
      console.error('[formatMsg] Message not found:', id);
      return id;
    }

    return formatMessage(msg, values);
  };

  pushToStart = () => this.props.history.push('/');

  renderError() {
    return (
      <TextCenter style={{ marginBottom: 30 }}>
        {this.formatMsg('runner_not_found')}
      </TextCenter>
    );
  }

  renderSuccess() {
    const {
      enrollment: { name },
      team: {
        team_name: teamName,
        manager_name: managerName,
        kit_delivery: kitCollectionMethod,
      },
    } = this.props.confirmJoinState.data;

    return (
      <FullWidthDiv>
        <BoldCaps>{this.formatMsg('congratulations', { name })}</BoldCaps>
        <TextCenter>
          {this.formatMsg('team_joined_by_you', {
            team: teamName,
          })}
        </TextCenter>

        <TeamTitle style={{ marginBottom: 0, marginTop: 30 }}>
          {teamName}
        </TeamTitle>
        <TwoColTableGrid style={{ marginBottom: 30 }}>
          <div>{this.formatMsg('team_leader')}:</div>
          <div>{managerName}</div>
          <div>{this.formatMsg('kit_collection_method_short')}:</div>
          <div>{kitCollectionMethod}</div>
        </TwoColTableGrid>
      </FullWidthDiv>
    );
  }

  render() {
    const {
      history: { push },
      confirmJoinState: { requesting, data },
    } = this.props;
    if (requesting) return <LoadingScreen />;
    const innerContent =
      data && data.enrollment ? this.renderSuccess() : this.renderError();

    return (
      <FullWidthDiv>
        <h1>{this.formatMsg('add_to_team_registration_title')}</h1>

        {innerContent}

        <BigButton onClick={() => push('/')}>
          {this.formatMsg('go_back')}
        </BigButton>
      </FullWidthDiv>
    );
  }
}

JoinTeamResultScreen.propTypes = {
  intl: intlShape.isRequired,
  history: object,
  match: object,
  confirmJoinState: object,
  confirmJoinRequest: func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  confirmJoinState: selectConfirmJoinRequest(),
});

const mapDispatchToProps = (dispatch) => bindActionCreators(actions, dispatch);

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withReducer = injectReducer({ key: 'teams', reducer });
const withSaga = injectSaga({ key: 'teams', saga });

export default compose(
  withReducer,
  withSaga,
  withConnect
)(injectIntl(JoinTeamResultScreen));
