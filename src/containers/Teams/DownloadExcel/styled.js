import styled from 'styled-components';

export const MarginTopTitle = styled.h4`
  margin: 1rem auto 0 auto;
`;
