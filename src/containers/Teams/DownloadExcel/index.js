import _ from 'lodash';
import React from 'react';
import { func, object } from 'prop-types';
import { FileDownloadIcon } from 'mdi-react';
import {
  CenteredBoldText,
  DownloadButton,
  FullWidthDiv,
  SubheadingDiv,
  TwoColTableGrid,
} from 'components/styled';
import { BackButton, BigButton, Modal } from 'components';
import { scrollToTop } from 'utils/functions';
import { MarginTopTitle } from './styled';
import { CREATE_TEAM_URL } from '../constants';

class DownloadExcel extends React.Component {
  state = { showModal: false, modalContent: '' };

  componentWillMount() {
    scrollToTop();
  }

  renderHelpModal() {
    const { showModal, modalContent } = this.state;
    if (!showModal) return null;

    return (
      <Modal onClick={() => this.setState({ showModal: false })}>
        {modalContent}
      </Modal>
    );
  }

  renderContinueButton() {
    const {
      formatMsg,
      history: { push },
    } = this.props;

    return (
      <BigButton onClick={() => push(CREATE_TEAM_URL)}>
        {formatMsg('continue')}
      </BigButton>
    );
  }

  renderContinueInstructions() {
    const { formatMsg } = this.props;

    return (
      <CenteredBoldText style={{ fontSize: '18px', marginBottom: '30px' }}>
        {formatMsg('continue_when_ready')}
      </CenteredBoldText>
    );
  }

  renderAvailableShirtSizes() {
    const { formatMsg, shirtSizesState } = this.props;

    return (
      <CenteredBoldText style={{ marginBottom: '30px' }}>
        <SubheadingDiv>
          {formatMsg('currently_available_shirt_sizes')}
        </SubheadingDiv>
        {[`chip`, `nochip`].map((type) => {
          const sizes = _.filter(shirtSizesState.data, { type });
          const mensSizes = _.find(sizes, { gender: 'M' }).sizes.join(', ');
          const womensSizes = _.find(sizes, { gender: 'F' }).sizes.join(', ');

          return (
            <React.Fragment key={type}>
              <MarginTopTitle>{formatMsg(type)}</MarginTopTitle>
              <TwoColTableGrid className="bg-white">
                <div>{formatMsg('M')}:</div>
                <div>{mensSizes}</div>
                <div>{formatMsg('F')}:</div>
                <div>{womensSizes}</div>
              </TwoColTableGrid>
            </React.Fragment>
          );
        })}
      </CenteredBoldText>
    );
  }

  render() {
    const { formatMsg, pushToStart } = this.props;

    return (
      <FullWidthDiv>
        <BackButton onClick={pushToStart} />

        <h1>{formatMsg('team_registration_title')}</h1>
        <CenteredBoldText style={{ fontSize: '18px' }}>
          {formatMsg('download_file_help_info')}
        </CenteredBoldText>

        <DownloadButton
          href="https://s3-eu-west-1.amazonaws.com/cdn.corridadotejo.knit.pt/man2019/assets/man2019.xlsx"
          download="nome_da_equipa.xlsx"
        >
          <div>
            <FileDownloadIcon size={34} />
          </div>
          <span>{formatMsg('download_this_file')}</span>
        </DownloadButton>

        {this.renderAvailableShirtSizes()}

        {this.renderContinueInstructions()}
        {this.renderContinueButton()}
        {this.renderHelpModal()}
      </FullWidthDiv>
    );
  }
}

DownloadExcel.propTypes = {
  formatMsg: func.isRequired,
  history: object.isRequired,
  pushToStart: func.isRequired,
  shirtSizesState: object.isRequired,
};

export default DownloadExcel;
