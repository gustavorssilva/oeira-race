import _ from 'lodash';
import React, { Component } from 'react';
import { func, number, object } from 'prop-types';
import { PreviewIcon } from 'assets/icons';
import { PORTUGAL, HIDE_DORSAL_PREVIEW_BUTTON } from 'utils/constants';
import { scrollToTop, validateStartBoxFile } from 'utils/functions';
import { saveTeam } from 'utils/sessions';
import {
  BackButton,
  BigButton,
  DatePicker,
  Dropdown,
  FormField,
  HelpIcon,
  Modal,
  RadioSelect,
  TickBox,
  SecondaryButton,
  DorsalPreview,
  UploadField,
} from 'components';
import {
  CenteredBoldText,
  FullWidthDiv,
  OneColumnGrid,
  TeamTitle,
  TwoColTableGrid,
} from 'components/styled';
import {
  ID_NUMBER,
  DOB,
  FULL_NAME,
  COUNTRY_OF_RESIDENCE,
  DISTRICT,
  EMAIL,
  EMAIL_CONFIRMATION,
  PHONE,
  GENDER,
  SHIRT_SIZE,
  DORSAL_NAME,
  START_BOX,
  OPTIONS_BY_FIELD,
  MODAL_MSGS,
  DROPDOWN_OPTIONS_BY_FIELD,
  EVIDENCE_UPLOAD,
  START_BOX_FAST_RUNNERS,
} from 'containers/Registration/constants';
import messages from 'containers/Registration/messages';
import {
  applyRequiredChanges,
  validateRunnerDetails,
} from 'containers/Registration/utils';
import OptIns from 'components/OptIns';
import { CONFIRM_NEW_MEMBER_DETAILS_URL, CONFIRM_TEAM_URL } from '../constants';

class NewMemberForm extends Component {
  state = {
    modalContent: '',
    showDorsalPreview: false,
    timeout: () => null,
    hasDuplicateID: false,
  };

  componentWillMount() {
    const {
      createRunnerDetails,
      pushToStart,
      setRunnerIndex,
      testTeamCodeState,
    } = this.props;
    if (!this.shouldRender()) return pushToStart();

    try {
      const { team_name: teamName } = testTeamCodeState.success;
      saveTeam(teamName);
    } catch (e) {
      console.warn('redirecting because:', e.toString());
      return pushToStart();
    }

    scrollToTop();
    setRunnerIndex(0);
    return createRunnerDetails();
  }

  componentWillReceiveProps(nextProps) {
    const {
      resetIDCheckState,
      idCheck: { error, data },
    } = nextProps;

    // on success, data equals false, so check to see
    // if it's null (or undefined, just in case)
    if (_.isNil(data) && !error) return;

    resetIDCheckState();
    this.setState({ hasDuplicateID: error || data, modalContent: error });
  }

  getRunner() {
    const {
      currentRunnerIndex: i,
      runnerState: { data },
    } = this.props;
    return _.isArray(data) && data[i];
  }

  getShirtSizes() {
    const runner = this.getRunner();
    const { data } = this.props.shirtSizesState;

    const sizeData = _.find(data, { type: runner.type, gender: runner.gender });
    return _.get(sizeData, 'sizes') || [];
  }

  setErrorMsg(error) {
    this.setState({ modalContent: error });
  }

  setTimeout(id) {
    clearTimeout(this.state.timeout);
    const { getIDCheck } = this.props;
    if (!id) return;
    this.setState({ timeout: setTimeout(() => getIDCheck(id), 1000) });
  }

  shouldRender() {
    const { testTeamCodeState } = this.props;
    return !!testTeamCodeState.success && !!this.getRunner();
  }

  applyChange = (data) => {
    const runner = this.getRunner();
    const { updateRunnerDetails } = this.props;
    applyRequiredChanges(runner, data);
    updateRunnerDetails({ ...runner, ...data });
  };

  handleChange(field, value) {
    if (field === ID_NUMBER) {
      this.setTimeout(value);
    }
    this.applyChange({ [field]: value });
  }

  handleImageChange(e, field) {
    const file = e.target.files[0];

    const error = validateStartBoxFile(file);
    if (error) return this.setErrorMsg(error);

    return this.applyChange({ [field]: file });
  }

  handleContinue = () => {
    const {
      // getPrices,
      history: { push },
    } = this.props;
    // getPrices();
    push(CONFIRM_NEW_MEMBER_DETAILS_URL);
  };

  findErrors = () => {
    const { formatMsg } = this.props;
    const runner = this.getRunner();
    const shirtSizes = this.getShirtSizes();
    const errors = validateRunnerDetails(runner, shirtSizes);
    if (!errors) return null;
    return _.map(_.castArray(errors), formatMsg).join('\n');
  };

  renderContinueButton() {
    const {
      formatMsg,
      history: { push },
    } = this.props;
    const errors = this.findErrors() || this.state.hasDuplicateID;

    const onClick = errors
      ? () => this.setState({ modalContent: errors })
      : () => push(CONFIRM_NEW_MEMBER_DETAILS_URL);

    return <BigButton onClick={onClick}>{formatMsg('continue')}</BigButton>;
  }

  renderHelpIcon(field) {
    const modalContent = this.props.formatMsg(MODAL_MSGS[field]);

    return <HelpIcon onClick={() => this.setState({ modalContent })} />;
  }

  renderHelpModal() {
    const { modalContent } = this.state;
    if (!modalContent) return null;

    return (
      <Modal onClick={() => this.setState({ modalContent: null })}>
        {modalContent}
      </Modal>
    );
  }

  renderDorsalPreview() {
    const runner = this.getRunner();
    const { showDorsalPreview } = this.state;
    if (!showDorsalPreview) return null;
    return (
      <DorsalPreview
        runner={runner}
        handleClose={() => this.setState({ showDorsalPreview: false })}
      />
    );
  }

  renderDatePicker(field) {
    const runner = this.getRunner();
    const { formatMsg } = this.props;

    return (
      <DatePicker
        placeholder={formatMsg(`short_${field}`)}
        date={runner[field]}
        onChange={(value) => this.handleChange(field, value)}
      />
    );
  }

  renderDropdown(field) {
    const runner = this.getRunner();
    const { formatMsg } = this.props;

    if (field === DISTRICT && runner[COUNTRY_OF_RESIDENCE] !== PORTUGAL) {
      return null;
    }

    const options = DROPDOWN_OPTIONS_BY_FIELD[field];

    return (
      <Dropdown
        value={runner[field]}
        placeholder={formatMsg(field)}
        onChange={(value) => this.handleChange(field, value)}
        options={options}
      />
    );
  }

  renderRadioSelect(field) {
    const runner = this.getRunner();
    const { formatMsg } = this.props;
    const availableShirts = this.getShirtSizes();
    const isShirt = SHIRT_SIZE === field;
    if (isShirt && !runner[GENDER]) return null;

    const options = _.map(OPTIONS_BY_FIELD[field], (value) => {
      const text = !isShirt && messages[value] ? formatMsg(value) : value;
      const isDisabled = isShirt && !_.includes(availableShirts, value);

      return { text, value, isDisabled };
    });

    const extraInfo =
      field === START_BOX ? formatMsg(`start_box_explanation`) : null;

    return (
      <RadioSelect
        title={formatMsg(field)}
        options={options}
        selected={runner[field]}
        handleClick={(option) => this.handleChange(field, option)}
        extraInfo={extraInfo}
      />
    );
  }

  renderTickBox(field) {
    const runner = this.getRunner();
    const { formatMsg } = this.props;
    const value = !!runner[field];

    return (
      <TickBox
        text={formatMsg(field)}
        value={value}
        onClick={() => this.handleChange(field, !value)}
      />
    );
  }

  renderUploadField(field) {
    const runner = this.getRunner();
    const { formatMsg } = this.props;
    if (!runner[START_BOX] || runner[START_BOX] !== START_BOX_FAST_RUNNERS) {
      return null;
    }

    return (
      <UploadField
        field={field}
        text={formatMsg(field)}
        file={runner[field]}
        onChange={(e) => this.handleImageChange(e, field)}
        fileTypes={['.jpg', '.jpeg', '.pdf']}
      />
    );
  }

  renderField(field, type) {
    const runner = this.getRunner();
    const { formatMsg } = this.props;

    return (
      <FormField
        field={field}
        placeholder={formatMsg(field)}
        value={runner[field]}
        onChange={(e) => this.handleChange(field, e.target.value)}
        type={type || 'text'}
      />
    );
  }

  renderDorsalPreviewButton() {
    if (HIDE_DORSAL_PREVIEW_BUTTON) return null;

    const { formatMsg } = this.props;

    return (
      <SecondaryButton
        onClick={() => this.setState({ showDorsalPreview: true })}
        icon={<img src={PreviewIcon} alt="icon_preview" />}
      >
        {formatMsg('preview_dorsal')}
      </SecondaryButton>
    );
  }

  render() {
    if (!this.shouldRender()) return null;

    const {
      formatHTMLMsg,
      formatMsg,
      history: { push },
      testTeamCodeState: { success: data },
    } = this.props;
    const runner = this.getRunner();

    if (!data || !runner) return null;
    const {
      kit_delivery: kitCollectionMethod,
      manager_name: managerName,
      team_name: teamName,
    } = data;

    return (
      <FullWidthDiv>
        <BackButton onClick={() => push(CONFIRM_TEAM_URL)} />

        <TeamTitle style={{ margin: 0 }}>{teamName}</TeamTitle>
        <TwoColTableGrid style={{ marginBottom: 30 }}>
          <div>{formatMsg('team_leader')}:</div>
          <div>{managerName}</div>
          <div>{formatMsg('kit_collection_method_short')}:</div>
          <div>{kitCollectionMethod}</div>
        </TwoColTableGrid>

        <h1>{formatMsg('add_to_team_registration_title')}</h1>
        <CenteredBoldText style={{ fontSize: '18px' }}>
          {formatMsg('fill_all_fields')}
        </CenteredBoldText>

        <OneColumnGrid>
          {this.renderField(ID_NUMBER)}
          {this.renderDatePicker(DOB)}
          {this.renderField(FULL_NAME)}
          {this.renderDropdown(COUNTRY_OF_RESIDENCE)}
          {this.renderDropdown(DISTRICT)}
          {this.renderField(EMAIL, 'email')}
          {this.renderField(EMAIL_CONFIRMATION, 'email')}
          {this.renderField(PHONE)}
          {this.renderRadioSelect(GENDER)}
          {this.renderRadioSelect(SHIRT_SIZE)}

          {/* Dorsal name and Preview dorsal */}
          {this.renderField(DORSAL_NAME)}
          {this.renderDorsalPreviewButton()}

          {this.renderRadioSelect(START_BOX)}
          {this.renderUploadField(EVIDENCE_UPLOAD)}
        </OneColumnGrid>

        <OptIns
          showTermsAndPrivacyOptIns
          registrationObject={runner}
          updateRegistration={this.applyChange}
          formatMsg={formatMsg}
          formatHTMLMsg={formatHTMLMsg}
        />

        {this.renderContinueButton()}
        {this.renderHelpModal()}
        {this.renderDorsalPreview()}
      </FullWidthDiv>
    );
  }
}

NewMemberForm.propTypes = {
  currentRunnerIndex: number.isRequired,
  formatHTMLMsg: func.isRequired,
  formatMsg: func.isRequired,
  getIDCheck: func.isRequired,
  // getPrices: func.isRequired,
  createRunnerDetails: func.isRequired,
  updateRunnerDetails: func.isRequired,
  resetIDCheckState: func.isRequired,
  setRunnerIndex: func.isRequired,
  history: object.isRequired,
  idCheck: object.isRequired,
  runnerState: object.isRequired,
  shirtSizesState: object.isRequired,
  testTeamCodeState: object.isRequired,
  pushToStart: func.isRequired,
};

export default NewMemberForm;
