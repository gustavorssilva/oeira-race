import React, { Component } from 'react';
import { func, object } from 'prop-types';
import { connect } from 'react-redux';
import { injectIntl, intlShape, FormattedHTMLMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import { bindActionCreators, compose } from 'redux';
import { Switch, Route } from 'react-router-dom';

import { scrollToTop } from 'utils/functions';
import injectSaga from 'utils/injectSaga';
import injectReducer from 'utils/injectReducer';

import { ErrorMsgHandler, LoadingScreen } from 'components';
import { FullWidthDiv } from 'components/styled';

import NotFoundPage from 'containers/NotFoundPage/Loadable';
import * as regActions from 'containers/Registration/actions';
import registrationMessages from 'containers/Registration/messages';
import regReducer from 'containers/Registration/reducer';
import regSaga from 'containers/Registration/saga';
import {
  selectIDCheckState,
  selectRunnerDetailsState,
  selectCurrentRunnerIndex,
  selectPromoCode,
  selectAppliedPromoCode,
  selectShirtsState,
} from 'containers/Registration/selectors';
import * as actions from './actions';
import DownloadExcel from './DownloadExcel';
import UploadExcel from './UploadExcel';
import CreateTeam from './CreateTeam';
import EditTeamMember from './EditTeamMember';
import ConfirmDetails from './ConfirmDetails';
import PaymentConfirmation from './PaymentConfirmation';
import Leader from './Leader';
import JoinTeamStart from './JoinTeamStart';
import EnterTeamCode from './EnterTeamCode';
import ConfirmTeam from './ConfirmTeam';
import ConfirmNewMemberDetails from './ConfirmNewMemberDetails';
import NewMemberForm from './NewMemberForm';
import RequestJoinLink from './RequestJoinLink';
import messages from './messages';
import saga from './saga';
import {
  selectTeamName,
  selectTestTeamState,
  selectLeader,
  selectTeamCode,
  selectTestTeamCodeState,
  selectTeamFilePostState,
  selectTeamFileState,
  selectTeamRegistrationState,
  selectEvidenceFilesState,
  selectTeamMember,
  selectTeamMemberIndex,
  selectJoinTeamPostState,
  selectJoinType,
  selectLinkRequest,
} from './selectors';
import {
  DOWNLOAD_EXCEL_URL,
  JOIN_TEAM_START_URL,
  CONFIRM_TEAM_URL,
  LEADER_INFO_URL,
  CREATE_TEAM_URL,
  UPLOAD_EXCEL_URL,
  CONFIRM_DETAILS_URL,
  EDIT_TEAM_MEMBER_URL,
  PAYMENT_CONFIRMATION_URL,
  REGISTER_NEW_TEAM_MEMBER_URL,
  CONFIRM_NEW_MEMBER_DETAILS_URL,
  ENTER_TEAM_CODE_URL,
  REQUEST_JOIN_LINK_URL,
} from './constants';

class Teams extends Component {
  componentWillMount() {
    const {
      // getPrices,
      getShirts,
      resetTeamFileState,
      resetTeamRegistrationState,
      resetJoinTeamState,
      resetPromoCodeState,
    } = this.props;

    scrollToTop();
    resetTeamFileState();
    resetTeamRegistrationState();
    resetJoinTeamState();
    resetPromoCodeState();
    // getPrices();
    getShirts();
  }

  formatHTMLMsg = (id, values = {}) => {
    const msg = messages[id] || registrationMessages[id];

    if (!msg) {
      console.error('[formatHTMLMsg] Message not found:', id);
      return id;
    }

    return <FormattedHTMLMessage {...msg} values={values} />;
  };

  formatMsg = (id, values = {}) => {
    const { formatMessage } = this.props.intl;
    const msg = messages[id] || registrationMessages[id];

    if (!msg) {
      console.error('[formatMsg] Message not found:', id);
      return id;
    }

    return formatMessage(msg, values);
  };

  pushToStart = () => this.props.history.push('/');

  renderErrors() {
    const {
      joinTeamPostState,
      teamRegistrationState: teamRegState,
      resetJoinTeamState,
      resetTeamRegistrationState,
    } = this.props;

    const stateCollection = [
      { error: joinTeamPostState.error, onClose: resetJoinTeamState },
      { error: teamRegState.error, onClose: resetTeamRegistrationState },
    ];

    return <ErrorMsgHandler stateCollection={stateCollection} />;
  }

  render() {
    const { shirtSizesState } = this.props;
    if (!shirtSizesState.data) return <LoadingScreen />;

    const props = {
      ...this.props,
      availableShirts: shirtSizesState.data,
      formatMsg: this.formatMsg,
      formatHTMLMsg: this.formatHTMLMsg,
      pushToStart: this.pushToStart,
    };

    return (
      <FullWidthDiv>
        <Switch>
          <Route
            exact
            path={CONFIRM_NEW_MEMBER_DETAILS_URL}
            render={() => <ConfirmNewMemberDetails {...props} />}
          />

          <Route
            exact
            path={REGISTER_NEW_TEAM_MEMBER_URL}
            render={() => <NewMemberForm {...props} />}
          />

          <Route
            exact
            path={CONFIRM_TEAM_URL}
            render={() => <ConfirmTeam {...props} />}
          />

          <Route
            exact
            path={JOIN_TEAM_START_URL}
            render={() => <JoinTeamStart {...props} />}
          />

          <Route
            exact
            path={ENTER_TEAM_CODE_URL}
            render={() => <EnterTeamCode {...props} />}
          />

          <Route
            exact
            path={REQUEST_JOIN_LINK_URL}
            render={() => <RequestJoinLink {...props} />}
          />

          <Route
            exact
            path={LEADER_INFO_URL}
            render={() => <Leader {...props} />}
          />

          <Route
            exact
            path={CREATE_TEAM_URL}
            render={() => <CreateTeam {...props} />}
          />

          <Route
            exact
            path={UPLOAD_EXCEL_URL}
            render={() => <UploadExcel {...props} />}
          />

          <Route
            exact
            path={CONFIRM_DETAILS_URL}
            render={() => <ConfirmDetails {...props} />}
          />

          <Route
            exact
            path={EDIT_TEAM_MEMBER_URL}
            render={() => <EditTeamMember {...props} />}
          />

          <Route
            exact
            path={PAYMENT_CONFIRMATION_URL}
            render={() => <PaymentConfirmation {...props} />}
          />

          <Route
            exact
            path={DOWNLOAD_EXCEL_URL}
            render={() => <DownloadExcel {...props} />}
          />

          <Route component={NotFoundPage} />
        </Switch>
        {this.renderErrors()}
      </FullWidthDiv>
    );
  }
}

Teams.propTypes = {
  intl: intlShape.isRequired,
  // getPrices: func.isRequired,
  getShirts: func.isRequired,
  history: object,
  shirtSizesState: object,
  teamRegistrationState: object.isRequired,
  joinTeamPostState: object.isRequired,
  resetTeamFileState: func.isRequired,
  resetTeamRegistrationState: func.isRequired,
  resetJoinTeamState: func.isRequired,
  resetPromoCodeState: func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  teamName: selectTeamName(),
  testTeamState: selectTestTeamState(),
  leader: selectLeader(),
  teamCode: selectTeamCode(),
  testTeamCodeState: selectTestTeamCodeState(),
  filePostState: selectTeamFilePostState(),
  teamFile: selectTeamFileState(),
  shirtSizesState: selectShirtsState(),
  teamRegistrationState: selectTeamRegistrationState(),
  evidenceFiles: selectEvidenceFilesState(),
  teamMember: selectTeamMember(),
  teamMemberIndex: selectTeamMemberIndex(),
  joinTeamPostState: selectJoinTeamPostState(),
  idCheck: selectIDCheckState(),
  runnerState: selectRunnerDetailsState(),
  currentRunnerIndex: selectCurrentRunnerIndex(),
  joinType: selectJoinType(),
  joinLinkRequest: selectLinkRequest(),
  promoCode: selectPromoCode(),
  appliedPromoCode: selectAppliedPromoCode(),
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({ ...regActions, ...actions }, dispatch);

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);

const withRegReducer = injectReducer({
  key: 'registration',
  reducer: regReducer,
});
const withSaga = injectSaga({ key: 'teams', saga });
const withRegSaga = injectSaga({ key: 'registration', saga: regSaga });

export default compose(
  withRegReducer,
  withSaga,
  withRegSaga,
  withConnect
)(injectIntl(Teams));
