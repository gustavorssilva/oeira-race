import _ from 'lodash';
import { BASE_URL, API_RACE_NAME, IS_LOCALHOST } from 'utils/constants';
import {
  EMAIL_CONFIRMATION,
  MANAGER_NAME,
  MANAGER_EMAIL,
  MANAGER_PHONE,
  KIT_DELIVERY,
  EDIT_TEAM_MEMBER_FORM_FIELDS,
  BILLING_NAME,
  BILLING_ADDRESS,
  BILLING_CITY,
  BILLING_POSTCODE,
  BILLING_TAX_NUMBER,
  SHIRT_SIZE,
  RACE,
  START_BOX,
  EVIDENCE_UPLOAD,
} from 'containers/Registration/constants';
import {
  hasValue,
  hasAllInvoiceInfo,
  hasNoInvoiceInfo,
  isTooYoung,
  hasValidEmail,
  hasValidPhoneNumber,
  emailAndEmailConfirmationMatch,
  areRequiredOptInsMissing,
  needsEvidence,
} from 'containers/Registration/utils';

function hasAllLeaderInfo(leader) {
  return _.every(
    [MANAGER_NAME, MANAGER_EMAIL, MANAGER_PHONE, KIT_DELIVERY],
    (field) => hasValue(leader[field])
  )
    ? null
    : 'missing_leader_info';
}

export function hasValidShirtSize(runner, shirtSizes) {
  return _.includes(shirtSizes, runner[SHIRT_SIZE])
    ? null
    : 'shirt_size_not_available';
}

function checkLeaderInvoiceInfo(leader) {
  return hasAllInvoiceInfo(leader) || hasNoInvoiceInfo(leader)
    ? null
    : 'check_invoice_fields';
}

function leaderEmailAndEmailConfirmationMatch(leader) {
  return IS_LOCALHOST || leader[MANAGER_EMAIL] === leader[EMAIL_CONFIRMATION]
    ? null
    : 'email_not_confirmed';
}

export function validateLeaderDetails(leader) {
  return _.find([
    hasAllLeaderInfo(leader),
    checkLeaderInvoiceInfo(leader),
    leaderEmailAndEmailConfirmationMatch(leader),
    areRequiredOptInsMissing(leader),
  ]);
}

const mockLeaderObject = {
  [RACE]: API_RACE_NAME,
  [MANAGER_NAME]: 'Test Manager',
  [MANAGER_EMAIL]: 'ciaran.edwards@knit.pt',
  [MANAGER_PHONE]: '9879879898789798',
  [KIT_DELIVERY]: 'individual',
  [BILLING_NAME]: 'TEST',
  [BILLING_ADDRESS]: 'TEST',
  [BILLING_CITY]: 'TEST',
  [BILLING_POSTCODE]: 'TEST',
  [BILLING_TAX_NUMBER]: 'TEST',
};

export const defaultLeaderObject = () =>
  IS_LOCALHOST
    ? { ...mockLeaderObject }
    : {
        [RACE]: API_RACE_NAME,
        [MANAGER_NAME]: null,
        [MANAGER_EMAIL]: null,
        [MANAGER_PHONE]: null,
        [KIT_DELIVERY]: null,
      };

// Edit team member validation
function isMissingInfo(runner) {
  const requiredFields = needsEvidence(runner)
    ? EDIT_TEAM_MEMBER_FORM_FIELDS
    : _.without(EDIT_TEAM_MEMBER_FORM_FIELDS, START_BOX, EVIDENCE_UPLOAD);
  const emptyFields = _.filter(requiredFields, (key) => !hasValue(runner[key]));
  if (emptyFields.length === 0) return null;
  return ['missing_the_following_fields', ...emptyFields];
}

export function validateTeamMemberDetails(runner, shirtSizes) {
  const errors = [
    isTooYoung(runner),
    isMissingInfo(runner),
    hasValidEmail(runner),
    emailAndEmailConfirmationMatch(runner),
    hasValidPhoneNumber(runner),
    hasValidShirtSize(runner, shirtSizes),
  ];

  return _.find(errors);
}

export const getJoinTeamURL = (teamId) =>
  `${BASE_URL}team/${teamId}/enrollments`;
