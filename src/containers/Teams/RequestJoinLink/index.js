import React, { Component } from 'react';
import { func, object, string } from 'prop-types';
import { getTeamCode } from 'utils/sessions';
import {
  BackButton,
  BigButton,
  DatePicker,
  FormField,
  Modal,
  CheckYourEmail,
} from 'components';
import { FullWidthDiv, OneColumnGrid } from 'components/styled';
import { DOB, ID_NUMBER } from 'containers/Registration/constants';
import { JOIN_TEAM_START_URL } from '../constants';

class RequestJoinLink extends Component {
  state = { modalContent: '', emailSent: false, idNumber: '', dateOfBirth: '' };

  componentWillMount() {
    const {
      resetLinkRequestState,
      history: { push },
    } = this.props;
    if (!this.shouldRender()) return push(JOIN_TEAM_START_URL);
    return resetLinkRequestState();
  }

  componentWillReceiveProps(props) {
    const {
      formatMsg,
      joinLinkRequest: { data, error },
      resetLinkRequestState,
    } = props;
    const { modalContent } = this.state;

    if (data) {
      this.setState({ emailSent: true, modalContent: '' });
      return;
    }

    if (!error || modalContent) return;
    resetLinkRequestState();
    this.setModal(formatMsg('runner_not_found_or_already_associated'));
  }

  setModal = (modalContent) => this.setState({ modalContent });

  shouldRender() {
    const { testTeamCodeState, joinType } = this.props;
    return !!testTeamCodeState.success && !!joinType && !!getTeamCode();
  }

  handleContinue = () => {
    const { requestJoinLink } = this.props;
    const { idNumber, dateOfBirth } = this.state;
    const teamId = getTeamCode();
    requestJoinLink({ idNumber, dateOfBirth, teamId });
  };

  renderError() {
    const { modalContent } = this.state;
    if (!modalContent) return null;
    return <Modal onClick={() => this.setModal('')}>{modalContent}</Modal>;
  }

  renderContinueButton() {
    const { formatMsg } = this.props;
    const { idNumber, dateOfBirth } = this.state;

    return (
      <BigButton
        disabled={!idNumber || !dateOfBirth}
        onClick={this.handleContinue}
      >
        {formatMsg('continue')}
      </BigButton>
    );
  }

  renderCheckYourEmail() {
    const { formatMsg } = this.props;

    return (
      <CheckYourEmail
        title={formatMsg('add_to_team_registration_title')}
        toDoWhat={formatMsg('to_join_the_team')}
      />
    );
  }

  render() {
    if (!this.shouldRender()) return null;
    const {
      formatMsg,
      history: { push },
    } = this.props;
    const { emailSent, idNumber, dateOfBirth } = this.state;

    if (emailSent) return this.renderCheckYourEmail();

    return (
      <FullWidthDiv>
        <BackButton onClick={() => push('/')} />

        <h1>{formatMsg('add_to_team_registration_title')}</h1>

        <OneColumnGrid>
          <FormField
            placeholder={formatMsg(ID_NUMBER)}
            value={idNumber || ''}
            onChange={(e) => this.setState({ idNumber: e.target.value })}
          />

          <DatePicker
            placeholder={formatMsg(`short_${DOB}`)}
            date={dateOfBirth || ''}
            onChange={(value) => this.setState({ dateOfBirth: value })}
          />
        </OneColumnGrid>

        {this.renderContinueButton()}
        {this.renderError()}
      </FullWidthDiv>
    );
  }
}

RequestJoinLink.propTypes = {
  history: object.isRequired,
  formatMsg: func.isRequired,
  joinLinkRequest: object.isRequired,
  joinType: string.isRequired,
  requestJoinLink: func.isRequired,
  resetLinkRequestState: func.isRequired,
  testTeamCodeState: object.isRequired,
};

export default RequestJoinLink;
