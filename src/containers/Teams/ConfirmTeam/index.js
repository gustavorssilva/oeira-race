import React from 'react';
import { func, object, string } from 'prop-types';
import {
  FullWidthDiv,
  TeamTitle,
  TwoColTableGrid,
  MarginTop,
} from 'components/styled';
import { BackButton, BigButton } from 'components';
import { scrollToTop } from 'utils/functions';
import { saveTeamCode } from 'utils/sessions';
import {
  JOIN_TYPE_EXISTING,
  JOIN_TEAM_START_URL,
  REGISTER_NEW_TEAM_MEMBER_URL,
  REQUEST_JOIN_LINK_URL,
} from '../constants';

class ConfirmTeam extends React.Component {
  componentWillMount() {
    scrollToTop();
    if (!this.shouldRender()) this.props.pushToStart();
  }

  shouldRender() {
    return !!this.props.testTeamCodeState.success;
  }

  handleContinue = () => {
    const {
      history: { push },
      joinType,
      teamCode,
    } = this.props;

    saveTeamCode(teamCode);
    const nextPage =
      joinType === JOIN_TYPE_EXISTING
        ? REQUEST_JOIN_LINK_URL
        : REGISTER_NEW_TEAM_MEMBER_URL;
    return push(nextPage);
  };

  renderContinueButton() {
    const { formatMsg, teamCode, testTeamCodeState } = this.props;

    // @dev if there is a name and there is not a team with that name in the DB
    const isDisabled = !teamCode || !testTeamCodeState.success;
    return (
      <MarginTop>
        <BigButton disabled={isDisabled} onClick={this.handleContinue}>
          {formatMsg('join_team')}
        </BigButton>
      </MarginTop>
    );
  }

  render() {
    if (!this.shouldRender()) return null;
    const {
      formatMsg,
      history: { push },
      testTeamCodeState: {
        success: {
          kit_delivery: kitCollectionMethod,
          manager_name: managerName,
          team_name: teamName,
        },
      },
    } = this.props;

    return (
      <FullWidthDiv>
        <BackButton onClick={() => push(JOIN_TEAM_START_URL)} />

        <TeamTitle style={{ margin: 0 }}>{teamName}</TeamTitle>
        <TwoColTableGrid>
          <div>{formatMsg('team_leader')}:</div>
          <div>{managerName}</div>
          <div>{formatMsg('kit_collection_method_short')}:</div>
          <div>{kitCollectionMethod}</div>
        </TwoColTableGrid>

        {this.renderContinueButton()}
      </FullWidthDiv>
    );
  }
}

ConfirmTeam.propTypes = {
  formatMsg: func.isRequired,
  pushToStart: func.isRequired,
  history: object.isRequired,
  joinType: string.isRequired,
  teamCode: string.isRequired,
  testTeamCodeState: object.isRequired,
};

export default ConfirmTeam;
