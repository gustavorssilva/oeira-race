import 'formdata-polyfill';
import _ from 'lodash';
import axios from 'axios';
import { all, call, put, takeEvery, takeLatest } from 'redux-saga/effects';
import { getFirstErrorMsg } from 'utils/getErrorMsg';
import {
  // getPricesFailed,
  // getPricesSuccessful,
  getShirtsFailed,
  getShirtsSuccessful,
  getIDCheckFailed,
  getIDCheckSuccessful,
  getPaymentStatusFailed,
  getPaymentStatusSuccessful,
  postRegistrationSuccessful,
  postRegistrationFailed,
  getRegistrationFailed,
  getRegistrationSuccess,
  postRegUpdateFailed,
  postRegUpdateSuccess,
  requestEditLinkFailed,
  requestEditLinkSuccess,
  applyPromoCodeSuccess,
  applyPromoCodeFailed,
} from './actions';
import {
  // URLs
  // GET_PRICES_URL,
  GET_SHIRTS_URL,
  GET_ID_CHECK_URL,
  INDIVIDUAL_REGISTRATION_URL,
  PAYMENT_STATUS_URL,
  GET_REGISTRATION_DETAILS_URL,
  REQUEST_REG_UPDATE_LINK_URL,
  GET_FINAL_PRICE_URL,
  // reducers
  // GET_PRICES_REQUESTING,
  GET_SHIRTS_REQUESTING,
  GET_ID_CHECK_REQUESTING,
  POST_REGISTRATION_REQUESTING,
  GET_PAYMENT_STATUS_REQUESTING,
  GET_EDIT_REG_LINK_REQUESTING,
  APPLY_PROMO_CODE_REQUESTING,
  GET_REG_DETAILS_REQUESTING,
  POST_UPDATE_REQUESTING,
  // fields
  EVIDENCE_UPLOAD,
  REQUIRED_DATA,
  INVOICE_FIELDS,
  PAYMENT_METHOD,
  ID_NUMBER,
  DOB,
  SHIRT_SIZE,
  GENDER,
} from './constants';
import { hasAllInvoiceInfo, needsEvidence } from './utils';

// Workers sagas
// const getPrices = () => axios.get(GET_PRICES_URL);
const getShirts = () => axios.get(GET_SHIRTS_URL);
const getPaymentStatus = (id) =>
  axios.get(`${PAYMENT_STATUS_URL}${encodeURIComponent(id)}`);

function getIDCheck(id) {
  const tidiedID = encodeURIComponent(id.trim());
  return axios.get(`${GET_ID_CHECK_URL}${tidiedID}`);
}

function postRegistration(details) {
  const body = new FormData();
  const runnerData = {};
  const firstRunner = details[0];
  const code = firstRunner.discount_code;

  if (code) runnerData.discount_code = code;

  runnerData.payment_method = firstRunner[PAYMENT_METHOD];
  runnerData.enrollments = _.map(details, (runner, i) => {
    const file = runner[EVIDENCE_UPLOAD];
    if (file && needsEvidence(runner)) {
      body.set(`starting_line_proof_${i}`, file);
    }
    return _.pick(runner, REQUIRED_DATA);
  });

  // HAMMER
  runnerData.enrollments.push({...runnerData.enrollments[0]});
  runnerData.enrollments[1].race = 'cdt2019';
  runnerData.enrollments[0].type = 'chip';
  runnerData.enrollments[0].start_box = 'Normal';
  runnerData.enrollments[1].type = 'normal';
  runnerData.enrollments[1].start_box = '>60';


  if (hasAllInvoiceInfo(firstRunner)) {
    const invoiceInfo = _.pick(firstRunner, INVOICE_FIELDS);
    _.assign(runnerData, invoiceInfo);
  }

  console.log(runnerData);

  body.set('payload', JSON.stringify(runnerData));

  return axios.post(INDIVIDUAL_REGISTRATION_URL, body);
}

function requestEditLink(data) {
  const id = encodeURIComponent(data[ID_NUMBER].trim());
  const dob = encodeURIComponent(data[DOB]);
  const url = `${REQUEST_REG_UPDATE_LINK_URL}?civilId=${id}&birthdate=${dob}`;
  return axios.post(url, data, { responseType: 'text' });
}

function getExistingRegistration(id) {
  return axios.get(`${GET_REGISTRATION_DETAILS_URL}${id}`);
}

function getFinalPrice({ registrations, promoCode }) {
  const [types, races] = _.map([`type`, `race`], (field) =>
    _.map(registrations, field).join(`,`)
  );
  const nr = registrations.length;
  const code = encodeURIComponent(promoCode);
  const params = `?nr_enrollments=${nr}&types=${types}&races=${races}&discount_code=${code}`;
  return axios.get(`${GET_FINAL_PRICE_URL}${params}`);
}

function postUpdatedRegistration({ id, data }) {
  const body = new FormData();
  const file = data[EVIDENCE_UPLOAD];

  if (file && needsEvidence(data)) {
    body.set('starting_line_proof', file);
  }

  body.set('payload', JSON.stringify(_.pick(data, REQUIRED_DATA)));
  return axios.put(`${GET_REGISTRATION_DETAILS_URL}${id}`, body, {
    responseType: 'text',
  });
}

// Watcher sagas
// function* callGetPrices() {
//   try {
//     const { data } = yield call(getPrices);
//     yield put(getPricesSuccessful(data));
//   } catch (e) {
//     yield put(getPricesFailed(getFirstErrorMsg(e)));
//   }
// }
function* callGetShirts() {
  try {
    const { data } = yield call(getShirts);
    yield put(getShirtsSuccessful(data));
  } catch (e) {
    yield put(getShirtsFailed(getFirstErrorMsg(e)));
  }
}

function* callGetIDCheck({ payload: id }) {
  // it shouldn't get this far if there's no ID, but just in case
  if (!id) {
    yield put(getIDCheckSuccessful(false));
    return;
  }

  try {
    const response = yield call(getIDCheck, id);
    const { is_duplicate: isDuplicate } = response.data;

    if (isDuplicate) {
      yield put(
        getIDCheckFailed(`Já alguém se registou com o BI/CC inserido (${id})`)
      );
      return;
    }

    yield put(getIDCheckSuccessful(isDuplicate));
  } catch (e) {
    yield put(getIDCheckFailed(getFirstErrorMsg(e)));
  }
}

function* callPostRegistration({ payload: data }) {
  try {
    const result = yield call(postRegistration, data);
    yield put(postRegistrationSuccessful(result.data));
  } catch (e) {
    yield put(postRegistrationFailed(getFirstErrorMsg(e)));
  }
}

function* callGetPaymentStatus({ payload: data }) {
  try {
    const result = yield call(getPaymentStatus, data);
    yield put(getPaymentStatusSuccessful(result.data));
  } catch (e) {
    yield put(getPaymentStatusFailed(getFirstErrorMsg(e)));
  }
}

function* callGetEditLink({ payload }) {
  try {
    yield call(requestEditLink, payload);
    yield put(requestEditLinkSuccess(true));
  } catch (e) {
    yield put(requestEditLinkFailed(getFirstErrorMsg(e)));
  }
}

function* callGetExistingRegistration({ payload }) {
  try {
    const { data } = yield call(getExistingRegistration, payload);
    const size = data[SHIRT_SIZE];
    data.originalShirt = { gender: data[GENDER], size };

    yield put(getRegistrationSuccess(data));
  } catch (e) {
    yield put(
      getRegistrationFailed({ message: getFirstErrorMsg(e), data: payload })
    );
  }
}

function* callPostUpdatedRegistration({ payload }) {
  try {
    yield call(postUpdatedRegistration, payload);
    yield put(postRegUpdateSuccess(true));
  } catch (e) {
    yield put(postRegUpdateFailed(getFirstErrorMsg(e)));
  }
}
function* callApplyPromoCode({ payload }) {
  try {
    const { data } = yield call(getFinalPrice, payload);
    yield put(applyPromoCodeSuccess(data));
  } catch (e) {
    yield put(applyPromoCodeFailed(getFirstErrorMsg(e)));
  }
}

// Roots sagas
// export function* getPricesSaga() {
//   yield takeEvery(GET_PRICES_REQUESTING, callGetPrices);
// }

export function* getShirtsSaga() {
  yield takeEvery(GET_SHIRTS_REQUESTING, callGetShirts);
}

function* getIDCheckSaga() {
  yield takeEvery(GET_ID_CHECK_REQUESTING, callGetIDCheck);
}

function* postRegistrationSaga() {
  yield takeEvery(POST_REGISTRATION_REQUESTING, callPostRegistration);
}

function* getPaymentStatusSaga() {
  yield takeEvery(GET_PAYMENT_STATUS_REQUESTING, callGetPaymentStatus);
}

function* getEditLinkSaga() {
  yield takeEvery(GET_EDIT_REG_LINK_REQUESTING, callGetEditLink);
}

function* getExistingRegistrationSaga() {
  yield takeEvery(GET_REG_DETAILS_REQUESTING, callGetExistingRegistration);
}

function* postUpdatedRegistrationSaga() {
  yield takeEvery(POST_UPDATE_REQUESTING, callPostUpdatedRegistration);
}
function* applyPromoCodeSaga() {
  yield takeLatest(APPLY_PROMO_CODE_REQUESTING, callApplyPromoCode);
}

// All sagas to be loaded
export default function* rootSaga() {
  yield all([
    // getPricesSaga(),
    getShirtsSaga(),
    getIDCheckSaga(),
    postRegistrationSaga(),
    getPaymentStatusSaga(),
    getExistingRegistrationSaga(),
    postUpdatedRegistrationSaga(),
    getEditLinkSaga(),
    applyPromoCodeSaga(),
  ]);
}
