import _ from 'lodash';
import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom';
import { func, number, object } from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators, compose } from 'redux';
import { createStructuredSelector } from 'reselect';
import { FormattedHTMLMessage, injectIntl, intlShape } from 'react-intl';
import { getParam, isLoadingInProgress } from 'utils/functions';
import injectSaga from 'utils/injectSaga';
import { LoadingScreen, ErrorMsgHandler } from 'components';
import { FullWidthDiv } from 'components/styled';
import * as actions from './actions';
import saga from './saga';
import {
  selectShirtsState,
  selectIDCheckState,
  selectRunnerDetailsState,
  selectRegistrationPostState,
  selectPaymentStatusState,
  selectCurrentRunnerIndex,
  selectEditRegistration,
  selectUpdatedRegistrationPost,
  selectEditLinkRequest,
  selectGDPRForOthers,
  selectPromoCode,
  selectAppliedPromoCode,
} from './selectors';
import {
  PAYMENT_METHOD,
  CREDIT_CARD,
  ID_NUMBER,
  PAYMENT_CONFIRMATION_URL,
  DETAILS_CONFIRMATION_URL,
  REGISTRATION_FORM_URL,
  ADD_RUNNER_URL,
  EDIT_RUNNER_URL,
  EDIT_REGISTRATION_URL,
  REQUEST_EDIT_LINK_URL,
} from './constants';
import messages from './messages';
import {
  AddRunner,
  EditRunner,
  IndividualRegistration,
  EditRegistration,
  RequestEditLink,
  RaceSelect,
  DetailsConfirmation,
  PaymentConfirmation,
} from './pages';
import { applyRequiredChanges, validateRunnerDetails } from './utils';

class Registration extends Component {
  static propTypes = {
    getIDCheck: func.isRequired,
    getIDCheckFailed: func.isRequired,
    // getPrices: func.isRequired,
    getShirts: func.isRequired,
    currentRunnerIndex: number.isRequired,
    createRunnerDetails: func.isRequired,
    updateRunnerDetails: func.isRequired,
    intl: intlShape.isRequired,
    history: object.isRequired,
    location: object.isRequired,
    idCheck: object.isRequired,
    postingState: object.isRequired,
    shirtSizesState: object.isRequired,
    runnerState: object.isRequired,
    paymentStatusState: object.isRequired,
    resetIDCheckState: func.isRequired,
    resetRegistrationState: func.isRequired,
    resetPaymentStatusState: func.isRequired,
    hasAskedOthersAboutGDPR: func.isRequired,
    setPromoCode: func.isRequired,
    resetPromoCodeState: func.isRequired,
  };

  state = { loading: true };

  componentWillMount() {
    const {
      // getPrices,
      getShirts,
      createRunnerDetails,
      hasAskedOthersAboutGDPR,
      resetPromoCodeState,
    } = this.props;

    this.setPromoCode();
    // getPrices();
    getShirts();
    createRunnerDetails();
    resetPromoCodeState();
    hasAskedOthersAboutGDPR(false);
  }

  componentWillReceiveProps(nextProps) {
    const {
      postingState: { data: postResponse },
      location: { pathname: currentLocation },
      history: { push },
    } = nextProps;
    const { loading } = this.state;
    const stateChange = {};

    if (!loading && isLoadingInProgress(nextProps)) {
      stateChange.loading = true;
    } else if (!isLoadingInProgress(nextProps) && loading) {
      stateChange.loading = false;
    }

    if (postResponse && currentLocation !== PAYMENT_CONFIRMATION_URL) {
      const { creditcard_link: link } = postResponse;

      if (link && postResponse[PAYMENT_METHOD] === CREDIT_CARD) {
        window.location = link;
        return;
      }

      push(PAYMENT_CONFIRMATION_URL);
    }

    if (!_.isEmpty(stateChange)) this.setState(stateChange);
  }

  getShirtSizes(runner) {
    const { shirtSizesState } = this.props;
    if (!_.get(runner, 'gender') || !_.get(shirtSizesState, 'data')) {
      return [];
    }
    const sizeData = _.find(shirtSizesState.data, {
      type: runner.type,
      gender: runner.gender,
    });
    return _.get(sizeData, 'sizes') || [];
  }

  setPromoCode() {
    const { setPromoCode } = this.props;
    const code = getParam('discount_code');
    if (code) setPromoCode(code);
  }

  setIDCheck(id) {
    clearTimeout(this.state.timeout);
    if (!id) return;
    this.setState({ timeout: setTimeout(this.checkID(id), 1000) });
  }

  checkID = (id) => () => {
    const {
      getIDCheck,
      getIDCheckFailed,
      runnerState: { data: runners },
    } = this.props;

    if (_.filter(runners, [ID_NUMBER, id]).length > 1) {
      // set error message
      getIDCheckFailed(`Já alguém tem o BI/CC inserido (${id})`);
      return;
    }

    getIDCheck(id);
  };

  handleChange = (newData) => {
    const {
      currentRunnerIndex,
      runnerState: { data: runners },
      updateRunnerDetails,
    } = this.props;
    const data = runners[currentRunnerIndex];
    const isIdChange = _(newData)
      .keys()
      .includes(ID_NUMBER);

    if (isIdChange) {
      this.setIDCheck(newData[ID_NUMBER]);
    }

    applyRequiredChanges(data, newData);
    updateRunnerDetails({ ...data, ...newData });
  };

  formatMsg = (id, values = {}) => {
    const { formatMessage } = this.props.intl;

    if (!messages[id]) {
      console.error('[formatMsg] Message not found:', id);
      return id;
    }

    return formatMessage(messages[id], values);
  };

  formatHTMLMsg = (id, values = {}) => {
    if (!messages[id]) {
      console.error('[formatHTMLMsg] Message not found:', id);
      return id;
    }

    return <FormattedHTMLMessage {...messages[id]} values={values} />;
  };

  findErrors = (runner) => {
    const errors = validateRunnerDetails(runner, this.getShirtSizes(runner));
    if (!errors) return null;
    return _.map(_.castArray(errors), this.formatMsg).join('\n');
  };

  renderErrorMsg() {
    const {
      idCheck,
      postingState,
      paymentStatusState,
      resetIDCheckState,
      resetRegistrationState,
      resetPaymentStatusState,
    } = this.props;

    const stateCollection = [
      { error: postingState.error, onClose: resetRegistrationState },
      { error: idCheck.error, onClose: resetIDCheckState },
      { error: paymentStatusState.error, onClose: resetPaymentStatusState },
    ];

    return <ErrorMsgHandler stateCollection={stateCollection} />;
  }

  renderContent() {
    const {
      shirtSizesState,
      runnerState: { data: runners },
      currentRunnerIndex,
    } = this.props;

    if (!runners || !shirtSizesState.data) return null;

    const props = {
      ...this.props,
      runner: runners[currentRunnerIndex],
      allRunners: runners,
      formatMsg: this.formatMsg,
      formatHTMLMsg: this.formatHTMLMsg,
      findErrors: this.findErrors,
      updateRunner: this.handleChange,
    };

    return (
      <Switch>
        <Route
          exact
          path={PAYMENT_CONFIRMATION_URL}
          render={() => <PaymentConfirmation {...props} />}
        />

        <Route
          path={EDIT_REGISTRATION_URL}
          render={() => <EditRegistration {...props} />}
        />

        <Route
          exact
          path={REQUEST_EDIT_LINK_URL}
          render={() => <RequestEditLink {...props} />}
        />

        <Route
          exact
          path={ADD_RUNNER_URL}
          render={() => <AddRunner {...props} />}
        />

        <Route
          exact
          path={EDIT_RUNNER_URL}
          render={() => <EditRunner {...props} />}
        />

        <Route
          exact
          path={DETAILS_CONFIRMATION_URL}
          render={() => <DetailsConfirmation {...props} />}
        />

        <Route
          exact
          path={REGISTRATION_FORM_URL}
          render={() => <IndividualRegistration {...props} />}
        />

        <Route path="/" render={() => <RaceSelect {...props} />} />
      </Switch>
    );
  }

  renderLoadingScreen() {
    return this.state.loading ? <LoadingScreen /> : null;
  }

  render() {
    return (
      <FullWidthDiv>
        {this.renderErrorMsg()}
        {this.renderContent()}
        {this.renderLoadingScreen()}
      </FullWidthDiv>
    );
  }
}

const mapStateToProps = createStructuredSelector({
  idCheck: selectIDCheckState(),
  runnerState: selectRunnerDetailsState(),
  postingState: selectRegistrationPostState(),
  paymentStatusState: selectPaymentStatusState(),
  currentRunnerIndex: selectCurrentRunnerIndex(),
  editRegistration: selectEditRegistration(),
  updatedRegistrationPostState: selectUpdatedRegistrationPost(),
  editLinkRequest: selectEditLinkRequest(),
  hasUserConfirmedGDPRForOthers: selectGDPRForOthers(),
  promoCode: selectPromoCode(),
  appliedPromoCode: selectAppliedPromoCode(),
  shirtSizesState: selectShirtsState(),
});

const mapDispatchToProps = (dispatch) => bindActionCreators(actions, dispatch);

const withConnect = connect(
  mapStateToProps,
  mapDispatchToProps
);
const withSaga = injectSaga({ key: 'registration', saga });

export default injectIntl(
  compose(
    withSaga,
    withConnect
  )(Registration)
);
