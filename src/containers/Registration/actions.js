import {
  // RESET_PRICE_STATE,
  // GET_PRICES_REQUESTING,
  // GET_PRICES_FAILED,
  // GET_PRICES_SUCCESSFUL,
  RESET_SHIRT_STATE,
  GET_SHIRTS_REQUESTING,
  GET_SHIRTS_FAILED,
  GET_SHIRTS_SUCCESSFUL,
  POST_REGISTRATION_REQUESTING,
  POST_REGISTRATION_FAILED,
  POST_REGISTRATION_SUCCESSFUL,
  GET_PAYMENT_STATUS_REQUESTING,
  GET_PAYMENT_STATUS_FAILED,
  GET_PAYMENT_STATUS_SUCCESSFUL,
  CREATE_RUNNER_DETAILS,
  UPDATE_RUNNER_DETAILS,
  RESET_REGISTRATION_STATE,
  RESET_PAYMENT_STATUS_STATE,
  GET_ID_CHECK_REQUESTING,
  GET_ID_CHECK_FAILED,
  GET_ID_CHECK_SUCCESSFUL,
  RESET_ID_CHECK_STATE,
  ADD_NEW_RUNNER,
  DELETE_RUNNER,
  SET_CURRENT_RUNNER_INDEX,
  GET_REG_DETAILS_REQUESTING,
  GET_REG_DETAILS_FAILED,
  GET_REG_DETAILS_SUCCESSFUL,
  UPDATE_REG_DETAILS,
  POST_UPDATE_REQUESTING,
  POST_UPDATE_FAILED,
  POST_UPDATE_SUCCESSFUL,
  RESET_REG_EDIT_STATE,
  RESET_UPDATE_POST_STATE,
  RESET_EDIT_REG_LINK_REQUEST_STATE,
  GET_EDIT_REG_LINK_REQUESTING,
  GET_EDIT_REG_LINK_FAILED,
  GET_EDIT_REG_LINK_SUCCESSFUL,
  SET_GDPR_FOR_OTHERS,
  SET_PROMO_CODE,
  RESET_PROMO_CODE_STATE,
  APPLY_PROMO_CODE_REQUESTING,
  APPLY_PROMO_CODE_FAILED,
  APPLY_PROMO_CODE_SUCCESSFUL,
  RESET_ALL_STATE,
} from './constants';

// ~~~~~~~~~~ GET PRICES ~~~~~~~~~~
// export const resetPriceState = () => ({ type: RESET_PRICE_STATE });
// export const getPrices = () => ({ type: GET_PRICES_REQUESTING });
// export const getPricesFailed = (payload) => ({
//   type: GET_PRICES_FAILED,
//   payload,
// });
// export const getPricesSuccessful = (payload) => ({
//   type: GET_PRICES_SUCCESSFUL,
//   payload,
// });

// ~~~~~~~~~~ GET SHIRTS ~~~~~~~~~~
export const resetShirtState = () => ({ type: RESET_SHIRT_STATE });
export const getShirts = () => ({ type: GET_SHIRTS_REQUESTING });
export const getShirtsFailed = (payload) => ({
  type: GET_SHIRTS_FAILED,
  payload,
});
export const getShirtsSuccessful = (payload) => ({
  type: GET_SHIRTS_SUCCESSFUL,
  payload,
});

// ~~~~~~~~~~ CHECK ID ~~~~~~~~~~
export const resetIDCheckState = () => ({ type: RESET_ID_CHECK_STATE });
export const getIDCheck = (payload) => ({
  type: GET_ID_CHECK_REQUESTING,
  payload,
});
export const getIDCheckFailed = (payload) => ({
  type: GET_ID_CHECK_FAILED,
  payload,
});
export const getIDCheckSuccessful = (payload) => ({
  type: GET_ID_CHECK_SUCCESSFUL,
  payload,
});

// ~~~~~~~~~~ POST INDIVIDUAL REGISTRATION ~~~~~~~~~~
export const resetRegistrationState = () => ({
  type: RESET_REGISTRATION_STATE,
});
export const postRegistration = (payload) => ({
  type: POST_REGISTRATION_REQUESTING,
  payload,
});
export const postRegistrationFailed = (payload) => ({
  type: POST_REGISTRATION_FAILED,
  payload,
});
export const postRegistrationSuccessful = (payload) => ({
  type: POST_REGISTRATION_SUCCESSFUL,
  payload,
});

// ~~~~~~~~~~ GET PAYMENT STATUS ~~~~~~~~~~
export const resetPaymentStatusState = () => ({
  type: RESET_PAYMENT_STATUS_STATE,
});
export const getPaymentStatus = (payload) => ({
  type: GET_PAYMENT_STATUS_REQUESTING,
  payload,
});
export const getPaymentStatusFailed = (payload) => ({
  type: GET_PAYMENT_STATUS_FAILED,
  payload,
});
export const getPaymentStatusSuccessful = (payload) => ({
  type: GET_PAYMENT_STATUS_SUCCESSFUL,
  payload,
});

// ~~~~~~~~~~ CREATE/UPDATE RUNNER DETAILS ~~~~~~~~~~
export const createRunnerDetails = (payload) => ({
  type: CREATE_RUNNER_DETAILS,
  payload,
});
export const updateRunnerDetails = (payload) => ({
  type: UPDATE_RUNNER_DETAILS,
  payload,
});

export const addRunner = () => ({ type: ADD_NEW_RUNNER });
export const deleteRunner = (payload) => ({ type: DELETE_RUNNER, payload });
export const setRunnerIndex = (payload) => ({
  type: SET_CURRENT_RUNNER_INDEX,
  payload,
});

// ~~~~~~~~~~ REQUEST REGISTRATION EDIT LINK ~~~~~~~~~~
export const resetEditRegLinkRequestState = () => ({
  type: RESET_EDIT_REG_LINK_REQUEST_STATE,
});
export const requestEditLink = (payload) => ({
  type: GET_EDIT_REG_LINK_REQUESTING,
  payload,
});
export const requestEditLinkFailed = (payload) => ({
  type: GET_EDIT_REG_LINK_FAILED,
  payload,
});
export const requestEditLinkSuccess = (payload) => ({
  type: GET_EDIT_REG_LINK_SUCCESSFUL,
  payload,
});

// ~~~~~~~~~~ EDIT EXISTING REGISTRATION ~~~~~~~~~~
export const resetEditRegistrationState = () => ({
  type: RESET_REG_EDIT_STATE,
});
export const getRegistration = (payload) => ({
  type: GET_REG_DETAILS_REQUESTING,
  payload,
});
export const getRegistrationFailed = (payload) => ({
  type: GET_REG_DETAILS_FAILED,
  payload,
});
export const getRegistrationSuccess = (payload) => ({
  type: GET_REG_DETAILS_SUCCESSFUL,
  payload,
});
export const updateRegistration = (payload) => ({
  type: UPDATE_REG_DETAILS,
  payload,
});

// ~~~~~~~ POST UPDATE FOR EXISTING REGISTRATION ~~~~~~~
export const postRegUpdate = (payload) => ({
  type: POST_UPDATE_REQUESTING,
  payload,
});

export const postRegUpdateFailed = (payload) => ({
  type: POST_UPDATE_FAILED,
  payload,
});

export const postRegUpdateSuccess = (payload) => ({
  type: POST_UPDATE_SUCCESSFUL,
  payload,
});

export const resetUpdatePostState = () => ({
  type: RESET_UPDATE_POST_STATE,
});

// ~~~~~~ HANDLING OPT-INS ON BEHALF OF OTHERS ~~~~~
export const hasAskedOthersAboutGDPR = (payload) => ({
  type: SET_GDPR_FOR_OTHERS,
  payload,
});

// ~~~~~~ SET PROMO CODE ~~~~~
export const setPromoCode = (payload) => ({
  type: SET_PROMO_CODE,
  payload,
});

// ~~~~~~ APPLY PROMO CODE ~~~~~
export const resetPromoCodeState = () => ({ type: RESET_PROMO_CODE_STATE });
export const applyPromoCode = (payload) => ({
  type: APPLY_PROMO_CODE_REQUESTING,
  payload,
});
export const applyPromoCodeFailed = (payload) => ({
  type: APPLY_PROMO_CODE_FAILED,
  payload,
});
export const applyPromoCodeSuccess = (payload) => ({
  type: APPLY_PROMO_CODE_SUCCESSFUL,
  payload,
});

export const resetAllState = () => ({ type: RESET_ALL_STATE });
