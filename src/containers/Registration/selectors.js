import { createSelector } from 'reselect';

// Direct selector to the registration state domain
const selectRegistrationDomain = (state) => state.get('registration');

// Substate selector
const getSubstate = (key) => () =>
  createSelector(
    selectRegistrationDomain,
    (substate) => substate.get(key)
  );

// Other specific selectors
const selectShirtsState = getSubstate('shirtSizes');
const selectIDCheckState = getSubstate('checkIDState');
const selectRunnerDetailsState = getSubstate('runnerDetails');
const selectRegistrationPostState = getSubstate('registrationPost');
const selectPaymentStatusState = getSubstate('paymentStatus');
const selectCurrentRunnerIndex = getSubstate('currentRunnerIndex');
const selectEditLinkRequest = getSubstate('editLinkRequest');
const selectEditRegistration = getSubstate('editRegistration');
const selectUpdatedRegistrationPost = getSubstate('updatedRegistrationPost');
const selectGDPRForOthers = getSubstate('hasAskedOthersAboutGDPR');
const selectPromoCode = getSubstate('promoCode');
const selectAppliedPromoCode = getSubstate('appliedPromoCode');

// Default selector used by Registration
const selectRegistration = () =>
  createSelector(
    selectRegistrationDomain,
    (substate) => substate
  );

export {
  selectRegistration,
  selectPaymentStatusState,
  selectRegistrationPostState,
  selectRunnerDetailsState,
  selectIDCheckState,
  selectCurrentRunnerIndex,
  selectEditRegistration,
  selectUpdatedRegistrationPost,
  selectEditLinkRequest,
  selectGDPRForOthers,
  selectPromoCode,
  selectAppliedPromoCode,
  selectShirtsState,
};
