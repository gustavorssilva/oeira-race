import { BASE_URL, API_RACE_NAME } from 'utils/constants';

const BASE_WITH_RACE = `${BASE_URL}${API_RACE_NAME}/`;

// ~~~~~~~~ Endpoints ~~~~~~~~
export const GET_PRICES_URL = `${BASE_WITH_RACE}price_status`;
export const GET_SHIRTS_URL = `${BASE_WITH_RACE}available_shirt_sizes`;
export const INDIVIDUAL_REGISTRATION_URL = `${BASE_URL}enrollments/batch_insert`;
export const TEAM_REGISTRATION_URL = `${BASE_URL}teams`;
export const TEAM_EXCEL_PARSE_URL = `${BASE_WITH_RACE}team_enrollment_xlsx`;
export const PAYMENT_STATUS_URL = `${BASE_URL}payments/`;
export const GET_ID_CHECK_URL = `${BASE_WITH_RACE}verify_civil_id/`;
export const REQUEST_REG_UPDATE_LINK_URL = `${BASE_WITH_RACE}enrollments/request_update`;
export const GET_REGISTRATION_DETAILS_URL = `${BASE_URL}enrollments/`;
export const UPDATE_REGISTRATION_URL = `${BASE_URL}update_registration`;
export const GET_FINAL_PRICE_URL = `${BASE_URL}final_price`;
