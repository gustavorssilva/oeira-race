import { PRICES_BEFORE_CHANGE, PRICES_AFTER_CHANGE } from 'utils/constants';
import countries from 'containers/Registration/utils/countries';
import localidades from 'containers/Registration/utils/localidades';

// ~~~~~~~~ Price levels ~~~~~~~~
export const BEFORE_PRICE = `before`;
export const AFTER_PRICE = `after`;
export const PRICE_LEVELS = [BEFORE_PRICE];
export const PRICE_BY_LEVEL = {
  [BEFORE_PRICE]: PRICES_BEFORE_CHANGE,
  [AFTER_PRICE]: PRICES_AFTER_CHANGE,
};

// ~~~~~~~~ Registration types ~~~~~~~~
export const INDIVIDUAL = 'normal';
export const CREATE_TEAM = 'createTeam';
export const JOIN_TEAM = 'joinTeam';
export const EDIT = 'edit_registration';

export const REGISTRATION_TYPES = [INDIVIDUAL];

// ~~~~~~~~ Team registration details ~~~~~~~~
export const TEAM_NAME = 'team_name';
export const MANAGER_NAME = 'manager_name';
export const MANAGER_EMAIL = 'manager_email';
export const MANAGER_PHONE = 'manager_phone';
export const KIT_DELIVERY = 'kit_delivery';
export const ENROLLMENTS = 'enrollments';
export const KIT_DELIVERY_OPTION_INDIVIDUAL = 'individual';
export const KIT_DELIVERY_OPTION_GROUP = 'group';

// ~~~~~~~~ Individual registration details ~~~~~~~~
export const RACE = `race`;
export const REGISTRATION_TYPE = 'type';
export const CHIP = 'type';
export const ID_NUMBER = 'cc';
export const DOB = 'b_date';
export const FULL_NAME = 'name';
export const COUNTRY_OF_RESIDENCE = 'nationality';
export const DISTRICT = 'residence_county';
export const EMAIL = 'email';
export const EMAIL_CONFIRMATION = 'email_confirmation';
export const PHONE = 'phone_number';
export const GENDER = 'gender';
export const SHIRT_SIZE = 'shirt_size';
export const DORSAL_NAME = 'dorsal_name';
export const START_BOX = 'start_box';
export const EVIDENCE_UPLOAD = 'starting_line_proof';
export const AGREE_TO_TERMS = 'optin_02';
export const AGREE_TO_PRIVACY_POLICY = 'optin_03';
export const ACCEPT_EMAIL_CONTACT = 'optin_01';
export const ACCEPT_PHONE_CONTACT = 'optin_04';
export const ACCEPT_EMAIL_CONTACT_OTHER_EVENTS = 'optin_05';
export const PAYMENT_METHOD = 'payment_method';
export const GUARDIAN_NAME = 'guardian_name';

export const OPT_INS = [
  AGREE_TO_TERMS,
  AGREE_TO_PRIVACY_POLICY,
  ACCEPT_EMAIL_CONTACT,
  ACCEPT_PHONE_CONTACT,
  ACCEPT_EMAIL_CONTACT_OTHER_EVENTS,
];

export const NON_FILE_FIELDS = [
  RACE,
  CHIP,
  ID_NUMBER,
  DOB,
  FULL_NAME,
  GENDER,
  COUNTRY_OF_RESIDENCE,
  DISTRICT,
  EMAIL,
  PHONE,
  SHIRT_SIZE,
  DORSAL_NAME,
  START_BOX,
  ...OPT_INS,
];

export const CONFIRM_DETAILS_FIELDS = [
  CHIP,
  ID_NUMBER,
  DOB,
  GENDER,
  COUNTRY_OF_RESIDENCE,
  DISTRICT,
  EMAIL,
  PHONE,
  SHIRT_SIZE,
  DORSAL_NAME,
  START_BOX,
];

export const FORM_FIELDS = [
  ...NON_FILE_FIELDS,
  EVIDENCE_UPLOAD,
  AGREE_TO_TERMS,
];

export const EDIT_TEAM_MEMBER_FORM_FIELDS = [
  CHIP,
  ID_NUMBER,
  DOB,
  COUNTRY_OF_RESIDENCE,
  GENDER,
  DISTRICT,
  EMAIL,
  PHONE,
  SHIRT_SIZE,
  DORSAL_NAME,
  START_BOX,
];

export const REQUIRED_DATA = [...FORM_FIELDS, PAYMENT_METHOD];

// ~~~~~~~~ Payment types ~~~~~~~~
export const CREDIT_CARD = 'creditcard';
export const PAYPAL = 'paypal';
export const MULTIBANCO = 'mbreference';

export const PAYMENT_TYPES = [CREDIT_CARD, PAYPAL, MULTIBANCO];

// ~~~~~~~~ Invoice fields ~~~~~~~~
export const BILLING_NAME = 'billing_name';
export const BILLING_ADDRESS = 'billing_address';
export const BILLING_CITY = 'billing_city';
export const BILLING_POSTCODE = 'billing_postcode';
export const BILLING_TAX_NUMBER = 'billing_tax_number';

export const INVOICE_FIELDS = [
  BILLING_NAME,
  BILLING_ADDRESS,
  BILLING_CITY,
  BILLING_POSTCODE,
  BILLING_TAX_NUMBER,
];

// ~~~~~~~~ Reg. details values ~~~~~~~~
export const MALE = 'M';
export const FEMALE = 'F';

export const SHIRT_S = 'S';
export const SHIRT_M = 'M';
export const SHIRT_L = 'L';
export const SHIRT_XL = 'XL';

export const SHIRT_6_8 = '6/8';
export const SHIRT_8_10 = '8/10';
export const SHIRT_10_12 = '10/12';
export const SHIRT_12_14 = '12/14';

export const START_BOX_NO_CHIP = ``;
export const START_BOX_GENERAL = '>60';
export const START_BOX_FAST_RUNNERS = `FastRunners`;

export const OPTIONS_BY_FIELD = {
  [CHIP]: [
    { text: `without_chip`, value: `nochip` },
    { text: `with_chip`, value: `chip` },
  ],
  [GENDER]: [MALE, FEMALE],
  [SHIRT_SIZE]: [SHIRT_S, SHIRT_M, SHIRT_L, SHIRT_XL],
  [START_BOX]: [START_BOX_GENERAL, START_BOX_FAST_RUNNERS],
  [KIT_DELIVERY]: [KIT_DELIVERY_OPTION_GROUP, KIT_DELIVERY_OPTION_INDIVIDUAL],
};

export const DEFAULT_START_BOX_AND_DORSAL_DATA = {
  [START_BOX]: START_BOX_NO_CHIP,
  [DORSAL_NAME]: '---',
};

// ~~~~~~~~ Reg. details values ~~~~~~~~
export const START_BOX_MODAL = 'start_box_modal';

export const MODAL_MSGS = {
  [START_BOX]: START_BOX_MODAL,
};

export const DROPDOWN_OPTIONS_BY_FIELD = {
  [COUNTRY_OF_RESIDENCE]: countries,
  [DISTRICT]: localidades,
};

export const DEFAULT_DISTRICT = 'N/A';

// ~~~~~~~~ Age restrictions ~~~~~~~~
export const MIN_ADULTS_BIRTHDATE = Date.UTC(2001, 6, 15); // 18 years old

export const ERROR_EDIT_REQUEST_RUNNER_NOT_FOUND =
  'enrollments.enrollmentnotfound';

export const NON_EDITABLE_FIELDS = [CHIP, ID_NUMBER];
