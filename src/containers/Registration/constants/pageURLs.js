// ~~~~~~~~ Page URLs ~~~~~~~~
const urlBase = '/individual/';
export const PAYMENT_CONFIRMATION_URL = `${urlBase}confirmacao-de-pagamento`;
export const DETAILS_CONFIRMATION_URL = `${urlBase}confirmacao-de-dados`;
export const REGISTRATION_FORM_URL = `${urlBase}inscricao`;
export const ADD_RUNNER_URL = `${urlBase}adicionar`;
export const EDIT_RUNNER_URL = `${urlBase}alterar-dados`;
export const REQUEST_EDIT_LINK_URL = `${urlBase}procurar-inscricao`;
// The lack of urlBase here is to match the link in the email
export const EDIT_REGISTRATION_URL = '/alterar-inscricao/';
