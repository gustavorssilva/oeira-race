import { defineMessages } from 'react-intl';
import {
  INDIVIDUAL,
  CREATE_TEAM,
  JOIN_TEAM,
  EDIT,
  CHIP,
  ID_NUMBER,
  DOB,
  FULL_NAME,
  COUNTRY_OF_RESIDENCE,
  DISTRICT,
  EMAIL,
  EMAIL_CONFIRMATION,
  PHONE,
  GENDER,
  SHIRT_SIZE,
  DORSAL_NAME,
  START_BOX,
  EVIDENCE_UPLOAD,
  MALE,
  FEMALE,
  START_BOX_FAST_RUNNERS,
  START_BOX_GENERAL,
  START_BOX_NO_CHIP,
  START_BOX_MODAL,
  PAYMENT_METHOD,
  CREDIT_CARD,
  PAYPAL,
  MULTIBANCO,
  BILLING_NAME,
  BILLING_ADDRESS,
  BILLING_CITY,
  BILLING_POSTCODE,
  BILLING_TAX_NUMBER,
  AGREE_TO_TERMS,
  AGREE_TO_PRIVACY_POLICY,
  ACCEPT_EMAIL_CONTACT,
  ACCEPT_PHONE_CONTACT,
  ACCEPT_EMAIL_CONTACT_OTHER_EVENTS,
  KIT_DELIVERY,
  KIT_DELIVERY_OPTION_INDIVIDUAL,
  KIT_DELIVERY_OPTION_GROUP,
} from './constants';

const genericPaymentSuccess =
  'O pagamento foi efetuado com sucesso.\n\nBrevemente receberás um e-mail com a confirmação da tua inscrição e informações úteis acerca da prova.\n\nQualquer esclarecimento adicional podes entrar em contacto connosco através da nossa página de contactos.';

export default defineMessages({
  loading_prices: {
    id: 'app.containers.Registration.loading_prices',
    defaultMessage: 'A procurar valores...',
  },
  registration: {
    id: 'app.containers.Registration.registration',
    defaultMessage: 'inscrição',
  },
  registration_pack: {
    id: 'app.containers.Registration.registration_pack',
    defaultMessage: 'inscrição pack marginal à noite + corrida do tejo',
  },
  race_price_before: {
    id: 'app.containers.Registration.race_price_before',
    defaultMessage: '14 de Março – 17 de Março',
  },
  race_price_after: {
    id: 'app.containers.Registration.race_price_after',
    defaultMessage: '21 de Maio – 9 de Junho',
  },
  [INDIVIDUAL]: {
    id: 'app.containers.Registration.individual',
    defaultMessage: 'Inscrição',
  },
  [CREATE_TEAM]: {
    id: 'app.containers.Registration.create_team',
    defaultMessage: 'Criar equipa',
  },
  [JOIN_TEAM]: {
    id: 'app.containers.Registration.associate_team',
    defaultMessage: 'Associar a Equipa',
  },
  [EDIT]: {
    id: 'app.containers.Registration.edit_registration',
    defaultMessage: 'Alterar Inscrição',
  },
  edit_registration_title: {
    id: 'app.containers.Registration.edit_registration_title',
    defaultMessage: 'ALTERAR INSCRIÇÃO',
  },
  individual_registration_title: {
    id: 'app.containers.Registration.individual_registration',
    defaultMessage: 'Inscrição Individual',
  },
  team_registration_title: {
    id: 'app.containers.Registration.team_registration',
    defaultMessage: 'Criar equipa',
  },
  add_to_team_registration_title: {
    id: 'app.containers.Registration.add_to_team_registration',
    defaultMessage: 'Associar a Equipa',
  },
  [ID_NUMBER]: {
    id: 'app.containers.Registration.id_number',
    defaultMessage: 'BI, Cartão de Cidadão ou Passaporte',
  },
  [DOB]: {
    id: 'app.containers.Registration.dob',
    defaultMessage: 'Data de Nascimento (DD/MM/AAAA)',
  },
  [FULL_NAME]: {
    id: 'app.containers.Registration.full_name',
    defaultMessage: 'Nome (primeiro e último)',
  },
  [COUNTRY_OF_RESIDENCE]: {
    id: 'app.containers.Registration.residence_country',
    defaultMessage: 'País de residência',
  },
  [DISTRICT]: {
    id: 'app.containers.Registration.county',
    defaultMessage: 'Distrito',
  },
  [EMAIL]: {
    id: 'app.containers.Registration.email',
    defaultMessage: 'E-mail',
  },
  [EMAIL_CONFIRMATION]: {
    id: 'app.containers.Registration.email_confirmation',
    defaultMessage: 'Confirmação do e-mail',
  },
  [PHONE]: {
    id: 'app.containers.Registration.phone',
    defaultMessage: 'Número de Telemóvel',
  },
  [GENDER]: {
    id: 'app.containers.Registration.gender',
    defaultMessage: 'Género',
  },
  [MALE]: {
    id: 'app.containers.Registration.male',
    defaultMessage: 'Masculino',
  },
  [FEMALE]: {
    id: 'app.containers.Registration.female',
    defaultMessage: 'Feminino',
  },
  [SHIRT_SIZE]: {
    id: 'app.containers.Registration.shirt_size',
    defaultMessage: 'Tamanho de T-shirt',
  },
  [DORSAL_NAME]: {
    id: 'app.containers.Registration.dorsal_name',
    defaultMessage: 'Nome para dorsal',
  },
  [START_BOX]: {
    id: 'app.containers.Registration.start_box',
    defaultMessage: 'Caixa de Partida',
  },
  [EVIDENCE_UPLOAD]: {
    id: 'app.containers.Registration.evidence_upload',
    defaultMessage: 'Upload de comprovativo de tempo',
  },
  preview_dorsal: {
    id: 'app.containers.Registration.preview_dorsal',
    defaultMessage: 'Pré-visualizar Dorsal',
  },
  fill_all_fields: {
    id: 'app.containers.Registration.fill_all_fields',
    defaultMessage: 'Todos os campos são de preenchimento obrigatório.',
  },
  [START_BOX_NO_CHIP]: {
    id: `app.containers.Registration.start_box_${START_BOX_NO_CHIP}`,
    defaultMessage: 'Sem chip',
  },
  [START_BOX_FAST_RUNNERS]: {
    id: `app.containers.Registration.start_box_${START_BOX_FAST_RUNNERS}`,
    defaultMessage: 'Fast Runners',
  },
  [START_BOX_GENERAL]: {
    id: `app.containers.Registration.start_box_${START_BOX_GENERAL}`,
    defaultMessage: 'Geral',
  },
  [START_BOX_MODAL]: {
    id: 'app.containers.Registration.start_box_modal',
    defaultMessage:
      "Se a tua caixa de entrada não é de +60', terás de fazer o upload do comprovativo de tempo.\n\nOs ficheiros suportados são:\n.jpg e .pdf.\n\nSerá enviado um email, após validação do comprovativo por parte da organização.",
  },
  start_box_explanation: {
    id: 'app.containers.Registration.start_box_explanation',
    defaultMessage: `Se quiseres entrar na caixa de partida "Fast Runners", terás de fazer o upload de um comprovativo de tempo – ou de uma prova de 8km com tempo inferior a 36’, ou de uma prova de 10km com tempo inferior a 45’. Será enviado um email, após validação do comprovativo por parte da organização.`,
  },
  missing_the_following_fields: {
    id: 'app.containers.Registration.missing_the_following_fields',
    defaultMessage: 'A informação seguinte está em falta:',
  },
  missing_the_following_permissions: {
    id: 'app.containers.Registration.missing_the_following_permissions',
    defaultMessage: 'É preciso aceitar o seguinte:',
  },
  invalid_email: {
    id: 'app.containers.Registration.invalid_email',
    defaultMessage: 'Email inválido',
  },
  email_not_confirmed: {
    id: 'app.containers.Registration.email_not_confirmed',
    defaultMessage: 'O email e a confirmação do email não coincidem',
  },
  invalid_phone: {
    id: 'app.containers.Registration.invalid_phone',
    defaultMessage: 'Número de telefone inválido',
  },
  no_xl_shirts_for_women: {
    id: 'app.containers.Registration.no_xl_shirts_for_women',
    defaultMessage: 'O tamanho do T-shirt deve ser S, M ou L',
  },
  terms: {
    id: 'app.containers.Registration.terms',
    defaultMessage: 'Regulamento',
  },
  continue: {
    id: 'app.containers.Registration.continue',
    defaultMessage: 'Avançar',
  },
  go_back: {
    id: 'app.containers.Registration.go_back',
    defaultMessage: 'Voltar',
  },
  pay: {
    id: 'app.containers.Registration.pay',
    defaultMessage: 'Pagar',
  },
  finalize: {
    id: 'app.containers.Registration.finalize',
    defaultMessage: 'Finalizar',
  },
  edit: {
    id: 'app.containers.Registration.edit',
    defaultMessage: 'Alterar',
  },
  delete: {
    id: 'app.containers.Registration.delete',
    defaultMessage: 'Apagar',
  },
  search: {
    id: 'app.containers.Registration.search',
    defaultMessage: 'Pesquisar',
  },
  no_matches: {
    id: 'app.containers.Registration.no_matches',
    defaultMessage: 'Nenhum resultado',
  },
  hello: {
    id: 'app.containers.Registration.hello',
    defaultMessage: 'Olá, {name}!',
  },
  continue_to_edit: {
    id: 'app.containers.Registration.continue_to_edit',
    defaultMessage: 'Avança para alterares os teus dados de inscrição.',
  },
  runner_not_found: {
    id: 'app.containers.Registration.runner_not_found',
    defaultMessage:
      'Não existe uma inscrição com estes dados.\n\nBI/CC: {id}\nData de nascimento: {dob}',
  },
  runner_not_found_or_already_associated: {
    id: 'app.containers.Registration.runner_not_found_or_already_associated',
    defaultMessage:
      'Não existe uma inscrição com estes dados ou esta pessoa já está associada a uma equipa',
  },
  registration_updated: {
    id: 'app.containers.Registration.registration_updated',
    defaultMessage: 'Inscrição atualizada',
  },
  cancel: {
    id: 'app.containers.Registration.cancel',
    defaultMessage: 'Voltar',
  },
  save: {
    id: 'app.containers.Registration.save',
    defaultMessage: 'Guardar',
  },
  yes: {
    id: 'app.containers.Registration.yes',
    defaultMessage: 'Sim',
  },
  no: {
    id: 'app.containers.Registration.no',
    defaultMessage: 'Não',
  },
  invoice: {
    id: 'app.containers.Registration.invoice',
    defaultMessage: 'Dados de Faturação',
  },
  add_runner: {
    id: 'app.containers.Registration.add_runner',
    defaultMessage: 'Adicionar Inscrição',
  },
  edit_runner: {
    id: 'app.containers.Registration.edit_runner',
    defaultMessage: 'Alterar dados',
  },
  confirm_delete: {
    id: 'app.containers.Registration.confirm_delete',
    defaultMessage: 'Tens a certeza de que pretendes apagar {name}?',
  },
  if_you_want_an_invoice: {
    id: 'app.containers.Registration.if_you_want_an_invoice',
    defaultMessage:
      'Se pretendes fatura com os teus dados, por favor preenche todos estes campos. Não será possível alterar esta informação depois de inscrever.',
  },
  invoice_confirmation_no_info: {
    id: 'app.containers.Registration.invoice_confirmation_no_info',
    defaultMessage:
      'Não quero fatura, e percebo que não será possível pedir uma fatura com os meus dados depois de inscrever.',
  },
  final_confirmation: {
    id: 'app.containers.Registration.final_confirmation',
    defaultMessage: 'Confirmação final',
  },
  please_confirm_the_following_data: {
    id: 'app.containers.Registration.please_confirm_the_following_data',
    defaultMessage:
      'Confirma por favor os dados de faturação. Não será possível alterar esta informação depois de inscrever.',
  },
  check_invoice_fields: {
    id: 'app.containers.Registration.check_invoice_fields',
    defaultMessage:
      'Se pretendes fatura com os teus dados, tens de preencher todos os campos. Se não, deixa-os vazios.',
  },
  no_going_back_after_clicking_pay: {
    id: 'app.containers.Registration.no_going_back_after_clicking_pay',
    defaultMessage:
      'Confirma por favor que este é o método de pagamento que pretendes utilizar uma vez que ao avançar serás redireccionado para outro website.',
  },
  [PAYMENT_METHOD]: {
    id: 'app.containers.Registration.payment_method',
    defaultMessage: 'Método de Pagamento',
  },
  details_confirmation: {
    id: 'app.containers.Registration.details_confirmation',
    defaultMessage: 'Dados da Inscrição',
  },
  payment: {
    id: 'app.containers.Registration.payment',
    defaultMessage: 'Pagamento',
  },
  multibanco_payment_details: {
    id: 'app.containers.Registration.multibanco_payment_details',
    defaultMessage: 'Dados de Pagamento por Multibanco',
  },
  entity: {
    id: 'app.containers.Registration.entity',
    defaultMessage: 'Entidade',
  },
  reference: {
    id: 'app.containers.Registration.reference',
    defaultMessage: 'Referência',
  },
  amount: {
    id: 'app.containers.Registration.amount',
    defaultMessage: 'Valor',
  },
  deadline: {
    id: 'app.containers.Registration.deadline',
    defaultMessage: 'Data limite',
  },
  congratulations: {
    id: 'app.containers.Registration.congratulations',
    defaultMessage: 'Parabéns, {name}!',
  },
  team_created: {
    id: 'app.containers.Registration.team_created',
    defaultMessage: 'A tua equipa {team} foi criada!',
  },
  team_updated: {
    id: 'app.containers.Registration.team_updated',
    defaultMessage: '{name} já faz parte da equipa {team}!',
  },
  team_joined_by_you: {
    id: 'app.containers.Registration.team_joined_by_you',
    defaultMessage: 'Já fazes parte da equipa {team}!',
  },
  good_luck: {
    id: 'app.containers.Registration.good_luck',
    defaultMessage: 'Boas corridas!',
  },
  [`paid_with_${CREDIT_CARD}`]: {
    id: 'app.containers.Registration.paid_with_creditcard',
    defaultMessage: genericPaymentSuccess,
  },
  [`paid_with_${PAYPAL}`]: {
    id: 'app.containers.Registration.paid_with_paypal',
    defaultMessage: genericPaymentSuccess,
  },
  [`paid_with_${MULTIBANCO}`]: {
    id: 'app.containers.Registration.paid_with_multibanco',
    defaultMessage:
      'Será enviado um email com os dados de pagamento, que caso não seja efetuado nas próximas 72H, todos os dados submetidos serão apagados.\n\nApós o pagamento efetuado, receberás um e-mail com a confirmação da tua inscrição e informações úteis acerca da prova.\n\nQualquer esclarecimento adicional podes entrar em contacto connosco através da nossa página de contactos.',
  },
  payment_failed: {
    id: 'app.containers.Registration.payment_failed',
    defaultMessage:
      'O pagamento falhou. Por favor verifica que introduziste todos os dados corretamente.',
  },
  or_pay_with_multibanco: {
    id: 'app.containers.Registration.or_pay_with_multibanco',
    defaultMessage:
      'Como alternativa, podes pagar com os dados de Multibanco abaixo.',
  },
  try_again: {
    id: 'app.containers.Registration.try_again',
    defaultMessage: 'Tentar outra vez',
  },
  [`short_${ID_NUMBER}`]: {
    id: 'app.containers.Registration.short_id_number',
    defaultMessage: 'BI',
  },
  [`short_${DOB}`]: {
    id: 'app.containers.Registration.short_dob',
    defaultMessage: 'Data de Nascimento',
  },
  [BILLING_NAME]: {
    id: 'app.containers.Registration.billing_name',
    defaultMessage: 'Nome',
  },
  [BILLING_ADDRESS]: {
    id: 'app.containers.Registration.billing_address',
    defaultMessage: 'Morada',
  },
  [BILLING_CITY]: {
    id: 'app.containers.Registration.billing_city',
    defaultMessage: 'Distrito',
  },
  [BILLING_POSTCODE]: {
    id: 'app.containers.Registration.billing_postcode',
    defaultMessage: 'Código postal',
  },
  [BILLING_TAX_NUMBER]: {
    id: 'app.containers.Registration.billing_tax_number',
    defaultMessage: 'NIF',
  },
  // ~~~~~ Screen: Get excel file ~~~~~
  create_team: {
    id: 'app.containers.Registration.create_team',
    defaultMessage: 'Criar Equipa',
  },
  download_file_help_info: {
    id: 'app.containers.Registration.download_file_help_info',
    defaultMessage: 'Descarrega este ficheiro para inscreveres a tua equipa.',
  },
  download_this_file: {
    id: 'app.containers.Registration.download_this_file',
    defaultMessage: 'Download de Folha de Excel',
  },
  upload_excel_file: {
    id: 'app.containers.Registration.upload_excel_file',
    defaultMessage: 'Upload de Folha de Excel',
  },
  continue_when_ready: {
    id: 'app.containers.Registration.continue_when_ready',
    defaultMessage:
      'Caso já tenhas o ficheiro preparado, avança e segue os passos para criares a tua equipa.',
  },
  follow_the_instructions: {
    id: 'app.containers.Registration.follow_the_instructions',
    defaultMessage:
      'Para um preenchimento correto, siga as instruções de preenchimento do excel.',
  },
  // ~~~~ Screen: Enter team name ~~~~
  team_name: {
    id: 'app.containers.Registration.team_name',
    defaultMessage: 'Nome da Equipa',
  },

  team_already_exists: {
    id: 'app.containers.Registration.team_already_exists',
    defaultMessage:
      'Este nome de equipa já existe!\n\nCaso não te queiras associar a esta equipa, altera o nome e cria outra equipa.',
  },
  team_does_not_exist: {
    id: 'app.containers.Registration.team_does_not_exist',
    defaultMessage: 'Esta equipa ainda não existe!',
  },

  join_team: {
    id: 'app.containers.Registration.join_team',
    defaultMessage: 'Associar a Equipa',
  },

  team_code: {
    id: 'app.containers.Registration.team_code',
    defaultMessage: 'Código da equipa',
  },

  // ~~~~~ Screen: Enter team leader ~~~~~
  team_leader: {
    id: 'app.containers.Registration.team_leader',
    defaultMessage: 'Responsável da Equipa',
  },
  missing_leader_info: {
    id: 'app.containers.Registration.missing_leader_info',
    defaultMessage:
      'Todos os campos são de preenchimento obrigatório.\n\n* Nome\n* E-mail\n* Número de telemóvel\n* Modo de levantamento dos kits',
  },

  kit_collection_method_short: {
    id: 'app.containers.Registration.kit_collection_method',
    defaultMessage: 'Levantamento de Kits',
  },

  kit_collection_method: {
    id: 'app.containers.Registration.kit_collection_method',
    defaultMessage: 'Modo de Levantamento dos kits',
  },
  group: {
    id: 'app.containers.Registration.group',
    defaultMessage: 'Grupo',
  },
  individual: {
    id: 'app.containers.Registration.individual',
    defaultMessage: 'individual',
  },
  [KIT_DELIVERY]: {
    id: `app.containers.Registration.${KIT_DELIVERY}`,
    defaultMessage: 'Modo de Levantamento dos kits',
  },
  [KIT_DELIVERY_OPTION_INDIVIDUAL]: {
    id: `app.containers.Registration.${KIT_DELIVERY_OPTION_INDIVIDUAL}`,
    defaultMessage: 'individual',
  },
  [KIT_DELIVERY_OPTION_GROUP]: {
    id: `app.containers.Registration.${KIT_DELIVERY_OPTION_GROUP}`,
    defaultMessage: 'grupo',
  },
  // helo info:
  two_ways_to_collect_kits: {
    id: 'app.containers.Registration.two_ways_to_collect_kits',
    defaultMessage:
      'O Levantamento dos kits de atleta será feito de 2 formas distintas:',
  },
  as_a_group: {
    id: 'app.containers.Registration.as_a_group',
    defaultMessage: 'EM GRUPO',
  },
  as_a_group_explanation: {
    id: 'app.containers.Registration.as_a_group_explanation',
    defaultMessage: 'O responsável da equipa levanta os kits de toda a equipa',
  },
  individual_collection: {
    id: 'app.containers.Registration.individual_collection',
    defaultMessage: 'INDIVIDUAL',
  },
  individual_collection_explanation: {
    id: 'app.containers.Registration.individual_collection_explanation',
    defaultMessage: 'Cada atleta levanta o seu kit',
  },
  team_registration: {
    id: 'app.containers.Registration.team_registration',
    defaultMessage: 'Inscrição da Equipa',
  },
  check_the_file: {
    id: 'app.containers.Registration.check_the_file',
    defaultMessage:
      'Antes de procederes ao upload certifica-te que o ficheiro tem o nome da equipa e a extensão corretas.',
  },
  example: {
    id: 'app.containers.Registration.example',
    defaultMessage: 'exemplo:',
  },
  file_example: {
    id: 'app.containers.Registration.file_example',
    defaultMessage: 'nomedaequipa.xlsx',
  },
  // ~~~~~ Screen: Details confirmation - group edition ~~~~~
  please_upload_evidence: {
    id: 'app.containers.Registration.please_upload_evidence',
    defaultMessage:
      'Por favor, faça o upload do comprovativo de caixa de partida',
  },
  please_upload_evidence_for: {
    id: 'app.containers.Registration.please_upload_evidence_for',
    defaultMessage:
      'Por favor, faça o upload do comprovativo de caixa de partida para: {name}',
  },
  please_change_shirt_size_for: {
    id: 'app.containers.Registration.please_change_shirt_size_for',
    defaultMessage:
      'Ao menos um tamanho de t-shirt selecionado não está disponível. Por favor, altera os dados para: {name}',
  },
  adults_race_is_for_adults: {
    id: 'app.containers.Registration.adults_race_is_for_adults',
    defaultMessage:
      'A idade mínima de participação na Oeiras run é de 18 anos',
  },
  something_went_wrong: {
    id: 'app.containers.Registration.something_went_wrong',
    defaultMessage:
      'Correu um erro inesperado. Por favor, contacta ajuda@lastlap.pt\n\n{error}',
  },
  please_check_the_excel_file: {
    id: 'app.containers.Registration.please_check_the_excel_file',
    defaultMessage:
      'Por favor, verifica os dados na folha de excel.\n\n{fields}',
  },
  // ~~~~~ Opt-ins ~~~~~
  [AGREE_TO_TERMS]: {
    id: 'app.containers.Registration.agreed_to_terms',
    defaultMessage:
      'Li e aceito as condições descritas <a href="http://www.marginalanoite.pt/informacoes/regulamento" target="_blank" rel="noopener noreferrer">no regulamento</a>',
  },
  [AGREE_TO_PRIVACY_POLICY]: {
    id: 'app.containers.Registration.marketing_opt_in',
    defaultMessage:
      'Li e aceito a <a href="/Politica_de_Privacidade_Last_Lap.pdf" download="">Politica de privacidade e proteção de dados</a> do evento indicada <a href="http://www.marginalanoite.pt/informacoes/regulamento" target="_blank" rel="noopener noreferrer">no regulamento</a>',
  },
  communication_agreements_for_this_event: {
    id: 'app.containers.Registration.communication_agreements_for_this_event',
    defaultMessage: 'Comunicação sobre este evento',
  },
  [ACCEPT_EMAIL_CONTACT]: {
    id: 'app.containers.Registration.accept_email_contact',
    defaultMessage:
      'Aceito que o meu <strong>contacto de email</strong> seja utilizado pela organização Câmara Municipal de Oeiras para enviar informação sobre o evento em que me inscrevi',
  },
  [ACCEPT_PHONE_CONTACT]: {
    id: 'app.containers.Registration.accept_phone_contact',
    defaultMessage:
      'Aceito que o meu <strong>contacto telefónico</strong> seja utilizado pela organização Câmara Municipal de Oeiras para enviar informação sobre o evento em que me inscrevi',
  },
  communication_agreements_for_other_events: {
    id: 'app.containers.Registration.communication_agreements_for_other_events',
    defaultMessage:
      'Comunicação sobre outros eventos desportivos da Câmara Municipal de Oeiras',
  },
  [ACCEPT_EMAIL_CONTACT_OTHER_EVENTS]: {
    id: 'app.containers.Registration.accept_email_contact_other_events',
    defaultMessage:
      'Aceito que o meu <strong>contacto de email</strong> seja utilizado pela Câmara Municipal de Oeiras para enviar informação sobre outros eventos desportivos organizados pelo Município',
  },
  personal_data_collection: {
    id: 'app.containers.Registration.personal_data_collection',
    defaultMessage: 'Recolha de dados pessoais',
  },
  personal_data_collection_desc: {
    id: 'app.containers.Registration.personal_data_collection_desc',
    defaultMessage:
      'Os dados pessoais recolhidos em função do presente evento serão armazenados pelo prazo de dois anos e estão acessíveis de forma gratuita para consulta, retificação ou eliminação através de <strong><a href="mailto:gestao.dados@lastlap.pt">gestao.dados@lastlap.pt</a></strong>',
  },
  view_privacy_policy_here: {
    id: 'app.containers.Registration.view_privacy_policy_here',
    defaultMessage:
      'Consulta a nossa <strong><a href="https://www.corridadotejo.com/Politica_de_Privacidade_Last_Lap.pdf" target="_blank" rel="noopener">Política de Privacidade</a></strong>',
  },
  [`missing_${AGREE_TO_TERMS}`]: {
    id: 'app.containers.Registration.missing_agreed_to_terms',
    defaultMessage: 'As condições descritas no regulamento',
  },
  [`missing_${AGREE_TO_PRIVACY_POLICY}`]: {
    id: 'app.containers.Registration.missing_marketing_opt_in',
    defaultMessage:
      'A Politica de privacidade e proteção de dados do evento indicada no regulamento',
  },
  duplicate_ids_error: {
    id: 'app.containers.Registration.duplicate_ids_error',
    defaultMessage: 'Existem números de BI/CC duplicados.',
  },
  // ============ Telling users to check their email ============
  check_your_email: {
    id: 'app.containers.Registration.check_your_email',
    defaultMessage:
      'Foi enviado um email para a tua caixa de correio com um link que te permitirá {toDoWhat}.\n\nQualquer esclarecimento adicional podes entrar em contacto connosco através da nossa página de contactos.\n\nBoas Corridas!',
  },
  to_edit_your_details: {
    id: 'app.containers.Registration.to_edit_your_details',
    defaultMessage: 'alterar os teus dados de inscrição',
  },
  to_join_the_team: {
    id: 'app.containers.Registration.to_join_the_team',
    defaultMessage: 'associar à equipa',
  },
  confirm_you_have_asked_others_about_contact_preferences: {
    id:
      'app.containers.Registration.confirm_you_have_asked_others_about_contact_preferences',
    defaultMessage:
      'Ao avançar, confirmas ter perguntado a todos os elementos que estás a inscrever, quais as suas preferências relativamente às diferentes opções sobre o tratamento dos dados pessoais.',
  },
  confirm_others_accept_terms_and_privacy_policy: {
    id:
      'app.containers.Registration.confirm_others_accept_terms_and_privacy_policy',
    defaultMessage:
      'Ao avançar, confirmas que todos os elementos que estás a inscrever aceitam o regulamento da prova bem como a politica de privacidade.',
  },
  promo_code_input: {
    id: 'app.containers.Registration.promo_code_input',
    defaultMessage: 'Código Promocional',
  },
  currently_available_shirt_sizes: {
    id: 'app.containers.Registration.currently_available_shirt_sizes',
    defaultMessage: 'T-shirts disponíveis',
  },
  shirt_size_not_available: {
    id: 'app.containers.Registration.shirt_size_not_available',
    defaultMessage: 'O tamanho selecionado não está disponível',
  },
  only_60_plus_from_now_on: {
    id: 'app.containers.Registration.only_60_plus_from_now_on',
    defaultMessage: 'A única caixa de partida disponível é >60',
  },
  [CHIP]: {
    id: `app.containers.Registration.${CHIP}`,
    defaultMessage: 'Chip',
  },
  with_chip: {
    id: 'app.containers.Registration.with_chip',
    defaultMessage: 'Com chip',
  },
  without_chip: {
    id: 'app.containers.Registration.without_chip',
    defaultMessage: 'Sem chip',
  },
  chip: {
    id: 'app.containers.Registration.chip',
    defaultMessage: 'Com chip',
  },
  nochip: {
    id: 'app.containers.Registration.nochip',
    defaultMessage: 'Sem chip',
  },

  // Errors
  'enrollments.enrollmentnotfound': {
    id: 'enrollments.enrollmentnotfound',
    defaultMessage: 'Inscrição não encontrada',
  },

  'enrollments.paymentnotfound': {
    id: 'enrollments.paymentnotfound',
    defaultMessage: 'Pagamento não encontrado',
  },

  'enrollments.unreferencedpayment': {
    id: 'enrollments.unreferencedpayment',
    defaultMessage: 'Este pagamento não é ligado á uma inscrição',
  },

  'enrollments.teamnotfound': {
    id: 'enrollments.teamnotfound',
    defaultMessage: 'Equipa não encontrada',
  },

  'validation.requiredfield': {
    id: 'validation.requiredfield',
    defaultMessage: 'A informação seguinte está em falta: {field}',
  },

  'validation.invalidfield': {
    id: 'validation.invalidfield',
    defaultMessage: 'A informação seguinte não é valida: {field}',
  },

  'enrollments.shirtsizenotavailable': {
    id: 'enrollments.shirtsizenotavailable',
    defaultMessage: 'O tamanho selecionado não está disponível',
  },

  'enrollments.nostartinglineproof': {
    id: 'enrollments.nostartinglineproof',
    defaultMessage:
      'Por favor, faça o upload do comprovativo de caixa de partida',
  },

  'validation.arrayempty': {
    id: 'validation.arrayempty',
    defaultMessage: 'A folha deve incluir pelo menos os dados de uma pessoa',
  },

  'enrollments.kidsnotallowedinteams': {
    id: 'enrollments.kidsnotallowedinteams',
    defaultMessage: 'As crianças não podem fazer parte de uma equipa',
  },

  'enrollments.invaliddiscountcode': {
    id: 'enrollments.invaliddiscountcode',
    defaultMessage: 'O código promocional não é válido',
  },

  'enrollments.duplicateenrollment': {
    id: 'enrollments.duplicateenrollment',
    defaultMessage: 'Já alguém se registou com este BI/CC',
  },

  'enrollments.invalidstartbox': {
    id: 'enrollments.invalidstartbox',
    defaultMessage: 'A caixa de partida não é válida',
  },
});
