import { fromJS } from 'immutable';
import {
  // RESET_PRICE_STATE,
  // GET_PRICES_REQUESTING,
  // GET_PRICES_FAILED,
  // GET_PRICES_SUCCESSFUL,
  RESET_SHIRT_STATE,
  GET_SHIRTS_REQUESTING,
  GET_SHIRTS_FAILED,
  GET_SHIRTS_SUCCESSFUL,
  POST_REGISTRATION_REQUESTING,
  POST_REGISTRATION_FAILED,
  POST_REGISTRATION_SUCCESSFUL,
  GET_PAYMENT_STATUS_REQUESTING,
  GET_PAYMENT_STATUS_FAILED,
  GET_PAYMENT_STATUS_SUCCESSFUL,
  CREATE_RUNNER_DETAILS,
  UPDATE_RUNNER_DETAILS,
  RESET_REGISTRATION_STATE,
  RESET_PAYMENT_STATUS_STATE,
  GET_ID_CHECK_REQUESTING,
  GET_ID_CHECK_FAILED,
  GET_ID_CHECK_SUCCESSFUL,
  RESET_ID_CHECK_STATE,
  ADD_NEW_RUNNER,
  DELETE_RUNNER,
  SET_CURRENT_RUNNER_INDEX,
  REGISTRATION_TYPE,
  GET_REG_DETAILS_REQUESTING,
  GET_REG_DETAILS_FAILED,
  GET_REG_DETAILS_SUCCESSFUL,
  UPDATE_REG_DETAILS,
  POST_UPDATE_REQUESTING,
  POST_UPDATE_FAILED,
  POST_UPDATE_SUCCESSFUL,
  RESET_REG_EDIT_STATE,
  RESET_UPDATE_POST_STATE,
  RESET_EDIT_REG_LINK_REQUEST_STATE,
  GET_EDIT_REG_LINK_REQUESTING,
  GET_EDIT_REG_LINK_FAILED,
  GET_EDIT_REG_LINK_SUCCESSFUL,
  SET_GDPR_FOR_OTHERS,
  SET_PROMO_CODE,
  RESET_PROMO_CODE_STATE,
  APPLY_PROMO_CODE_REQUESTING,
  APPLY_PROMO_CODE_FAILED,
  APPLY_PROMO_CODE_SUCCESSFUL,
  RESET_ALL_STATE,
} from './constants';
import { newRunner, defaultEditObject } from './utils';

const setState = (requesting = false, error = null, data = null) => ({
  requesting,
  error,
  data,
});

function addRunnerToState(state) {
  const runnerArray = state.get('runnerDetails').data;
  const type = runnerArray[0][REGISTRATION_TYPE];
  const newRunnerArray = [...runnerArray, newRunner(type)];

  return state
    .set('currentRunnerIndex', newRunnerArray.length - 1)
    .set('runnerDetails', setState(false, null, newRunnerArray));
}

function deleteRunnerFromState(state, index) {
  const runnerArray = state.get('runnerDetails').data;
  runnerArray.splice(index, 1);
  return state
    .set('runnerDetails', setState(false, null, [...runnerArray]))
    .set('currentRunnerIndex', 0);
}

function updateRunnerState(requesting = false, error = null, data, state) {
  const index = state.get('currentRunnerIndex');
  const runnerArray = state.get('runnerDetails').data;
  runnerArray[index] = data;

  return state.set('runnerDetails', {
    requesting,
    error,
    data: [...runnerArray],
  });
}

const initialState = fromJS({
  shirtSizes: setState(),
  runnerDetails: setState(false, null, []),
  currentRunnerIndex: 0,
  registrationPost: setState(),
  paymentStatus: setState(),
  checkIDState: setState(),
  editRegistration: setState(false, null, defaultEditObject()),
  updatedRegistrationPost: setState(),
  editLinkRequest: setState(),
  hasAskedOthersAboutGDPR: false,
  promoCode: '',
  appliedPromoCode: setState(),
});

function registrationReducer(state = initialState, { type, payload }) {
  switch (type) {
    // case GET_PRICES_REQUESTING:
    //   return state.set('currentPrice', setState(true));
    // case GET_PRICES_FAILED:
    //   return state.set('currentPrice', setState(false, payload));
    // case GET_PRICES_SUCCESSFUL:
    //   return state.set('currentPrice', setState(false, null, payload));
    // case RESET_PRICE_STATE:
    //   return state.set('currentPrice', setState());

    case GET_SHIRTS_REQUESTING:
      return state.set('shirtSizes', setState(true));
    case GET_SHIRTS_FAILED:
      return state.set('shirtSizes', setState(false, payload));
    case GET_SHIRTS_SUCCESSFUL:
      return state.set('shirtSizes', setState(false, null, payload));
    case RESET_SHIRT_STATE:
      return state.set('shirtSizes', setState());

    case GET_ID_CHECK_REQUESTING:
      return state.set('checkIDState', setState(true));
    case GET_ID_CHECK_FAILED:
      return state.set('checkIDState', setState(false, payload));
    case GET_ID_CHECK_SUCCESSFUL:
      return state.set('checkIDState', setState(false, null, payload));
    case RESET_ID_CHECK_STATE:
      return state.set('checkIDState', setState());

    case POST_REGISTRATION_REQUESTING:
      return state.set('registrationPost', setState(true));
    case POST_REGISTRATION_FAILED:
      return state.set('registrationPost', setState(false, payload));
    case POST_REGISTRATION_SUCCESSFUL:
      return state.set('registrationPost', setState(false, null, payload));
    case RESET_REGISTRATION_STATE:
      return state.set('registrationPost', setState());

    case GET_PAYMENT_STATUS_REQUESTING:
      return state.set('paymentStatus', setState(true));
    case GET_PAYMENT_STATUS_FAILED:
      return state.set('paymentStatus', setState(false, payload));
    case GET_PAYMENT_STATUS_SUCCESSFUL:
      return state.set('paymentStatus', setState(false, null, payload));
    case RESET_PAYMENT_STATUS_STATE:
      return state.set('paymentStatus', setState());

    case CREATE_RUNNER_DETAILS:
      return state.set(
        'runnerDetails',
        setState(false, null, [newRunner(payload)])
      );
    case UPDATE_RUNNER_DETAILS:
      return updateRunnerState(false, null, payload, state);
    case ADD_NEW_RUNNER:
      return addRunnerToState(state);
    case DELETE_RUNNER:
      return deleteRunnerFromState(state, payload);
    case SET_CURRENT_RUNNER_INDEX:
      return state.set('currentRunnerIndex', payload);

    case RESET_EDIT_REG_LINK_REQUEST_STATE:
      return state.set('editLinkRequest', setState());
    case GET_EDIT_REG_LINK_REQUESTING:
      return state.set('editLinkRequest', setState(true));
    case GET_EDIT_REG_LINK_FAILED:
      return state.set('editLinkRequest', setState(false, payload));
    case GET_EDIT_REG_LINK_SUCCESSFUL:
      return state.set('editLinkRequest', setState(false, null, payload));

    case RESET_REG_EDIT_STATE:
      return state.set(
        'editRegistration',
        setState(false, null, defaultEditObject())
      );
    case GET_REG_DETAILS_REQUESTING:
      return state.set('editRegistration', setState(true, null, payload));
    case GET_REG_DETAILS_FAILED:
      return state.set(
        'editRegistration',
        setState(false, payload.message, payload.data)
      );
    case GET_REG_DETAILS_SUCCESSFUL:
    case UPDATE_REG_DETAILS:
      return state.set('editRegistration', setState(false, null, payload));

    case RESET_UPDATE_POST_STATE:
      return state.set('updatedRegistrationPost', setState());
    case POST_UPDATE_REQUESTING:
      return state.set('updatedRegistrationPost', setState(true));
    case POST_UPDATE_FAILED:
      return state.set('updatedRegistrationPost', setState(false, payload));
    case POST_UPDATE_SUCCESSFUL:
      return state.set(
        'updatedRegistrationPost',
        setState(false, null, payload)
      );

    case SET_GDPR_FOR_OTHERS:
      return state.set('hasAskedOthersAboutGDPR', payload || false);

    case SET_PROMO_CODE:
      return state.set('promoCode', payload);

    case RESET_PROMO_CODE_STATE:
      return state.set('appliedPromoCode', setState());
    case APPLY_PROMO_CODE_REQUESTING:
      return state.set('appliedPromoCode', setState(true));
    case APPLY_PROMO_CODE_FAILED:
      return state.set('appliedPromoCode', setState(false, payload));
    case APPLY_PROMO_CODE_SUCCESSFUL:
      return state.set('appliedPromoCode', setState(false, null, payload));

    case RESET_ALL_STATE:
      return state
        .set(`currentRunnerIndex`, 0)
        .set(`runnerDetails`, setState(false, null, []))
        .set(`registrationPost`, setState())
        .set(`paymentStatus`, setState())
        .set(`checkIDState`, setState())
        .set(`editRegistration`, setState(false, null, defaultEditObject()))
        .set(`updatedRegistrationPost`, setState())
        .set(`editLinkRequest`, setState())
        .set(`hasAskedOthersAboutGDPR`, false)
        .set(`promoCode`, '')
        .set(`appliedPromoCode`, setState());

    default:
      return state;
  }
}

export default registrationReducer;
