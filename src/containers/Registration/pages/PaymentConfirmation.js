import _ from 'lodash';
import React, { Component } from 'react';
import { func, object } from 'prop-types';
import { scrollToTop } from 'utils/functions';
import { BigButton } from 'components';
import {
  FullWidthDiv,
  BoldCaps,
  OneColumnGrid,
  TextCenter,
  MultibancoDiv,
} from 'components/styled';
import { FULL_NAME, PAYMENT_METHOD, MULTIBANCO } from '../constants';

class PaymentConfirmation extends Component {
  static propTypes = {
    formatMsg: func.isRequired,
    // getPrices: func.isRequired,
    getShirts: func.isRequired,
    resetPromoCodeState: func.isRequired,
    resetAllState: func.isRequired,
    history: object.isRequired,
    runner: object.isRequired,
    postingState: object.isRequired,
  };

  componentWillMount() {
    scrollToTop();
    const { history, resetPromoCodeState, resetAllState } = this.props;

    if (!this.shouldRender()) {
      resetAllState();
      history.push('/');
    }

    resetPromoCodeState();
  }

  componentWillUnmount() {
    const {
      // getPrices,
      getShirts,
      resetAllState,
    } = this.props;

    resetAllState();
    // getPrices();
    getShirts();
  }

  shouldRender() {
    try {
      const {
        runner,
        postingState: {
          data: { payment_amount: amount },
        },
      } = this.props;
      return runner[PAYMENT_METHOD] && _.isNumber(amount);
    } catch (e) {
      return false;
    }
  }

  handleContinue = () => {
    this.props.history.push('/');
  };

  renderConfirmationMsgByType() {
    const {
      formatMsg,
      runner,
      postingState: {
        data: { mb_entity: entity, mb_reference: ref, payment_amount: amount },
      },
    } = this.props;
    const wasFree = amount === 0;
    const paymentMethod = runner[PAYMENT_METHOD];
    const msgId = `paid_with_${wasFree ? 'creditcard' : paymentMethod}`;
    const paymentConfirmationMsg = formatMsg(msgId);

    if (paymentMethod !== MULTIBANCO || wasFree) {
      return <TextCenter>{paymentConfirmationMsg}</TextCenter>;
    }

    return (
      <OneColumnGrid>
        <MultibancoDiv>
          <h3>{formatMsg('multibanco_payment_details')}</h3>
          <div>
            <div>{formatMsg('entity')}:</div>
            <div>{entity}</div>
          </div>

          <div>
            <div>{formatMsg('reference')}:</div>
            <div>{ref}</div>
          </div>

          <div>
            <div>{formatMsg('amount')}:</div>
            <div>{amount}€</div>
          </div>
        </MultibancoDiv>
        <TextCenter>{paymentConfirmationMsg}</TextCenter>
      </OneColumnGrid>
    );
  }

  renderTeamNameMsg() {
    const {
      formatMsg,
      runner: { team_name: team },
    } = this.props;

    if (!team) return null;
    return <div>{formatMsg('team_created', { team })}</div>;
  }

  render() {
    if (!this.shouldRender()) return null;
    const { formatMsg, runner } = this.props;
    const name = runner[FULL_NAME];

    return (
      <FullWidthDiv>
        <h1>{formatMsg('payment')}</h1>
        <BoldCaps style={{ margin: 20 }}>
          {formatMsg('congratulations', { name })}
        </BoldCaps>

        {this.renderTeamNameMsg()}

        {this.renderConfirmationMsgByType()}

        <BoldCaps style={{ marginTop: 20, marginBottom: 40 }}>
          {formatMsg('good_luck')}
        </BoldCaps>

        <BigButton onClick={this.handleContinue}>
          {formatMsg('go_back')}
        </BigButton>
      </FullWidthDiv>
    );
  }
}

export default PaymentConfirmation;
