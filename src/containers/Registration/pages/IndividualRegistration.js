import React, { Component } from 'react';
import { array, func, object } from 'prop-types';
import RunnerForm from 'components/RunnerForm';

class IndividualRegistration extends Component {
  componentWillMount() {
    const {
      history: { push },
      runner,
      allRunners,
    } = this.props;
    if (!allRunners || !allRunners.length || !runner) push('/');
  }

  render() {
    const {
      formatMsg,
      runner,
      allRunners,
      history: { push },
    } = this.props;
    if (!allRunners || !runner) return null;

    return (
      <RunnerForm
        title={formatMsg('individual_registration_title')}
        goBack={() => push('/')}
        {...this.props}
      />
    );
  }
}

IndividualRegistration.propTypes = {
  allRunners: array.isRequired,
  formatMsg: func.isRequired,
  history: object.isRequired,
  runner: object.isRequired,
};

export default IndividualRegistration;
