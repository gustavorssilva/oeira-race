import _ from 'lodash';
import React, { Component } from 'react';
import { func, object } from 'prop-types';
import {
  BackButton,
  BigButton,
  DatePicker,
  FormField,
  Modal,
  CheckYourEmail,
} from 'components';
import {
  FullWidthDiv,
  TextCenter,
  CenteredBoldText,
  OneColumnGrid,
  MarginTop,
} from 'components/styled';
import {
  DOB,
  FULL_NAME,
  ID_NUMBER,
  ERROR_EDIT_REQUEST_RUNNER_NOT_FOUND,
} from '../constants';

class RequestEditLink extends Component {
  state = { modalContent: '', emailSent: false };

  componentWillMount() {
    const { updateRegistration, resetEditRegLinkRequestState } = this.props;
    updateRegistration({});
    resetEditRegLinkRequestState();
  }

  componentWillReceiveProps(props) {
    const {
      formatMsg,
      editLinkRequest: { data, error },
      editRegistration: { data: runner },
      resetEditRegLinkRequestState,
    } = props;
    const { modalContent } = this.state;

    if (data) {
      this.setState({ emailSent: true, modalContent: '' });
      return;
    }

    if (!error || modalContent) return;
    resetEditRegLinkRequestState();

    if (error !== ERROR_EDIT_REQUEST_RUNNER_NOT_FOUND) {
      this.setModal(error);
      return;
    }

    const notFoundError = formatMsg('runner_not_found', {
      id: runner[ID_NUMBER],
      dob: new Date(runner[DOB]).toLocaleDateString(),
    });
    this.setModal(notFoundError);
  }

  setModal = (modalContent) => this.setState({ modalContent });

  handleContinue = () => {
    const {
      editRegistration: { data: runner },
      requestEditLink,
    } = this.props;

    return requestEditLink(runner);
  };

  handleChange(field, value) {
    const {
      updateRegistration,
      editRegistration: { data },
    } = this.props;

    updateRegistration({ ..._.pick(data, [ID_NUMBER, DOB]), [field]: value });
  }

  renderError() {
    const { formatMsg } = this.props;
    const { modalContent } = this.state;
    if (!modalContent) return null;

    const text = modalContent.message
      ? formatMsg(modalContent.message)
      : modalContent;
    return <Modal onClick={() => this.setModal('')}>{text}</Modal>;
  }

  renderContinueButton() {
    const {
      formatMsg,
      editRegistration: { data: runner },
    } = this.props;

    // If a runner was found, the name should be in the object
    const name = runner[FULL_NAME];
    const disabled = !runner[ID_NUMBER] || !runner[DOB];

    if (!name) {
      return (
        <BigButton disabled={disabled} onClick={this.handleContinue}>
          {formatMsg('continue')}
        </BigButton>
      );
    }

    return (
      <FullWidthDiv>
        <CenteredBoldText>{formatMsg('hello', { name })}</CenteredBoldText>
        <TextCenter style={{ margin: 20 }}>
          {formatMsg('continue_to_edit')}
        </TextCenter>
        <BigButton onClick={this.handleContinue}>
          {formatMsg('continue')}
        </BigButton>
      </FullWidthDiv>
    );
  }

  renderCheckYourEmail() {
    const { formatMsg } = this.props;

    return (
      <CheckYourEmail
        title={formatMsg('edit_registration_title')}
        toDoWhat={formatMsg('to_edit_your_details')}
      />
    );
  }

  render() {
    const {
      formatMsg,
      editRegistration: { data: runner },
      history: { push },
    } = this.props;

    if (!runner) return null;
    if (this.state.emailSent) return this.renderCheckYourEmail();

    return (
      <FullWidthDiv>
        <BackButton onClick={() => push('/')} />

        <h1>{formatMsg('edit_registration_title')}</h1>

        <OneColumnGrid>
          <FormField
            placeholder={formatMsg(ID_NUMBER)}
            value={runner[ID_NUMBER] || ''}
            onChange={(e) => this.handleChange(ID_NUMBER, e.target.value)}
          />

          <DatePicker
            placeholder={formatMsg(`short_${DOB}`)}
            date={runner[DOB]}
            onChange={(value) => this.handleChange(DOB, value)}
          />
        </OneColumnGrid>

        <MarginTop>
          {this.renderContinueButton()}
          {this.renderError()}
        </MarginTop>
      </FullWidthDiv>
    );
  }
}

RequestEditLink.propTypes = {
  editRegistration: object.isRequired,
  history: object.isRequired,
  editLinkRequest: object.isRequired,
  formatMsg: func.isRequired,
  requestEditLink: func.isRequired,
  resetEditRegLinkRequestState: func.isRequired,
  updateRegistration: func.isRequired,
};

export default RequestEditLink;
