import _ from 'lodash';
import React, { Component } from 'react';
import { func, object } from 'prop-types';
import { Modal } from 'components';
import RunnerForm from 'components/RunnerForm';
import { FullWidthDiv } from 'components/styled';
import { EDIT_REGISTRATION_URL } from '../constants';
import { applyRequiredChanges, validateRegistrationEdit } from '../utils';
import { resetEditRegistrationState } from '../actions';

class EditRegistration extends Component {
  state = {
    modalContent: null,
  };

  componentWillMount() {
    const {
      location: { pathname },
      getRegistration,
      resetUpdatePostState,
    } = this.props;

    const id = pathname.replace(EDIT_REGISTRATION_URL, '');
    this.setState({ id });
    resetUpdatePostState();
    getRegistration(id);
  }

  componentWillReceiveProps(nextProps, { modalContent }) {
    const {
      formatMsg,
      resetUpdatePostState,
      editRegistration: { error: getDetailsError },
      updatedRegistrationPostState: { data: successfulPost, error },
    } = nextProps;
    if ((!successfulPost && !error && !getDetailsError) || modalContent) return;

    if (successfulPost || error) resetUpdatePostState();
    if (getDetailsError) resetEditRegistrationState();

    this.setModal(
      getDetailsError || error || formatMsg('registration_updated')
    );
  }

  getShirtSizes(runner) {
    const { shirtSizesState } = this.props;
    if (!_.get(runner, 'gender') || !_.get(shirtSizesState, 'data')) {
      return [];
    }
    const sizeData = _.find(shirtSizesState.data, {
      type: runner.type,
      gender: runner.gender,
    });
    return _.get(sizeData, 'sizes') || [];
  }

  setModal = (modalContent) => this.setState({ modalContent });

  shouldRender() {
    const { data: runner } = this.props.editRegistration;
    return runner && runner.type;
  }

  handleContinue = () => {
    const {
      editRegistration: { data },
      postRegUpdate,
    } = this.props;

    const errors = this.findErrors();
    return errors
      ? this.setModal(errors)
      : postRegUpdate({ id: this.state.id, data });
  };

  handleChange = (newData) => {
    const {
      editRegistration: { data },
      updateRegistration,
    } = this.props;

    applyRequiredChanges(data, newData);
    updateRegistration({ ...data, ...newData });
  };

  findErrors = () => {
    const {
      formatMsg,
      editRegistration: { data: runner },
    } = this.props;
    const errors = validateRegistrationEdit(runner, this.getShirtSizes(runner));
    if (!errors) return false;
    return _.map(_.castArray(errors), formatMsg).join('\n');
  };

  goBack = () => this.props.history.push('/');

  renderModal() {
    const { formatMsg } = this.props;
    const { modalContent } = this.state;
    if (!modalContent) return null;

    const text = modalContent.message
      ? formatMsg(modalContent.message)
      : modalContent;
    return <Modal onClick={() => this.setModal('')}>{text}</Modal>;
  }

  render() {
    // Render modal in case there's an error message to show
    if (!this.shouldRender()) return this.renderModal();

    const {
      formatMsg,
      formatHTMLMsg,
      history,
      editRegistration: { data: runner },
      shirtSizesState,
    } = this.props;

    return (
      <FullWidthDiv>
        <RunnerForm
          editingExistingRegistration
          formatMsg={formatMsg}
          formatHTMLMsg={formatHTMLMsg}
          goBack={this.goBack}
          history={history}
          updateRunner={this.handleChange}
          findErrors={this.findErrors}
          runner={runner}
          title={formatMsg('edit_registration_title')}
          handleContinue={this.handleContinue}
          shirtSizesState={shirtSizesState}
        />
        {this.renderModal()}
      </FullWidthDiv>
    );
  }
}

EditRegistration.propTypes = {
  editRegistration: object.isRequired,
  shirtSizesState: object.isRequired,
  formatMsg: func.isRequired,
  formatHTMLMsg: func.isRequired,
  history: object.isRequired,
  location: object.isRequired,
  postRegUpdate: func.isRequired,
  getRegistration: func.isRequired,
  resetUpdatePostState: func.isRequired,
  updateRegistration: func.isRequired,
  updatedRegistrationPostState: object.isRequired,
};

export default EditRegistration;
