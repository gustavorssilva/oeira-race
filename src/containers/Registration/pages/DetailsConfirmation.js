import _ from 'lodash';
import React, { Component } from 'react';
import { array, bool, func, number, object, string } from 'prop-types';
import { CheckIcon } from 'mdi-react';
import { scrollToTop } from 'utils/functions';
import { CloseIcon, DeleteIcon } from 'assets/icons';
import DetailsConfirmationPage from 'components/DetailsConfirmationPage';
import {
  FullWidthDiv,
  SmallButton,
  SmallRedButton,
  TwoButtonDiv,
} from 'components/styled';
import {
  FULL_NAME,
  GENDER,
  MALE,
  CREDIT_CARD,
  PAYMENT_METHOD,
  REGISTRATION_FORM_URL,
  ADD_RUNNER_URL,
  EDIT_RUNNER_URL,
} from '../constants';
import {
  hasAllInvoiceInfo,
  hasNoInvoiceInfo,
  containsDuplicateIDs,
} from '../utils';

class DetailsConfirmation extends Component {
  static propTypes = {
    addRunner: func.isRequired,
    currentRunnerIndex: number.isRequired,
    formatMsg: func.isRequired,
    postRegistration: func.isRequired,
    setRunnerIndex: func.isRequired,
    deleteRunner: func.isRequired,
    updateRunner: func.isRequired,
    applyPromoCode: func.isRequired,
    findErrors: func.isRequired,
    allRunners: array.isRequired,
    promoCode: string,
    appliedPromoCode: object.isRequired,
    runner: object.isRequired,
    history: object.isRequired,
    hasAskedOthersAboutGDPR: func.isRequired,
    hasUserConfirmedGDPRForOthers: bool.isRequired,
  };

  state = {
    showDetails: null,
    errorMsg: '',
    selectedPayment: null,
  };

  componentWillMount() {
    const {
      history: { push },
      currentRunnerIndex,
      setRunnerIndex,
      allRunners,
      runner,
      updateRunner,
      promoCode,
      applyPromoCode,
      appliedPromoCode,
    } = this.props;
    if (!this.shouldRender()) return push('/');

    if (currentRunnerIndex !== 0) {
      setRunnerIndex(0);
    } else if (!runner.discount_code && promoCode) {
      updateRunner({ discount_code: promoCode });
    }

    if (promoCode && !appliedPromoCode.data) {
      applyPromoCode({ promoCode, registrations: allRunners });
    }

    return scrollToTop();
  }

  componentWillReceiveProps(nextProps) {
    this.checkIfFree(nextProps);
  }

  setErrorMsg = (errorMsg) => this.setState({ errorMsg });

  setDeleteConfirmation = (runner) => {
    const { formatMsg } = this.props;
    const article = runner[GENDER] === MALE ? 'o' : 'a';
    const name = `${article} ${runner[FULL_NAME]}`;
    const modalContent = (
      <FullWidthDiv>
        <div>{formatMsg('confirm_delete', { name })}</div>
        <TwoButtonDiv>
          <SmallButton onClick={() => this.setErrorMsg('')}>
            <img src={CloseIcon} alt="" />
            <span>{formatMsg('cancel')}</span>
          </SmallButton>
          <SmallRedButton onClick={() => this.handleDeleteRunner(runner)}>
            <img src={DeleteIcon} alt="" />
            <span>{formatMsg('delete')}</span>
          </SmallRedButton>
        </TwoButtonDiv>
      </FullWidthDiv>
    );
    this.setState({ errorMsg: modalContent });
  };

  setGDPRWarningForOthers() {
    const { formatMsg } = this.props;
    const modalContent = (
      <FullWidthDiv>
        <div>
          {formatMsg('confirm_you_have_asked_others_about_contact_preferences')}
        </div>
        <TwoButtonDiv>
          <SmallButton onClick={() => this.setErrorMsg('')}>
            <img src={CloseIcon} alt="" />
            <span>{formatMsg('cancel')}</span>
          </SmallButton>
          <SmallButton onClick={this.handleAcceptGDPRForOthers}>
            <CheckIcon size={50} style={{ padding: 5 }} />
            <span>{formatMsg('continue')}</span>
          </SmallButton>
        </TwoButtonDiv>
      </FullWidthDiv>
    );
    this.setState({ errorMsg: modalContent });
  }

  checkIfFree(props) {
    const { appliedPromoCode, updateRunnerDetails, runner } = props;

    if (!this.isOrderFree(appliedPromoCode)) return;

    if (!runner[PAYMENT_METHOD]) {
      const newDetails = { ...runner, [PAYMENT_METHOD]: CREDIT_CARD };
      this.setState({ selectedPayment: 'visa' });
      updateRunnerDetails(newDetails);
    }
  }

  isOrderFree(appliedPromoCode) {
    const dataToCheck = appliedPromoCode || this.props.appliedPromoCode;
    const totalPriceWithPromoCode = _.get(dataToCheck, 'data.total_price');
    return totalPriceWithPromoCode === 0;
  }

  shouldRender() {
    const { findErrors, runner } = this.props;
    return !findErrors(runner);
  }

  handleAddRunner = () => {
    const {
      addRunner,
      hasUserConfirmedGDPRForOthers,
      history: { push },
    } = this.props;

    if (!hasUserConfirmedGDPRForOthers) return this.setGDPRWarningForOthers();

    addRunner();
    return push(ADD_RUNNER_URL);
  };

  handleAcceptGDPRForOthers = () => {
    const {
      addRunner,
      hasAskedOthersAboutGDPR,
      history: { push },
    } = this.props;

    hasAskedOthersAboutGDPR(true);
    addRunner();
    push(ADD_RUNNER_URL);
  };

  handleContinue = () => {
    const {
      formatMsg,
      postRegistration,
      runner,
      allRunners,
      appliedPromoCode,
    } = this.props;

    if (!hasAllInvoiceInfo(runner) && !hasNoInvoiceInfo(runner)) {
      return this.setErrorMsg(formatMsg('check_invoice_fields'));
    }
    if (containsDuplicateIDs(allRunners)) {
      return this.setErrorMsg(formatMsg('duplicate_ids_error'));
    }

    if (!appliedPromoCode.data) {
      allRunners[0].discount_code = undefined;
    }

    return postRegistration(allRunners);
  };

  handleDeleteRunner = (runner) => {
    const { allRunners, deleteRunner } = this.props;
    const index = _.indexOf(allRunners, runner);
    deleteRunner(index);
    this.setErrorMsg('');
  };

  handleEditClick = (runner) => {
    const {
      allRunners,
      history: { push },
      setRunnerIndex,
    } = this.props;

    setRunnerIndex(_.indexOf(allRunners, runner));
    push(EDIT_RUNNER_URL);
  };

  render() {
    if (!this.shouldRender()) return null;
    const { formatMsg, allRunners, runner, updateRunner } = this.props;
    const { errorMsg } = this.state;

    return (
      <DetailsConfirmationPage
        title={formatMsg('details_confirmation')}
        previousPage={REGISTRATION_FORM_URL}
        mainData={runner}
        allRunners={allRunners}
        handleAddRunner={this.handleAddRunner}
        handleEditClick={this.handleEditClick}
        handleDeleteClick={this.setDeleteConfirmation}
        handleContinue={this.handleContinue}
        updateMainData={updateRunner}
        errorMsg={errorMsg}
        dismissError={() => this.setErrorMsg('')}
      />
    );
  }
}

export default DetailsConfirmation;
