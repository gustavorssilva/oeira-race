import React, { Component } from 'react';
import { array, func, object } from 'prop-types';
import { scrollToTop } from 'utils/functions';
import RunnerForm from 'components/RunnerForm';
import { DETAILS_CONFIRMATION_URL } from '../constants';

class EditRunner extends Component {
  state = { oldRunnerData: this.props.runner };

  componentWillMount() {
    const {
      history: { push },
      allRunners,
      runner,
      findErrors,
    } = this.props;
    if (!allRunners.length || findErrors(runner)) return push('/');
    return scrollToTop();
  }

  handleGoBack = () => {
    const { oldRunnerData } = this.state;
    const {
      history: { push },
      updateRunner,
    } = this.props;
    updateRunner(oldRunnerData);
    push(DETAILS_CONFIRMATION_URL);
  };

  render() {
    const { formatMsg, runner, allRunners } = this.props;
    if (!allRunners || !runner) return null;

    return (
      <RunnerForm
        title={formatMsg('edit_runner')}
        goBack={this.handleGoBack}
        {...this.props}
      />
    );
  }
}

EditRunner.propTypes = {
  findErrors: func.isRequired,
  formatMsg: func.isRequired,
  updateRunner: func.isRequired,
  runner: object.isRequired,
  allRunners: array.isRequired,
  history: object.isRequired,
};

export default EditRunner;
