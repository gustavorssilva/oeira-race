import React, { Component } from 'react';
import { array, func, number, object } from 'prop-types';
import { scrollToTop } from 'utils/functions';
import RunnerForm from 'components/RunnerForm';
import { DETAILS_CONFIRMATION_URL } from '../constants';

class AddRunner extends Component {
  componentWillMount() {
    const {
      history: { push },
      allRunners,
      currentRunnerIndex,
    } = this.props;
    if (!allRunners.length || currentRunnerIndex === 0) return push('/');
    return scrollToTop();
  }

  handleGoBack = () => {
    const {
      history: { push },
      deleteRunner,
      currentRunnerIndex,
    } = this.props;
    deleteRunner(currentRunnerIndex);
    push(DETAILS_CONFIRMATION_URL);
  };

  render() {
    const { formatMsg, runner, allRunners } = this.props;
    if (!allRunners || !runner) return null;

    return (
      <RunnerForm
        title={formatMsg('add_runner')}
        goBack={this.handleGoBack}
        {...this.props}
      />
    );
  }
}

AddRunner.propTypes = {
  formatMsg: func.isRequired,
  currentRunnerIndex: number.isRequired,
  deleteRunner: func.isRequired,
  runner: object.isRequired,
  allRunners: array.isRequired,
  history: object.isRequired,
};

export default AddRunner;
