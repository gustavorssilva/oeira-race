import _ from 'lodash';
import React, { Component } from 'react';
import { func, number, object } from 'prop-types';
import styled from 'styled-components';
import { isAfterPriceChange } from 'utils/functions';
import { BigButton } from 'components';
import { FullWidthDiv } from 'components/styled';
import {
  DOWNLOAD_EXCEL_URL,
  JOIN_TEAM_START_URL,
} from 'containers/Teams/constants';
import {
  PRICE_BY_LEVEL,
  PRICE_LEVELS,
  REGISTRATION_TYPES,
  EDIT,
  INDIVIDUAL,
  CREATE_TEAM,
  JOIN_TEAM,
  REGISTRATION_FORM_URL,
  REQUEST_EDIT_LINK_URL,
  BEFORE_PRICE,
} from '../constants';

class RaceSelect extends Component {
  componentWillMount() {
    this.props.createRunnerDetails();
  }

  isPriceAvailable(index) {}

  isRegistrationTypeDisabled(type) {
    return false;
  }

  handleRegistrationSelect(type) {
    const {
      createRunnerDetails,
      history: { push },
    } = this.props;

    switch (type) {
      case EDIT:
        return push(REQUEST_EDIT_LINK_URL);
      case CREATE_TEAM:
        return push(DOWNLOAD_EXCEL_URL);
      case JOIN_TEAM:
        return push(JOIN_TEAM_START_URL);
      default:
        createRunnerDetails(INDIVIDUAL);
        return push(REGISTRATION_FORM_URL);
    }
  }

  renderPriceInfo = (key) => {
    const { formatMsg } = this.props;
    const { withChip } = PRICE_BY_LEVEL[key];
    const isAvailable =
      key === BEFORE_PRICE ? !isAfterPriceChange() : isAfterPriceChange();

    return (
      <PriceTableRow key={key} isAvailable={isAvailable}>
        <PriceTitle>{formatMsg(`race_price_${key}`)}</PriceTitle>
        <PriceRow>
          <div>
            <BigText>{withChip}</BigText>
            <EuroSymbol>€</EuroSymbol>
          </div>
        </PriceRow>
      </PriceTableRow>
    );
  };

  renderPriceTable() {
    return <PriceTable>{_.map(PRICE_LEVELS, this.renderPriceInfo)}</PriceTable>;
  }

  renderRaceButtons() {
    const { formatMsg } = this.props;
    const buttons = _.map(REGISTRATION_TYPES, (type) => (
      <BigButton
        key={type}
        disabled={this.isRegistrationTypeDisabled(type)}
        onClick={() => this.handleRegistrationSelect(type)}
      >
        {formatMsg(type)}
      </BigButton>
    ));

    return <Column>{buttons}</Column>;
  }

  render() {
    const { formatMsg } = this.props;

    return (
      <FullWidthDiv>
        <h1>{formatMsg('registration_pack')}</h1>

        {this.renderPriceTable()}
        {this.renderRaceButtons()}
      </FullWidthDiv>
    );
  }
}

RaceSelect.propTypes = {
  createRunnerDetails: func.isRequired,
  currentPrice: number,
  formatMsg: func.isRequired,
  history: object.isRequired,
};

const Column = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  margin: 1.5rem auto;
`;

const PriceTable = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  padding: 10px;
  max-width: 100vw;
`;

const PriceTableRow = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  flex-direction: column;

  color: ${({ isAvailable, theme }) =>
    isAvailable ? theme.textColor : theme.inactiveGrey};
  padding: 20px 20px 20px 10px;
  width: 300px;
  max-width: inherit;

  &:not(:last-child) {
    border-bottom: 1px solid ${({ theme }) => theme.primary};
  }
`;

const PriceRow = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  text-align: center;

  > div {
    line-height: 1.5;
    margin-top: 0.25rem;
    width: 100%;


    > span {
      line-height: 1;
    }
  }
`;

const PriceTitle = styled.div`
  font-weight: 600;
  margin-bottom: 0.5rem;
  text-transform: uppercase;
`;

const EuroSymbol = styled.span`
  line-height: 1;
`;

const BigText = styled.span`
  font-size: 24px;
  letter-spacing: -1px;
  line-height: 0.9 !important;
  margin-right: 3px;
`;

export default RaceSelect;
