import _ from 'lodash';
import isSameDay from 'date-fns/is_same_day';
import isBefore from 'date-fns/is_before';
import { getCurrentPrice } from 'utils/functions';
import {
  BASIC_PHONE_REGEX,
  EMAIL_REGEX,
  PORTUGAL,
  IS_LOCALHOST,
  API_RACE_NAME,
} from 'utils/constants';
import {
  // REGISTRATION_TYPE,
  CHIP,
  ID_NUMBER,
  DOB,
  FULL_NAME,
  COUNTRY_OF_RESIDENCE,
  DISTRICT,
  EMAIL,
  PHONE,
  GENDER,
  SHIRT_SIZE,
  DORSAL_NAME,
  START_BOX,
  EVIDENCE_UPLOAD,
  AGREE_TO_TERMS,
  AGREE_TO_PRIVACY_POLICY,
  ACCEPT_EMAIL_CONTACT,
  ACCEPT_PHONE_CONTACT,
  ACCEPT_EMAIL_CONTACT_OTHER_EVENTS,
  FORM_FIELDS,
  INVOICE_FIELDS,
  TEAM_NAME,
  MANAGER_NAME,
  MANAGER_EMAIL,
  MANAGER_PHONE,
  KIT_DELIVERY,
  ENROLLMENTS,
  DEFAULT_DISTRICT,
  OPT_INS,
  EMAIL_CONFIRMATION,
  BILLING_NAME,
  BILLING_ADDRESS,
  BILLING_CITY,
  BILLING_POSTCODE,
  BILLING_TAX_NUMBER,
  MIN_ADULTS_BIRTHDATE,
  START_BOX_FAST_RUNNERS,
  START_BOX_GENERAL,
  START_BOX_NO_CHIP,
  RACE,
} from '../constants';

export const defaultRunnerObject = {
  [RACE]: API_RACE_NAME,
  [CHIP]: `chip`,
  [DISTRICT]: '',
  [DOB]: '',
  [DORSAL_NAME]: '',
  [EMAIL]: '',
  [FULL_NAME]: '',
  [GENDER]: '',
  [ID_NUMBER]: '',
  [COUNTRY_OF_RESIDENCE]: '',
  [PHONE]: '',
  [SHIRT_SIZE]: '',
  [START_BOX]: '',
  [EVIDENCE_UPLOAD]: null,
  [AGREE_TO_TERMS]: false,
  [AGREE_TO_PRIVACY_POLICY]: false,
  [ACCEPT_EMAIL_CONTACT]: false,
  [ACCEPT_PHONE_CONTACT]: false,
  [ACCEPT_EMAIL_CONTACT_OTHER_EVENTS]: false,
};

export const dummyRunnerObject = () => ({
  ...defaultRunnerObject,
  [CHIP]: `chip`,
  [DISTRICT]: 'Faro',
  [DOB]: new Date('1974-08-31').toISOString(),
  [DORSAL_NAME]: 'Juan Guy',
  [EMAIL]: 'ciaran.edwards@knit.pt',
  [FULL_NAME]: 'Juan Guy',
  [GENDER]: 'M',
  [ID_NUMBER]: _.toString(Math.random() * 10).replace(/\D/, ''),
  [COUNTRY_OF_RESIDENCE]: 'Portugal',
  [PHONE]: '8937345345',
  [SHIRT_SIZE]: 'M',
  [START_BOX]: START_BOX_GENERAL,
  [EVIDENCE_UPLOAD]: null,
  [AGREE_TO_TERMS]: true,
  [AGREE_TO_PRIVACY_POLICY]: true,
  [ACCEPT_EMAIL_CONTACT]: true,
  [ACCEPT_PHONE_CONTACT]: true,
  [ACCEPT_EMAIL_CONTACT_OTHER_EVENTS]: true,
  [BILLING_NAME]: 'TEST',
  [BILLING_ADDRESS]: 'TEST',
  [BILLING_CITY]: 'TEST',
  [BILLING_POSTCODE]: 'TEST',
  [BILLING_TAX_NUMBER]: 'TEST',
});

export function newRunner() {
  const runnerObject = IS_LOCALHOST ? dummyRunnerObject() : defaultRunnerObject;
  return { ...runnerObject };
}

export const newTeam = () => ({
  [RACE]: API_RACE_NAME,
  [TEAM_NAME]: '',
  [MANAGER_NAME]: '',
  [MANAGER_EMAIL]: '',
  [MANAGER_PHONE]: '',
  [KIT_DELIVERY]: '',
  [ENROLLMENTS]: [],
});

export const defaultEditObject = () => ({
  [ID_NUMBER]: '',
  [DOB]: '',
});

export const hasValue = (value) => !!value || value === false;

export function needsEvidence(runner) {
  return hasChip(runner) && runner[START_BOX] === START_BOX_FAST_RUNNERS;
}

function isMissingInfo(runner, isEdit = false) {
  let requiredFields = [];

  if (!needsEvidence(runner)) {
    requiredFields = _.without(FORM_FIELDS, EVIDENCE_UPLOAD, START_BOX);
  } else {
    requiredFields = FORM_FIELDS;
  }

  if (isEdit) requiredFields = _.without(requiredFields, ...OPT_INS);

  const emptyFields = _.filter(requiredFields, (key) => !hasValue(runner[key]));
  if (emptyFields.length === 0) return null;
  return ['missing_the_following_fields', ...emptyFields];
}

export function areRequiredOptInsMissing(runner) {
  const missingOptIns = _.filter(
    [AGREE_TO_TERMS, AGREE_TO_PRIVACY_POLICY],
    (field) => !runner[field]
  );

  if (missingOptIns.length === 0) return null;
  return [
    'missing_the_following_permissions',
    ..._.map(missingOptIns, (optIn) => `missing_${optIn}`),
  ];
}

export function hasValidEmail(runner) {
  return EMAIL_REGEX.test(runner[EMAIL]) ? null : 'invalid_email';
}

export function emailAndEmailConfirmationMatch(runner) {
  return IS_LOCALHOST || runner[EMAIL] === runner[EMAIL_CONFIRMATION]
    ? null
    : 'email_not_confirmed';
}

export function hasValidPhoneNumber(runner) {
  return BASIC_PHONE_REGEX.test(runner[PHONE]) ? null : 'invalid_phone';
}

export function hasValidShirtSize(runner, shirtSizes) {
  const { originalShirt } = runner;
  const hasAvailableShirt = _.includes(shirtSizes, runner[SHIRT_SIZE]);
  const sameAsOriginal =
    originalShirt &&
    runner[SHIRT_SIZE] === originalShirt.size &&
    originalShirt.gender === runner[GENDER];

  return sameAsOriginal || hasAvailableShirt
    ? null
    : 'shirt_size_not_available';
}

function getBirthday(runner) {
  const dateString = runner[DOB];
  if (!dateString) return new Date();
  return new Date(dateString.split('T')[0]);
}

const isSameOrBefore = (a, b) => isBefore(a, b) || isSameDay(a, b);

function isOver18(runner) {
  const birthday = getBirthday(runner);
  return isSameOrBefore(birthday, MIN_ADULTS_BIRTHDATE);
}

export function isTooYoung(runner) {
  return !isOver18(runner) ? 'adults_race_is_for_adults' : null;
}

export function validateRunnerDetails(runner, shirtSizes) {
  const errors = [
    isMissingInfo(runner),
    hasValidShirtSize(runner, shirtSizes),
    isTooYoung(runner),
    hasValidEmail(runner),
    emailAndEmailConfirmationMatch(runner),
    hasValidPhoneNumber(runner),
    areRequiredOptInsMissing(runner),
  ];
  return _.find(errors);
}

export function validateRegistrationEdit(runner, shirtSizes) {
  const errors = [
    isMissingInfo(runner, true),
    hasValidShirtSize(runner, shirtSizes),
    isTooYoung(runner),
    hasValidEmail(runner),
    hasValidPhoneNumber(runner),
    emailAndEmailConfirmationMatch(runner),
  ];
  return _.find(errors);
}

function pickGivenInvoiceInfo(details) {
  return _.compact(_.values(_.pick(details, INVOICE_FIELDS)));
}

export const hasAllInvoiceInfo = (details) =>
  pickGivenInvoiceInfo(details).length === INVOICE_FIELDS.length;

export const hasNoInvoiceInfo = (details) =>
  _.isEmpty(pickGivenInvoiceInfo(details));

export const hasIncompleteInvoiceInfo = (details) =>
  !hasNoInvoiceInfo(details) && !hasAllInvoiceInfo(details);

export function applyRequiredChanges(data, newData) {
  // If a person changes gender, deselect the t-shirt
  if (newData[GENDER] && data[GENDER] !== newData[GENDER]) {
    newData[SHIRT_SIZE] = '';
    return;
  }

  // If a person changes the chip selection, reset their shirt and start box
  if (hasValue(newData[CHIP]) && newData[CHIP] !== data[CHIP]) {
    newData[SHIRT_SIZE] = '';
    newData[START_BOX] = hasChip(newData)
      ? START_BOX_GENERAL
      : START_BOX_NO_CHIP;
  }

  const country = newData[COUNTRY_OF_RESIDENCE];
  if (country) {
    if (country !== PORTUGAL) {
      newData[DISTRICT] = DEFAULT_DISTRICT;
    } else if (data[DISTRICT] === DEFAULT_DISTRICT) {
      newData[DISTRICT] = '';
    }
  }
}

export function containsDuplicateIDs(allRunners) {
  return _.uniqBy(allRunners, ID_NUMBER).length !== allRunners.length;
}

export function enableShirtOption(runner, availableShirts, shirtSize) {
  const { originalShirt } = runner;
  const isOriginalShirt =
    originalShirt &&
    originalShirt.size === shirtSize &&
    originalShirt.gender === runner[GENDER];
  const isAvailable = _.includes(availableShirts, shirtSize);
  return isOriginalShirt || isAvailable;
}

export const hasChip = (runner) => runner[CHIP] === `chip`;

export function getRunnerPrice(runner) {
  const { withChip, withoutChip } = getCurrentPrice();
  return hasChip(runner) ? withChip : withoutChip;
}
