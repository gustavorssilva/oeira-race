import _ from 'lodash';
import allMessages from './allMessages';

const ptMessages = {};
_.each(allMessages, ({ id, defaultMessage }) => {
  ptMessages[id] = defaultMessage;
});

export default ptMessages;
