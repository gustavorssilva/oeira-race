import reg from 'containers/Registration/messages';
import team from 'containers/Teams/messages';
import nav from 'components/Navbar/messages';
import payment from 'components/PaymentTypeSelector/messages';
import promoCode from 'components/PromoCodeHandler/messages';

export default { ...nav, ...reg, ...team, ...payment, ...promoCode };
